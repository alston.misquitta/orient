                               ORIENT 5.0

			   by Anthony Stone

                        with contributions from
       Andreas Dullweber, Ola Engkvist, Elena Fraschini, Matt Hodges,
          Andy Meredith, David Nutt, Paul Popelier and David Wales


The Orient program provides tools for the development and evaluation
of analytical models of interactions between molecules, or between
molecules and solid surfaces. It does not provide any models itself;
they have to be obtained elsewhere, for example by using the CamCASP
program of Misquitta and Stone.

Obtaining the program
---------------------

In a suitable directory, run the command
  git clone https://gitlab.com/anthonyjs/Orient.git
This will construct a clone of the program files in the subdirectory
Orient.

Documentation
-------------

Full documentation is in Orient/doc/Manual.pdf.

Licensing
---------

It is no longer necessary to apply for a licence to use this program.
The program is distributed under the GNU General Public Licence (GPL)
(see https://www.gnu.org/licenses/gpl-3.0.en.html) and by downloading
it you accept the terms of this licence. A copy of the licence is in
the file Orient/doc/gpl-3.0.pdf.

Installation
------------

Please see the file Orient/INSTALL for installation instructions.

