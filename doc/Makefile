SHELL = /bin/bash
#
#  This makefile automatically runs LaTeX and BibTeX as necessary,
#  produces a .ps file from the resulting .dvi file, and invokes ghostscript
#  to display it. It requires the "save" and "reinstate" scripts given below,
#  to store the .aux file and reinstate it if it hasn't changed.
#
#  This version doesn't deal with included .tex files, or with .toc or index
#  files. Note that it never runs LaTeX more than three times; check the log
#  to make sure that the "Label(s) may have changed" message didn't appear
#  the last time through. If it did, just "make" again.
#
#  Change the following line to give the root of the TeX source file name,
#  e.g. source.tex in this case, or specify it on the command line using
#  make NAME=file
NAME = Manual
#  Give the name of the bibfile here if changes in the bibfile are to trigger
#  a call to BibTeX, or specify it on the command line. If there is no
#  bibfile, omit this BIB definition altogether.
BIB = all.bib

vpath %.bib ${HOME}/tex/bibtex/bib
ifdef BIB
BBL = ${NAME}.bbl
endif

#  Needed for latex2html
TMP = /tmp
HTMLVERBOSITY = 1

FIGURES := figures/hsv.jpg figures/hls.jpg figures/rainbow.jpg \
           figures/acetylene.pdf figures/unitcell.pdf

manual: Manual.pdf
	evince $< &

Manual.pdf: Manual.tex Manual.bbl ${FIGURES} ${FRC}
	pdflatex Manual

implementation_notes:
	${MAKE} NAME=implementation_notes implementation_notes.pdf FRC=${FRC}

implementation_notes.pdf: implementation_notes.tex implementation_notes.bbl ${FRC}
	pdflatex $<

force:

always:

ifdef BIB
${NAME}.bbl: ${NAME}.aux ${BIB}
	-bibtex ${NAME}
ifneq "${NAME}" "implementation_notes"
	-[[ -a editbbl.pl ]] && editbbl.pl ${NAME}.bbl
endif
endif

%.aux:
	touch $@

restore:
	rm -f ${NAME}.aux
	reinstate ${NAME}.aux

clean:
	rm *.log 

