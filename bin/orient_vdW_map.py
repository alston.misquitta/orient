#!/usr/bin/python
#  -*-  coding  iso-8859-1  -*-

"""Set up an Orient data file to display an iso-density surface potential map.
"""

import argparse
import os.path
import re
import readline
import string
import subprocess
import sys

parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Set up an Orient data file to display the potential on a vdW surface.
""",epilog="""
Run this command in the molecule's top directory.



E.g.
$ORIENT/bin/orient_vdW_map.py piracetam_ISA.mdfn -m piracetam \\
   -d avdz-63/OUT_isa --scale 1.5""")

here = os.environ["PWD"]
home = os.environ["HOME"]

parser.add_argument("defn", help="Molecule definition (mdfn) file")
parser.add_argument("--dir", "-d", help="Directory containing files (default .)",
                    default=".")
parser.add_argument("--molecule", "-m", help="Molecule name")
parser.add_argument("--title", help="Title for display")
parser.add_argument("--limit", "--rank", default=5, 
                    help="Maximum rank of multipole to include (default 5)")
parser.add_argument("--scale", default=1.0, type=float,
                    help="Scale factor for van der Waals radii, default 1.0")
parser.add_argument("--step", help="Nominal grid step size, default 0.5", default="0.5")
parser.add_argument("--viewport", default=10.0, type=float,
                    help="Viewport half-width in bohr, default 10.0")
parser.add_argument("--colour-range", default=1.0, type=float,
                    help="limit of colour scale (volt), default 1.0")
parser.add_argument("--centre", help="Position in molecule to centre on screen",
                    nargs=3)
gridargs = parser.add_mutually_exclusive_group()
gridargs.add_argument("--export", help="File to export generated grid",
                      default="")
gridargs.add_argument("--import", dest="imprt", help="File for surface grid to import",
                      default="")
gridargs.add_argument("--compare", help="File for surface grid with reference values",
                      default="")

args = parser.parse_args()
defn = args.defn
data = re.sub(r'\.mdfn', '.data', defn)

#  Switch to specified directory
if args.dir != ".":
  os.chdir(args.dir)

colourmap = os.path.join(home,"molecules","colourmaps","colourmap")


if args.imprt:
  grid = args.imprt
elif args.export:
  grid = args.export
  if os.path.exists(grid):
    q = raw_input("Grid file {} exists already. Proceed? [yN]: ".format(grid))
    if q == "N" or q == "n" or q == "":
      exit(0)
elif args.compare:
  grid = args.compare
else:
  grid = ""
if grid:
  print grid

if args.title:
  title = args.title
else:
  title = args.molecule

if args.centre:
  centre = "{} {} {}".format(args.centre[0],args.centre[1],args.centre[2])
else:
  centre = "0.0 0.0 0.0"

with open(data,"w") as DATA:
  DATA.write("""UNITS BOHR

Parameters
      Sites      32 polarizable      0
      S-functions 50000
      Alphas 50000
      Parameter-sets 50000
      Pairs 100000
End

Types
      H         Z     1
      C         Z     6
      N         Z     7
      O         Z     8
      F         Z     9
      P         Z    15
      S         Z    16
     Cl         Z    17
End
""")

  with open(args.defn) as DEFN:
    mdfn = DEFN.read()
  DATA.write(mdfn)

  mol = re.search(r'\n *Molecule +(\S+) +', mdfn, flags=re.I).group(1)
  DATA.write("""
Edit {m}
  Bonds auto
  ! verbose
  limit {lim} clear all
End

Units Bohr eV
Display
  """.format(m="piracetam",lim=args.limit))
  if args.export or not grid:
    #  Specify new grid
    DATA.write("""  Grid
    Title "{m} vdWx{s:3.1f}"
    Molecule {m}
    Step {step}
    radii scale {s}
  End
""".format(m=mol,s=args.scale,step=args.step))
  elif args.imprt:
    #  Import grid, no map data
    DATA.write("import grid {}\n".format(args.imprt))
  elif args.compare:
    #  Import grid with reference map values
    DATA.write("import grid {} reference\n".format(args.compare))
  if args.export:
    DATA.write("export grid {}\n".format(args.export))

  DATA.write("""  Map new
    Molecule {m}
    Title "{t}"
    Potential
  End
  """.format(m=mol,t=title))

  if args.compare:
    map = "diff"
    DATA.write("""  Map diff
    Molecule {m}
    Title "{m} vdWx{s:3.1f}"
    Difference new minus reference
  End
""".format(m=mol,s=args.scale))
  else:
    map = "new"

  DATA.write("""  Viewport {v}

  Describe {m}

  Show {m}
    Viewpoint 0.0 1.0 0.0 up 0.0 0.0 1.0
    Centre {cen}
    #include {cm}
    Colour-scale min -{cr} max {cr}
    ball-and-stick {mol}
    ! Contours eV 2 1 0 -1 -2
    ! ticks eV -2.0 -1.0 0 1.0 2.0
  End
End

Finish
""".format(m=map,mol=mol,v=args.viewport,cr=args.colour_range,cm=colourmap,cen=centre))
    


with open(data) as IN:
  subprocess.call(["orient"], stdin=IN, stdout=sys.stdout, stderr=subprocess.STDOUT)
