#!/usr/bin/env python3
#  -*-  coding:  utf-8  -*-

"""Compare Orient dump file with reference.
"""

from functions import *
import argparse
import re
# import os.path
# import string
# import subprocess

this = __file__
parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Compare orient dump file with reference.
""",epilog=f"""
Usage: {this} file1 [file2]
Compare dump files.
If one file argument is given, file2 is taken to be check/file1.
""")

parser.add_argument("file", help="Files to compare", nargs="+")

args = parser.parse_args()

file1 = args.file[0]
f1 = ofile(file1)
if len(args.file) > 1:
    file2 = args.file[1]
else:
    file2 = "check/"+file1
f2 = ofile(file2)
print(f"\nChecking {file1} against {file2}")

eps = 1e-6
p = 0
ok = True
while p < len(f1.lines) and p < len(f2.lines):
    m1 = re.match(r'.{15} +(-?\d+\.\d+[DE][+-]\d+) +(-?\d+\.\d+[DE][+-]\d+) +(-?\d+\.\d+[DE][+-]\d+)$', f1.lines[p])
    m2 = re.match(r'.{15} +(-?\d+\.\d+[DE][+-]\d+) +(-?\d+\.\d+[DE][+-]\d+) +(-?\d+\.\d+[DE][+-]\d+)$', f2.lines[p])
    if m1 and m2:
        x1 = [float(m1.group(g)) for g in [1,2,3]]
        x2 = [float(m2.group(g)) for g in [1,2,3]]
        if abs(x1[0]-x2[0]) > eps or abs(x1[1]-x2[1]) > eps or abs(x1[2]-x2[2]) > eps:
            print(f"Mismatch at line {p:1d}:\n{f1.lines[p]}{f2.lines[p]}")
            ok = False
    else:
        if not m1:
            print(f"Unexpected line {p:1d} in file {f1.lines[p]}")
        if not m2:
            print(f"Unexpected line {p:1d} in file {f2.lines[p]}")
        exit(2)
    p += 1

if ok:
    print(f"Files {file1} and {file2} match")
    exit(0)
else:
    exit(1)
