#!/usr/bin/perl -w

$file="out";
$ok=1;

open(NEW,"$file") or die "No file $file. Stopped";
open(CHECK,"check/$file") or die "No file check/$file. Stopped";

print "\nChecking output file $file\n";

&check("Constrained minimum");

if ( $ok ) {
  print "File $file checked.\n";
}
else {
  print "Differences found in file $file.\n";
  exit 1;
}

sub find_energy {
  local (*H) = @_;
  local $n=0;
  local $e="";
  while (<H>) {
    die "End of file" if eof;
    $n++ if /^$s *$/;
    last if $n==2;
  }
  while (<H>) {
    last if /^Total energy/;
  }
  if (/^Total energy: *(-?[0-9]+\.[0-9]+)/) {
    $e=$1
  }
  $e
}

sub find {
  local (*H,$s) = @_;
  while (<H>) {
    die "End of file" if eof;
    last if /$s/;
  }
}

sub check {
  local ($s)=@_;
  print "$s: ";
  $e_check=&find_energy(*CHECK);
  $e_new=&find_energy(*NEW);
  if ( $e_new eq "") {
    print "No match in new file\n";
  }
  else {
    if (abs(($e_check-$e_new)/($e_check+$e_new)) < 1e-6 ) {
      printf "%-10.5f -- O.K.\n", $e_new;
    }
    else {
      printf "Energies differ -- new %-10.5f, check %-10.5f\n", $e_new, $e_check;
      $ok=0;
    }
  }
}

sub diff {
  local ($s)=@_;
  my $ok=1;
  &find(*CHECK,$s);
  &find(*NEW,$s);
  while (<CHECK>) {
    $new=<NEW>;
    last if /^ *$/;
    if ( $new ne $_ ) {
      print "check file: $_";
      print "new file:   $new\n";
      $ok=0;
    }
  }
  if ( ! $ok ) {
    printf "New file differs from check file\n";
  }
  $ok
}
