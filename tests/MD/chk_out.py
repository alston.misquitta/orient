#!/usr/bin/env python3
#  -*-  coding:  utf-8  -*-

"""Check output file for quench energies.
"""

from functions import *
import argparse
import re
# import os.path
# import string
# import subprocess

this = __file__
parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Compare output file with reference in check directory.
""",epilog="""
Usage: {} out
""".format(this))

parser.add_argument("file", default=["out"], nargs="*")

args = parser.parse_args()

eps = 1.0e-6
ok = True
quenches = 0

file = args.file[0]

# chk = ofile("check/"+file)
new = ofile(file)
print(f"\nChecking {file}")
Equench = [
  "-116.590619",
  "-100.505135",
  "-95.029567",
  "-93.677782",
  "-93.063993",
  "-85.452734",
  ]
quenches = {}
end = False     
ok = True

new.p = 0
while True:
    if new.p >= len(new.lines):
        break
    if re.match("Quench result", new.lines[new.p]):
        m = re.search(r'(-?\d+\.\d+)', new.lines[new.p])
        qe = m.group(1)
        if qe in list(quenches.keys()):
            quenches[qe] += 1
        else:
            quenches[qe] = 1
    new.p += 1

print("Quench energies:")
for qe in sorted(quenches.keys()):
    if qe in Equench:
        print(f"{qe:>15s}  {quenches[qe]:2d}")
    else:
        print(f"{qe:>15s}  {quenches[qe]:2d}  *")
        ok = False

if ok:
    print("Quenched energies checked.")
    exit(0)
else:
    print(f"""* Unexpected quenched energies in file {file}.
    These may be valid local minima -- check frequencies.""");
    exit (1)


