#!/usr/bin/env python3
#  -*-  coding:  utf-8  -*-

import subprocess
import sys

rc = 0
rc += subprocess.call(["../cmp_xyz.py", "w2mina.xyz"])
rc += subprocess.call(["../cmp_xyz.py", "w2minb.xyz"])
rc += subprocess.call(["../cmp_dump.py", "w2mina.dump"])
rc += subprocess.call(["../cmp_dump.py", "w2minb.dump"])
rc += subprocess.call(["./cmp_out.py"])

# print "rc = ", rc
sys.exit(rc)

