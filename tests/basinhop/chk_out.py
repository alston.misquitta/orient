#!/usr/bin/env python3
#  -*-  coding:  utf-8  -*-

"""Check output file for energy minima.
"""

from functions import ofile
import argparse
import re
# import os.path
# import string
# import subprocess

this = __file__
parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Check output file for energy minima.
""",epilog=f"""
Usage: {this} out
""")

parser.add_argument("file", default=["out"], nargs="*")

args = parser.parse_args()

eps = 1.0e-6
ok = True
quenches = 0

file = args.file[0]

# chk = ofile("check/"+file)
new = ofile(file)

print(f"\nChecking file {file}")
minima = {
  "-116.590619": 0,
  "-95.029567": 0,
  "-93.063992": 0,
  "-93.677782": 0,
  "-85.452734": 0,
  "-100.505135": 0,
  "-84.301791": 0,
  }

end = False     
ok = True
new.p = 0
while True:
    if new.p >= len(new.lines):
        break
    m= re.match("Step \d+: +new minimum \d+, energy = +(-?\d+\.\d+)", new.lines[new.p])
    if m:
        F = m.group(1)
        old = False
        for E in list(minima.keys()):
            if abs(float(F)-float(E)) < 1e-4:
                minima[E] += 1
                old = True
        if not old:
            print(f"New minimum {F}")
            ok = False
    new.p += 1

print("Minima found:")
for E in sorted(minima.keys()):
    print(f"{E:>12s} {minima[E]:2d}")
  
if ok:
    print("Test output checked.")
    exit(0)
else:
    print(f"""Unexpected new minimum in file {file}.
These may be valid local minima -- check frequencies.""");
    exit (1)


