Parameters
  molecules 10
  Sites 64 Polarizable 10
  S_functions 5000
End

Note  Modified ASP-W potential for water.

Note  Molpro MRCI multipole moments with respect to axes with x in plane,
Note  z parallel to bond for H, along C2 for O.

Note  Dispersion coefficients from Wormer & Hettema, JCP (1992) 97, 5592

Note  Charge transfer term calculated by AJS using IMPT, fitted by CM to
Note  anisotropic exponential attraction

Note  Polarizabilities as in original paper.

Note  Damping is site-site isotropic Tang-Toennies, scale factor 1.92.

Units bohr

Types
  O  Z 8
  H  Z 1
  CM Z 0 Mass 0 nolabel
  CT Z 0 Mass 0 nolabel  ( Charge-transfer site, coincident with O )
End

Molecule Water1 at    -1.053166    2.325118   -4.633859 rotated by   88.275 about   0.442127  0.880085 -0.173128
!  Molecule Water1 at x1 y1 z1 rotated by psi1 about nx1 ny1 nz1 type CT

O at 0.0 0.0 0.0   Type O   Rank 4
  Q00  =  -0.330960
  Q10  =  -0.297907
  Q20  =   0.117935  Q22c =   0.673922
  Q30  =  -0.151827  Q32c =   0.303856
  Q40  =   0.114584  Q42c =  -0.183221  Q44c =  -0.065424

H2  at   polar 1.808657595 127.74 0    Rank 4  Type H
  Q00  =   0.165480
  Q10  =  -0.050267  Q11c =   0.144471
  Q20  =   0.047356  Q21c =  -0.085999  Q22c =   0.064345
  Q30  =  -0.052434  Q31c =  -0.117723  Q32c =   0.195233  Q33c =  -0.118823
  Q40  =  -0.117187  Q41c =   0.029905  Q42c =   0.197992  Q43c =  -0.247286
  Q44c =   0.123927

H1  at  polar 1.808657595 127.74 180   Rank 4  Type H
  Q00  =   0.165480
  Q10  =  -0.050267  Q11c =  -0.144471
  Q20  =   0.047356  Q21c =   0.085999  Q22c =   0.064345
  Q30  =  -0.052434  Q31c =   0.117723  Q32c =   0.195233  Q33c =   0.118823
  Q40  =  -0.117187  Q41c =  -0.029905  Q42c =   0.197992  Q43c =   0.247286
  Q44c =   0.123927

Axes z from O to H2 x from H1 to H2 rotate for H2
Axes z from O to H1 x from H2 to H1 rotate for H1

CM at 0.0 0.0 -0.122991736   Type CM  rank -1

End


Molecule Water2 at    -4.350832    2.987567   -0.638948 rotated by  153.096 about   0.966096 -0.191745  0.172894  Copy Water1

Molecule Water3 at    -1.076477   -2.848553   -3.853621 rotated by  108.921 about   0.760035  0.649454 -0.023572  Copy Water1

Molecule Water4 at     3.710763    1.805337   -2.467927 rotated by  129.482 about   0.117932  0.599206  0.791862  Copy Water1

Molecule Water5 at    -1.460633    2.873256    3.523268 rotated by  140.526 about   0.205183 -0.787333 -0.581383  Copy Water1

Molecule Water6 at     3.676338    2.923868    2.482007 rotated by   64.840 about   0.158387 -0.799962  0.578770  Copy Water1

Molecule Water7 at    -1.765744   -2.237292    4.536836 rotated by  141.250 about  -0.909516 -0.013702  0.415443  Copy Water1

Molecule Water8 at    -4.381886   -2.200691   -0.004909 rotated by  163.892 about   0.560031  0.717156 -0.414792  Copy Water1

Molecule Water9 at     3.094195   -2.232438    3.068653 rotated by   89.207 about   0.158009 -0.450835  0.878511  Copy Water1

Molecule Water10 at    3.607441   -3.396172   -2.011499 rotated by   53.032 about  -0.846892  0.257361 -0.465337  Copy Water1


Polarizabilities for Water1
Read rank 2 site O
(      00  10    11c   11s   20    21c   21s   22c   22s)
(00)  0.0  0.0   0.0   0.0   0.0   0.0   0.0   0.0   0.0
(10)  0.0  9.907 0.0   0.0  -4.249 0.0   0.0  -4.409 0.0
(11c) 0.0  0.0  10.311 0.0   0.0 -11.92  0.0   0.0   0.0
(11s) 0.0  0.0   0.0   9.549 0.0   0.0  -3.575 0.0   0.0
(20)  0.0 -4.249 0.0   0.0  29.871 0.0   0.0  -0.425 0.0
(21c) 0.0  0.0 -11.92  0.0   0.0  52.566 0.0   0.0   0.0
(21s) 0.0  0.0   0.0  -3.575 0.0   0.0  28.179 0.0   0.0
(22c) 0.0 -4.409 0.0   0.0  -0.425 0.0   0.0  37.273 0.0
(22s) 0.0  0.0   0.0   0.0   0.0   0.0   0.0   0.0  31.449

End

Polarizabilities for Water2
Copy site O from molecule Water1 site O

End

Polarizabilities for Water3
Copy site O from molecule Water1 site O

End

Polarizabilities for Water4
Copy site O from molecule Water1 site O

End

Polarizabilities for Water5
Copy site O from molecule Water1 site O

End

Polarizabilities for Water6
Copy site O from molecule Water1 site O

End

Polarizabilities for Water7
Copy site O from molecule Water1 site O

End

Polarizabilities for Water8
Copy site O from molecule Water1 site O

End

Polarizabilities for Water9
Copy site O from molecule Water1 site O

End

Polarizabilities for Water10
Copy site O from molecule Water1 site O

End


Units mH
Pairs
  Damping factor 1.41 (This will apply to induction )
  CM  CM          C6           C7          C8           C9        C10
  Damping factor 1.92
    00  00  0  46443.0        0.0     1141700.0         0.0    33441000.0
    10  00  1      0.0   -58982.1           0.0  -1890995.4
    20  00  2      0.0        0.0       31975.8         0.0     1015622.1
    22c 00  2   1899.3        0.0       85128.5         0.0     1493227.5
    22c 22c 4    248.6        0.0           0.0         0.0
    30  00  3      0.0    10730.4           0.0    747009.0
    32c 00  3      0.0   -19536.8           0.0  -1465660.7
    42c 00  4      0.0        0.0       46527.6         0.0     3023117.2
    10  10  2      0.0        0.0       68209.7         0.0     2642213.6
    10  10  0      0.0        0.0      -21304.2         0.0     -924915.1
    40  00  4      0.0        0.0      -37166.7         0.0    -2349333.3
    32c 10  4      0.0        0.0       24516.8         0.0     2130963.0
    30  10  4      0.0        0.0      -13480.7         0.0    -1085514.0
    44c 00  4      0.0        0.0       22014.6         0.0     1030490.3
    32c 32c 6      0.0        0.0       42445.5         0.0     1570995.5
  End
  CT H         rho       alpha
    Unit  -1.0
    00  00  0  3.899244  1.794173
    00  10  1 -0.826647
    00  20  2  0.706998
    00  30  3 -0.223902
    22c 00  2 -0.208578
  End
  O  O         rho       alpha
    00  00 0   5.68      2.002732
    10  00 1   0.194693
    20  00 2  -0.395620
    22c 00 2   0.099227
    30  00 3   0.360409
    32c 00 3  -0.204828
    40  00 4  -0.117409
    42c 00 4   0.070905
  End
  O  H         rho       alpha
    00  00 0   4.78      1.980393
    10  00 1   0.194693
    20  00 2  -0.395620
    22c 00 2   0.099227
    30  00 3   0.360409
    32c 00 3  -0.204828
    40  00 4  -0.117409
    42c 00 4   0.070905
    00 10  1  -0.402480
    00 11c 1  -0.281719
    00 20  2   0.006327
    00 21c 2  -0.143812
    00 22c 2   0.032326
    00 30  3   0.068294
    00 31c 3   0.074584
    00 32c 3   0.026826
    00 33c 3   0.142389
  End
  H  H         rho       alpha
    00 00  0   3.88      1.929894
    00 10  1  -0.402480
    00 11c 1  -0.281719
    00 20  2   0.006327
    00 21c 2  -0.143812
    00 22c 2   0.032326
    00 30  3   0.068294
    00 31c 3   0.074584
    00 32c 3   0.026826
    00 33c 3   0.142389
  End
End

units pop energy

Switch induce on iterate on

time

Units kJ/mol Angstrom

Options
  bfgs
  plot xyz new file opt.xyz every 1 title "Minimum"
End

optimize
  dump min.dump
end

time

Analyse

Time

!  Units mH bohr
!  triple-dipole 308200
Units kJ/mol angstrom
Triple-dipole 2633


time

Finish
