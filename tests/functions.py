#!/usr/bin/env python3
#  -*-  coding:  utf-8  -*-

"""Functions for comparing two program output files.
"""

# go_to(string,a,b,)
# In each file a and b, go to line starting with regular search pattern string

# get_float(string)
# Find a float number in string and return it.

#  cmp_after(s,a,b,skip=0,xyz=False):
#  If s is non-null, find line starting with string s in each file
#  Then skip lines as specified;
#  Then compare subsequent lines up to next blank line. Return True
#  if all match, False otherwise.

#  cmp_xyz(a,b):
#  Compare the last frame of each of two xyz files

import re
# import os.path
# import string
# import subprocess

eps = 1e-6

class ofile:
    # Read lines of file f into list self.lines
    # self.p is line pointer
    def __init__(self, f):
        self.p = 0
        self.file = f
        with open(f) as IN:
            self.lines = IN.readlines()

def die(string):
    print(string)
    exit(1)

def go_to(s,a,b):
    # go to line starting with string s in each of files a and b
    while True:
        if a.p >= len(a.lines):
            die(f"While searching for {s:1s}, end of file {a.file:1s} at line {a.p:1d}")
        if re.match(s,a.lines[a.p]):
            break
        a.p += 1
    while True:
        if b.p >= len(b.lines):
            die(f"While searching for {s:1s}, end of file {b.file:1s} at line {b.p:1d}")
        if re.match(s,b.lines[b.p]):
            break
        b.p += 1

def get_float(s):
    # get a float number from string s
    m = re.search(r' (-?\d+\.\d+([EeDd]-?\d+)?) ', s)
    if m:
        return float(m.group(1))
    else:
        die(f"Can't find float in string '{s}'")

def cmp_after(s,a,b,skip=0,xyz=False, show=False):
    #  If s is non-null, find line starting with string s in each file
    #  Then skip lines as specified;
    #  Then compare subsequent lines up to next blank line, printing
    #  each one if show=True
    ok = True
    fail = False
    if s:
        go_to(s,a,b)
    a.p += skip
    b.p += skip
    while True:
        a.p += 1
        b.p += 1
        #  break on blank lines or end of files
        if a.p >= len(a.lines) and b.p >= len(b.lines):
            break
        if a.p >= len(a.lines):
            print(f"End of file {a.file}")
            ok = False
            break
        elif b.p >= len(b.lines):
            print(f"End of file {b.file}")
            ok = False
            break
        elif re.match(r' *$', a.lines[a.p]) and re.match(r' *$', b.lines[b.p]):
            #  Both blank lines
            break
        elif a.lines[a.p] != b.lines[b.p]:
            #  Deal with negative zeros
            aline = re.sub(r'-(0\.0+)\b', r' \1', a.lines[a.p])
            bline = re.sub(r'-(0\.0+)\b', r' \1', b.lines[b.p])
            if aline != bline:
                if xyz:
                    m1 = re.match(r' *\w+ +(-?\d+\.\d+) +(-?\d+\.\d+) +(-?\d+\.\d+) *$', aline)
                    m2 = re.match(r' *\w+ +(-?\d+\.\d+) +(-?\d+\.\d+) +(-?\d+\.\d+) *$', bline)
                    if m1 and m2:
                        fail = any([abs(float(m1.group(g))-float(m2.group(g))) > eps for g in [1,2,3]])
                if fail or not xyz:
                    #  print mismatching lines
                    print("Mismatch")
                    print(f"{a.file:15s}: {a.lines[a.p]}", end=' ')
                    print(f"{b.file:15s}: {b.lines[b.p]}", end=' ')
                    ok = False
        elif show:
            print(b.lines[b.p], end=' ')
    return ok

def cmp_float(a,b):
    #  Compare float numbers in the current line of each file
    global eps
    Ea = get_float(a.lines[a.p])
    Eb = get_float(b.lines[b.p])
    return(abs(Ea-Eb) < eps)

def cmp_xyz(a,b):
    #  Compare the last frame of each of two xyz files
    na = int(a.lines[0])
    nb = int(b.lines[0])
    if na != nb:
        print("Files refer to different systems")
        return False
    a.p = len(a.lines) - na
    b.p = len(b.lines) - nb

    return cmp_after(None,a,b,xyz=True)
