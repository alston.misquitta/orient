      MODULE binomials

      USE parameters
      USE consts, ONLY : dp, pi
      IMPLICIT NONE
      INTEGER, PARAMETER :: tablesize=40

      DOUBLE PRECISION :: rtbinom(0:tablesize,0:tablesize),             &
          binom(0:tablesize,0:tablesize), rt(tablesize),                &
          factorial(0:tablesize), gamma_half(tablesize)
!  RTBINOM(N,M) contains the square root of the binomial coefficient.
!  Gamma_half(n) is the Gamma function Gamma(n/2)

!  Ladder operator coefficients
      DOUBLE PRECISION :: up(sq), down(sq)

      CONTAINS

      SUBROUTINE init_binomials

      IMPLICIT NONE
      INTEGER :: i,j,m,base

!  Initialize binomials and square roots
      binom(0,0)=1d0
      rtbinom(0,0)=1d0
      factorial(0)=1d0
      do i=1,tablesize
        factorial(i)=i*factorial(i-1)
        rt(i)=sqrt(dble(i))
        binom(i-1,i)=0d0
        binom(i,0)=1d0
        rtbinom(i,0)=1d0
        do j=1,i
          binom(i,j)=binom(i-1,j-1)+binom(i-1,j)
          rtbinom(i,j)=sqrt(binom(i,j))
        end do
      end do

!  Ladder operator coefficients
!     print "(a)", "Ladder operator coefficients"
      up(1)=0d0
      down(1)=0d0
      do j=1,maxlq
        base=j*j+j+1
        do m=-j,j
          up(base+m)=sqrt(real(j*(j+1)-m*(m+1),DP))
          down(base+m)=sqrt(real(j*(j+1)-m*(m-1),DP))
!         print "(2i3, i4, 2f12.6)", j, m, base+m, up(base+m), down(base+m)
        end do
      end do

      gamma_half(1)=sqrt(pi)       !  Gamma(0.5)=sqrt(pi)
      gamma_half(2)=1d0            !  Gamma(1) = 0!
      do j=3,tablesize
        gamma_half(j)=0.5d0*(j-2)*gamma_half(j-2)
      end do
!     print "(4(i4,f15.6))", (j, gamma_half(j), j=1,12)
      END SUBROUTINE init_binomials

      END MODULE binomials
