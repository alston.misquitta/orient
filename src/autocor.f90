MODULE autocor

USE molecules, ONLY : nmols
USE indparams
USE consts
USE types
USE sites
USE sizes

!  MAXAUTOCOR maximum number of grid points in the autocorrelation function.
!  MAXAUTOCORFUNC maximum number of autocorrelation function.
!  MAXAUTOCORSITES number of relevant sites for the autocorrelation
!  This number is less than or equal to NSITES. For the dipole
!  autocorrelation function MAXAUTOCORSITES=1.

INTEGER, PARAMETER :: MAXAUTOCOR=5000, MAXAUTOCORFUNC=1,           &
    MAXAUTOCORSITES=1

INTEGER :: autocor_len,autocor_num,autocor_pos,autocor_opos
INTEGER :: autocor_oshift
LOGICAL :: autocor_flag
CHARACTER(LEN=32) :: autocor_file
DOUBLE PRECISION :: autocor_timestep
DOUBLE PRECISION :: autocor_func(MAXAUTOCOR,MAXAUTOCORFUNC)
DOUBLE PRECISION :: autocor_data(MAXAUTOCOR,3*MAXAUTOCORSITES)

CONTAINS

SUBROUTINE update_autocor(momenta)

IMPLICIT NONE

DOUBLE PRECISION :: momenta(6*NMOLS)

!  New position in autocor_data
autocor_pos=autocor_pos+1
if (autocor_pos .gt. autocor_len) then
  autocor_pos=1
end if

!  Check if origin is hit and update functions if necessary
if (autocor_pos .eq. autocor_opos) then
  if (autocor_num .ne. -1) then
    call update_dipoleautocor(1)
  else
    autocor_num=0
  end if
end if

!  Collect data from time step
call update_dipoleautocordata

END SUBROUTINE update_autocor

!-----------------------------------------------------------------------

SUBROUTINE output_autocor

IMPLICIT NONE

INTEGER :: i

open(1,FILE=autocor_file,STATUS='UNKNOWN')
do i=1,autocor_len
  write (1,'(F15.4,2(A,1p,e20.12))')                                &
      dble(i-1)*autocor_timestep*time2fs,' ',                         &
      autocor_func(i,1)/dble(autocor_num),' ',                        &
      autocor_data(1,1)*autocor_data(i,1)+                            &
      autocor_data(1,2)*autocor_data(i,2)+                            &
      autocor_data(1,3)*autocor_data(i,3)
end do
close(1)

END SUBROUTINE output_autocor

!-----------------------------------------------------------------------

SUBROUTINE init_autocor

IMPLICIT NONE

INTEGER :: i,j

!  Init autocorrelation functions
do j=1,maxautocorfunc
  do i=1,autocor_len
    autocor_func(i,j)=0.0d0
  end do
end do
!  Init data storage fields
do j=1,3*maxautocorsites
  do i=1,autocor_len
    autocor_data(i,j)=0.0d0
  end do
end do

autocor_num=-1
autocor_pos=0
autocor_opos=1

END SUBROUTINE init_autocor

!-----------------------------------------------------------------------

SUBROUTINE update_dipoleautocordata

IMPLICIT NONE

DOUBLE PRECISION :: moments(4)
INTEGER :: i

!  Calculate dipole moments
call calc_moments(moments)

!  Overall dipole moment is stored in moments(2),moments(3),moments(4)
do i=1,3
  autocor_data(autocor_pos,i)=moments(i+1)
end do

END SUBROUTINE update_dipoleautocordata

!-----------------------------------------------------------------------

SUBROUTINE calc_moments(moments)

USE molecules
USE induction, ONLY: dq0, old, maxind, induction_cvg
USE properties
USE rotations, ONLY : wigner
IMPLICIT NONE

INTEGER :: n ,np, imol, i, k
DOUBLE PRECISION :: v(4,4),qi(4),moments(:)

do i=1,4
  moments(i) = 0.0d0
end do

!  Evaluate dipole moment in global system
do imol = 1,mols
  k = head(imol)
  do while (k > 0)
    if (l(k) >= 0) then
      !  Transform multipoles to global axis system
      !  Construct transformation matrix
      call wigner(se(1,1,sm(k)),v,1)
      !  Transform dipole
      !    QS=V*Q0
      !  print "(I3, 4F10.5)", n, q(1:4,k)
      if (l(k) > 0) then
        n = 4
      else
        n = 1
      endif
      call dgemv('n',n,n,1d0,v,4, q(1,k),1,0d0,qi,1)
      if (ps(k) .gt. 0) then
        !           This is a polarizable site; add the induced moments.
        if (pr(k) > 0) then
          np=4
        else
          np=1
        endif
        call dgemv('n',np,np,1d0,v,4,dq0(1,ps(k),old),1,1d0,qi,1)
      endif
      call shiftq(qi, 0,min(l(k),1),                                       &
          sx(1,k),sx(2,k),sx(3,k), moments,1)
    endif
    k=next(k)
  end do
end do

END SUBROUTINE calc_moments

!-----------------------------------------------------------------------

SUBROUTINE update_dipoleautocor(fctnr)

IMPLICIT NONE

INTEGER :: i, j, pos, fctnr
DOUBLE PRECISION :: norm

!  Calc norm
norm=0.0d0
do j=1,3
  norm=norm+autocor_data(autocor_opos,j)**2
end do
if (norm .eq. 0.0d0) then
  stop 'ERROR: No dipoles detected for dipole autocorrelation!'
end if

!  Calc v(0)*v(i) for full time length
pos=autocor_opos
do i=1,autocor_len
  do j=1,3
    autocor_func(i,fctnr)=autocor_func(i,fctnr)+                    &
        autocor_data(autocor_opos,j)*autocor_data(pos,j)/norm
  end do
  pos=pos+1
  if (pos .gt. autocor_len) then
    pos=1
  end if
end do

!  Set new origin
autocor_opos=autocor_opos+autocor_oshift
if (autocor_opos .gt. autocor_len) then
  autocor_opos=autocor_opos-autocor_len
end if

!  Increase number of correlation function evaluations
autocor_num=autocor_num+1

END SUBROUTINE update_dipoleautocor

!-----------------------------------------------------------------------

SUBROUTINE update_veloautocordata(momenta)

USE molecules, ONLY : mols, natoms, mmass
IMPLICIT NONE

DOUBLE PRECISION :: momenta(6*NMOLS)
INTEGER :: datapos, imol, i, mshft
DOUBLE PRECISION :: v_lin(3)

!  Evaluate site velocities in global system
datapos=0
do imol=1,mols
  mshft=(imol-1)*6
  !  Linear velocities
  do i=1,3
    v_lin(i)=momenta(mshft+i)/MMASS(imol)
  end do
  if (imol .gt. natoms) then
    stop 'not ready yet'
    !  Calculate angular velocities in principal axis system
    !         do i=1,3
    !          omega(i)=momenta(2*mshft+i+3)/MI(i,imol)
    !         end do
    !  Loop over all sites
    !         k=cm(imol)
    !         k=next(k)
    !         do while (k .ne. 0)
    !          sitemass=mass(type(k))
    !          if (sitemass .ne. 0.0d0) then
    !  To be continued...
    !          end if
    !          k=next(k)
    !         end do !k
    !  Transform omega into global system
    !         do i=1,3
    !          omega_g(i)=0.0d0
    !          do j=1,3
    !           omega_g(i)=omega_g(i)+omega(j)*SE(j,i,cm(imol))
    !          end do
    !         end do

  else
    !  And for atoms only linear velocities
    datapos=datapos+1
    do i=1,3
      autocor_data(autocor_pos,3*(datapos-1)+i)=v_lin(i)
    end do
  end if

end do!imol

END SUBROUTINE update_veloautocordata
!-----------------------------------------------------------------------

SUBROUTINE update_veloautocor(fctnr)

USE molecules, ONLY : mols, natoms
IMPLICIT NONE

INTEGER :: datapos, imol, i, j, pos, fctnr
DOUBLE PRECISION :: norm

datapos=0
do imol=1,mols
  if (imol .gt. natoms) then
    stop 'not ready yet!'
  else
    !  ATOMS: calc norm from A(0)*A(0)
    datapos=datapos+1
    norm=0.0d0
    do j=1,3
      norm=norm+autocor_data(autocor_opos,3*(datapos-1)+j)**2
    end do
    !  Set pos to origin
    pos=autocor_opos
    !  Calc v(0)*v(i) for full time length
    do i=1,autocor_len
      do j=1,3
        autocor_func(i,fctnr)=autocor_func(i,fctnr)+                &
            autocor_data(autocor_opos,3*(datapos-1)+j)*                &
            autocor_data(pos,3*(datapos-1)+j)/norm
      end do
      pos=pos+1
      if (pos .gt. autocor_len) then
        pos=1
      end if
    end do
  end if !imol>natoms
end do !imol

!  Set new origin
autocor_opos=autocor_opos+autocor_oshift
if (autocor_opos .gt. autocor_len) then
  autocor_opos=autocor_opos-autocor_len
end if
!  Increase number of correlation function evaluations
autocor_num=autocor_num+1

END SUBROUTINE update_veloautocor

END MODULE autocor
