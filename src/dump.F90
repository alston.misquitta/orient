MODULE dumper

PRIVATE
PUBLIC dump, dumpmc, undump

CONTAINS

SUBROUTINE dump(dumpfile,full)

!  Dump optimization data to disc for a subsequent restart.

USE consts
USE global
USE molecules
USE sites
USE utility
USE rotations, ONLY: quater
USE input, ONLY: find_io
IMPLICIT NONE

CHARACTER(LEN=*), INTENT(IN) :: dumpfile
LOGICAL, OPTIONAL, INTENT(IN) :: full

CHARACTER(LEN=5) :: nam(3) = (/' ux  ',' uy  ',' uz  '/)
DOUBLE PRECISION :: quaternion(4)
INTEGER i, imol, j, k, u
LOGICAL :: all

if (present(full)) then
  all=full
else
  all=.true.
end if

if (dumpfile .eq. "stdout") then
  u=6
else
  u=find_io(12)
  open(unit=u,file=pfile(dumpfile))
endif
if (all) then
  print '(A,A)', 'Dumping geometry to file ', pfile(dumpfile)
  do imol=1,mols
    k=cm(imol)
    do while (k .ne. 0)
      write(u,'(a15,1p,3e20.10)') name(k), (sx(i,k),i=1,3)
      if (sm(k) .eq. k) then
        do i=1,3
          write(u,'(10x,a5,1p,3e20.10)')  nam(i),(se(j,i,k),j=1,3)
        end do
      endif
      k=next(k)
    end do
  end do
else
  do imol=1,mols
    k=cm(imol)
    call quater(se(:,:,k),quaternion)
    write (u,'(a15,3f12.6,4f10.6)') name(k), sx(:,k)*rfact, quaternion
  end do
end if
if (dumpfile .ne. "stdout") close(u)

END SUBROUTINE dump
!--------------------------------------------------------------------
SUBROUTINE DUMPmc(dumpfile)

!  Dump optimization data to disc for a subsequent restart.

USE consts
USE global
USE molecules
USE sites
USE utility
#ifdef NAGF95
USE f90_unix_io, ONLY: flush
#endif
IMPLICIT NONE

CHARACTER(LEN=32) :: dumpfile
LOGICAL, SAVE :: firsta=.true.
INTEGER :: i, k, m

print '(A,A)', 'Dumping geometry to file ', pfile(dumpfile)
call flush(6)
if (firsta) then
  open(unit=122,file=pfile(dumpfile))
  firsta=.false.
endif
do m=1,mols
  if (head(m) .gt. 0) then
    k=head(m)
    do while (k .ne. 0)
      write(122,'(6F12.6)')(sx(i,k)*rfact, i=1,3)
      k=next(k)
    enddo
  endif
end do
call flush(122)
!close(122)

END SUBROUTINE DUMPmc

!-----------------------------------------------------------------------

SUBROUTINE undump(readfile)

USE global
USE input, only : die
USE molecules
USE sites
USE utility
IMPLICIT NONE
CHARACTER(LEN=32) :: readfile
CHARACTER(LEN=15) :: sname
CHARACTER(LEN=80) ::  buffer
INTEGER :: i, imol, j, k

!  Read information from previous dump file
open(unit=11,file=pfile(readfile))
do imol=1,mols
  k=cm(imol)
  do while (k .ne. 0)
    read (11,'(a15,3e20.10)') sname, (sx(i,k),i=1,3)
    if (sname .ne. name(k)(1:15)) then
      write (buffer,"(4a)") "Error in restore: site name ", trim(sname), &
          " doesn't match declared name ", trim(name(k)(1:15))
      call die (trim(buffer),.true.)
    endif
    if (sm(k) .eq. k) then
      do i=1,3
        read(11,'(15x,3e20.10)') (se(j,i,k),j=1,3)
      end do
    endif
    k=next(k)
  end do
end do
close(11)
write (6,'(A,A,A/A)') 'Site positions and orientations',           &
    ' have been read from file ',trim(pfile(readfile)),            &
    '(overwriting original data)'

END SUBROUTINE undump

END MODULE dumper
