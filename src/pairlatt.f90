      SUBROUTINE pairlatt (m1,k1,k2,order,er,edisp)

      USE consts
      USE damping
      USE interact
      USE molecules
      USE lattice
      USE pairderiv
      USE sites, ONLY : type
      USE switches
      USE types, ONLY : dform, dampf, pairindex, BMunit
      IMPLICIT NONE

      INTEGER :: k1, k2, m1, order
      DOUBLE PRECISION :: er, edisp

      INTEGER :: k, n, t1, t2
      DOUBLE PRECISION :: vr(12), gn, pn, g, g1, g2
      LOGICAL :: newd

!  Those in c0(n), n=6..10 are
!  the dispersion terms. alpha0 and rho0 are the parameters of the exponential
!  repulsion; er0 is the complete repulsion energy for this pair.
!  c1 and c2 hold the first and second derivatives of the dispersion
!  terms, and the other arrays similarly.

      er=0d0
      edisp=0d0
      vr(1)=1d0/r
      do k=1,11
        vr(k+1)=vr(k)*vr(1)
      end do
      do k=6,12
!  ......optimisation IF-statement:
       if (isdisp(k)) then
        c0(k)=0d0
       end if !isdisp(k)
      end do
      alpha0=0d0
      rho0=0d0
      er0=0d0
      disp0=0d0


!  Dispersion terms are similar, but remember that dispersion
!  coefficients have the minus sign omitted.
      do n=1,nmax
        do k=6,12
!  .........optimisation IF-statement
         if (isdisp(k)) then
!  .........optimisation variable introduced
          if (cx(n,k) .ne. 0d0) then
            c0(k)=c0(k)-cx(n,k)*s0(n)
          endif
         end if !isdisp(k)
        end do
!  Repulsion coefficients (alpha and rho) can be done in the same loop
        if (alphax(n) .ne. 0d0) then
          alpha0=alpha0+alphax(n)*s0(n)
        endif
        if (rhox(n) .ne. 0d0) then
          rho0=rho0+rhox(n)*s0(n)
        endif
      end do


!  Include damped or undamped r dependence for dispersion terms.
!  gn is R^-n f_n(R), gn1 and gn2 are its derivatives w.r.t. R.
!  f_n here is a Tang-Toennies damping function.
      t1=type(k1)
      t2=type(k2)
      if (t1 .gt. 0 .and. t2 .gt. 0) then
        pair=pairindex(t1,t2)
      else
        pair=0
      endif
      pn=6d0
      newd=.true.
      do n=6,12
        if (isdisp(n)) then
          call damp(dform(pair),dampf(0:,pair),n,alpha0,newd, r, g,g1,g2)
          gn=g*vr(n)
          disp0=disp0+c0(n)*gn
        endif
        pn=pn+1d0
        newd=.false.
      end do



!  Complete repulsion expression
      if (alpha0 .gt. 0d0) then
        er0=BMunit(0,pair)*(1d0+R*(BMunit(1,pair)+R*BMunit(2,pair)))*  &
            exp(alpha0*(rho0-R))
      endif

!  if (print .gt. 2) print '(2I4,F10.3,5'//EFMT//')',
!  &    k1, k2, r*rfact, es0*efact, er0*efact,
!  &    disp0*efact, (es0+er0+disp0)*efact

      er=er+er0
      edisp=edisp+disp0
      if (switch(7)) call fieldsml(m1,k1,0,k2,order)
      return

      END SUBROUTINE pairlatt
