MODULE damping

!  14 May 1998: This version uses code for the Pade damping function that
!  doesn't scale the variable R. (Because that seems to lead to numerical
!  instability.)

IMPLICIT NONE

DOUBLE PRECISION, PARAMETER :: RTPI=1.7724538509055160272d0
DOUBLE PRECISION, PARAMETER :: akma(20)=                               &
    (/0d0,0d0,0d0,0d0,0d0,0.3628d0,0d0,0.3073d0,0d0,0.2514d0,          &
    0d0,0d0,0d0,0d0,0d0,0d0,0d0,0d0,0d0,0d0/)
DOUBLE PRECISION, PARAMETER :: bkma(20)=                               &
    (/0d0,0d0,0d0,0d0,0d0,0.0336d0,0d0,0.02469d0,0d0,0.02379d0,        &
    0d0,0d0,0d0,0d0,0d0,0d0,0d0,0d0,0d0,0d0/)
DOUBLE PRECISION, PARAMETER :: dkma(20)=                               &
    (/0d0,0d0,0d0,0d0,0d0,1.651d-3,0d0,1.227d-3,0d0,0.5664d-3,         &
    0d0,0d0,0d0,0d0,0d0,0d0,0d0,0d0,0d0,0d0/)
!     DOUBLE PRECISION c, f
!     data c /2.75d0/, f /1.45d0/

CONTAINS

SUBROUTINE damp(type,dampf,n,alpha,new, r, g,g1,g2)

!  Damping function calculation. Returns the damped R^(-n) and its
!  derivatives w.r.t. R.
!  new is true if this calculation starts from scratch, false if a
!  previous call has evaluated intermediate quantities that can be
!  re-used.

!  Damping types:

!  0  No damping
!  1  TT: Tang-Toennies, b = scale
!  2  TT, b = alpha*scale
!  3  HFD
!  4  KMA
!  5  ERF: Error function (Sprik)
!  6  SBCS: Szcesniak et al
!  7  (1+exp(-br)-(1+c)exp(-kbr))**6
!  8-11
!     Pade. Order of Pade is 2*(t-5) for type t.
!  12-15
!     Shifted Pade. Order of Pade is 2*(t-9) for type t.
!  16 SqrtTT: Sqrt Tang-Toennies (as used in ASP-W)
!  17 (1+exp(-br**2)-(1+c)exp(-kbr**2))**3
!  20 Gaussian: Tkatchenko-DiStasio damping
!  21 Modified Tang-Toennies, b = -d(ln V_er)/dR

USE input, ONLY : die
USE gammafns, ONLY : erf
IMPLICIT NONE

!  type:  Type of damping function.
!  n:     Power of 1/R to be damped.
!  alpha: Repulsion steepness parameter (for use in type 2 damping).
!  dampf: Parameters of damping function. Usually just the scale factor
!         dampf(0).
!  new:   True if this calculation starts from scratch, false if a
!         previous call has evaluated intermediate quantities that can
!         be re-used.
!  g:     Damping factor for R^{-n}.
!  g1,g2: Derivatives of g w.r.t. R.
INTEGER, INTENT(IN) :: type, n
DOUBLE PRECISION, INTENT(IN) :: dampf(0:), alpha, r
LOGICAL, INTENT(IN) :: new
DOUBLE PRECISION, INTENT(OUT) :: g, g1, g2

DOUBLE PRECISION :: scale
INTEGER :: m
DOUBLE PRECISION :: c, s, b, br, br2, ebr, efbr, y, z, f, fn

scale=dampf(0)

select case(type)
case(0)
  g=1d0
  g1=0d0
  g2=0d0

case(1,2,16,21,22,24)
!  Tang-Toennies: incomplete gamma function of order n+1 for R^(-n)
!  interaction; i.e. g=P(scale*r,n+1). The scale factor is as supplied
!  in the data file for type 1; for type 2 it is multiplied by the current
!  value of the repulsion parameter alpha in the calling program.
!  For type 16 the damping function is the square root of the TT function.
!  Types 21, 22 and 24 are versions of modified Tang-Toennies
  select case(type)
  case(1,16,21)
    b = scale
  case(2,22,24)
    if (alpha < 1d-3) call die("DAMP called with zero alpha")
    b = alpha
  end select
  call Tang_Toennies(type,b,n,new, r, g,g1,g2)

case(3)
  !  HFD damping functions
  call HFD(n,r, g, g1, g2)


case(4)
  !  Koide-Meath-Allnatt
  fn=n
  z=exp(-akma(n)*R-bkma(n)*R**2-dkma(n)*R**3)
  g=(1d0-z)**n
  y=akma(n)+2d0*bkma(n)*R+3d0*dkma(n)*R**2
  g1=(fn*z/(1d0-z))*g*y
  g2=(fn*z/(1d0-z))*((fn-1)*z/(1d0-z)-1d0)*g*y**2                  &
      +2d0*g1*(bkma(n)+3d0*dkma(n)*R)

case(5)
  !  Error function damping (for Sprik water potential)
  g=erf(scale*r)
  g1=2d0*exp(-(scale*r)**2)/rtpi
  g2=-2d0*scale**2*r*g1

case(6)
!  Szcsesniak gaussian damping
  if (R < 0.1d0*scale) then
    g=0d0
    g1=0d0
    g2=0d0
  else if (R > scale) then
    g=1d0
    g1=0d0
    g2=0d0
  else
    z=scale/R
    g=exp(-(z-1)**2)
    g1=(2d0*z/R)*(z-1d0)*g
    g2=(2d0*z/R**2)*(((2d0*z-4d0)*z-1d0)*z+2d0)*g
  endif

case(7)
  !  Special damping function 7: exponential combination damping
  c=dampf(1)
  f=dampf(2)
  br=scale*r
  ebr=exp(-br)
  efbr=exp(-f*br)
  s=(1d0+c*ebr-(1d0+c)*efbr)
  g=s**6
  z=-scale*(c*ebr-f*(1d0+c)*efbr)  ! ds/dr
  g1=6d0*s**5
  g2=30d0*s**4*z**2+g1*(c*ebr-(1d0+c)*f*f*efbr)*scale*scale
  g1=g1*z

case(17)
  !  Special damping function 17: gaussian combination damping
  c=dampf(1)
  f=dampf(2)
  br2=scale*r**2
  ebr=exp(-br2)
  efbr=exp(-f*br2)
  s=(1d0+c*ebr-(1d0+c)*efbr)
  g=s**3
  z=-2d0*scale*(c*ebr-f*(1d0+c)*efbr)  ! z*r = ds/dr
  g1=3d0*s**2
  g2=6d0*s*(z*r)**2-g1*z+4d0*g1*(c*ebr-(1d0+c)*f*f*efbr)*scale*br2
  g1=g1*z*r

case(8,9,10,11)
!  Special damping functions 8-11
!  Pade damping function
!        c_m x^3 + ... + c_{2m-4} x^{m-1} + x^m
!  -----------------------------------------------------, x=r^2
!  c_0 + c_1 x + c_2 x^2 + ... + c_{m-1} x^{m-1} + x^m
  m=type-5 ! 2m = order of Pade
  call Pade(m,.false., dampf, r, g,g1,g2)

case(12,13,14,15)
!  Special damping functions 12-15
!  Shifted Pade damping function
!        c_m + c_(m+1) x + ... + c_{2m-4} x^{m-4} + x^(m-3)
!  r^6 -----------------------------------------------------, x=r^2+a^2
!      c_0 + c_1 x + c_2 x^2 + ... + c_{m-1} x^{m-1} + x^m
  m=type-9 ! 2m = order of Pade
  call Pade(m,.true., dampf, r, g,g1,g2)

case(20)
  !  Tkatchenko-DiStasio damping
  g = 1d0 - exp(-(r/scale)**2)
  g1 = (2d0*r/scale**2)*(1d0-g)
  g2 = g1/r + (2d0*r/scale**2)*g1

case default
  call die('Unrecognized damping type',.false.)
end select

END SUBROUTINE damp

!-----------------------------------------------------------------------

SUBROUTINE Tang_Toennies(type,b,n,new, r, g,g1,g2)
!  Tang-Toennies: incomplete gamma function of order n+1 for R^(-n)
!  interaction; i.e. g=P(scale*r,n+1). The scale factor is as supplied
!  in the data file for type 1; for type 2 it is multiplied by the current
!  value of the repulsion parameter alpha in the calling program.
!  For type 16 the damping function is the square root of the TT function.
!  type:  Type of damping function.
!  b:     Damping factor
!  n:     Power of 1/R to be damped.
!  new:   True if this calculation starts from scratch, false if a
!         previous call with the same b and r and smaller n has evaluated
!         intermediate quantities that can be re-used.
!  g:     Damping factor for R^{-n}.
!  g1,g2: Derivatives of g w.r.t. R.

USE input, ONLY: die

INTEGER, INTENT(IN) :: type, n
DOUBLE PRECISION, INTENT(IN) :: b, r
LOGICAL, INTENT(IN) :: new
DOUBLE PRECISION, INTENT(OUT) :: g, g1, g2

DOUBLE PRECISION :: br, denom, epr, fn, g1p, g2p, pr, pr1, pr2, s, z
INTEGER :: k, n0
SAVE n0, z, pr, pr1, pr2, epr, fn, s

!  Types:
!  1,2    TT: Standard Tang-Toennies, constant b
!  16     SqrtTT: Sqrt Tang-Toennies (as used in ASP-W)
!  21,22  Modified Tang-Toennies, b = -d(ln V_er)/dR
!  21     b = (br**2*(1d0 + br)) / (3d0 + br*(3d0 + br)), b from data
!  22     as above, br = alpha*r
!  24     b = alpha - ((7/alpha)-1)/r
  if (new) then
    !  Starting from scratch
    br = b*r
    select case(type)
    case(1,2,16)
      pr = b*r
      pr1 = b
      pr2 = 0d0
    case(21,22)
      denom = 3d0 + br*(3d0 + br)
      !  p is the modified b; pr = p*r
      pr = (br**2*(1d0 + br)) / denom
      !  First and second derivatives w.r.t. r.
      pr1 = b*br*(6d0+br*(12d0+br*(6d0+br)))/denom**2
      pr2 = 6d0*b**2*(3d0+br*(3d0+br)**2)/denom**3
    case(24)
      
      pr = b - ((7d0/b)-1d0)/r
      pr1 = ((7d0/b)-1d0)/r**2
      pr2 = -2d0*pr1/r
    case default
      print "(a)", "Program error in Tang_Toennies"
      call die("Please consult program developer")
    end select
    epr=exp(-pr)
    s=(1d0+pr)*epr
    z=epr*pr
    fn=1d0
    do k=2,n
      fn=fn+1d0
      z=z*pr/fn
      s=s+z
    end do
  else
    !  Continuing from previous calculation
    do k=n0+1,n
      fn=fn+1d0
      z=z*pr/fn
      s=s+z
    end do
  endif
  !  z is now exp(-pR)*(pR)^n/n!
  n0=n
  g = (1d0-s)
  !  Derivatives w.r.t. pr
  g1p = z
  g2p = z*(fn/pr-1d0)
  !  Derivatives w.r.t. r
  g1 = g1p*pr1
  g2 = g2p*pr1**2 + g1p*pr2
  if (type == 16) then
    g = sqrt(g)
    g1 = 0.5d0*g1/g
    g2 = 0.5d0*g2/g - 0.25d0*g1**2/g**3
  end if

END SUBROUTINE Tang_Toennies

!-----------------------------------------------------------------------

SUBROUTINE HFD(n,r,g,g1,g2)

!  HFD damping function

INTEGER, INTENT(IN) :: n
DOUBLE PRECISION, INTENT(IN) :: r
DOUBLE PRECISION, INTENT(OUT) :: g, g1, g2

DOUBLE PRECISION :: fn, a1, a2, b1, b2, t1, t2, t4, t6, t8, t11, t12,   &
    t13, t14, t16, t18, t21, t24, t28, t37

fn = n
a1 = 1.8817d0*0.529177d0/fn
a2 = 0.2475*0.529177d0**2/sqrt(fn)
b1 = 0.936067
b2 = 0.94833855
!  Maple code, slightly modified
t1 = r**2.236D0
t2 = b1*t1
t4 = dexp(-b2*r)
t6 = 1-t2*t4
t8 = r**2
t11 = dexp(-a1*r-a2*t8)
t12 = 1-t11
g = t6*t12
t13 = r**1.236D0
t14 = b1*t13
t16 = b2*t4
t18 = -2.236D0*t14*t4+t2*t16
t21 = -a1-2*a2*r
g1 = t18*t12-t6*t21*t11
t24 = r**0.236D0
t28 = b2**2
t37 = t21**2
g2 = (-2.763696D0*b1*t24*t4+4.472D0*t14*t16-t2*t28*t4)*t12       &
    -2D0*t18*t21*t11+2D0*t6*a2*t11-t6*t37*t11

END SUBROUTINE HFD

!-----------------------------------------------------------------------

SUBROUTINE Pade(m, shift, dampf, r, g,g1,g2)

INTEGER, INTENT(IN) :: m
LOGICAL, INTENT(IN) :: shift
DOUBLE PRECISION, INTENT(IN) :: dampf(0:), r
DOUBLE PRECISION, INTENT(OUT) :: g, g1, g2

DOUBLE PRECISION :: x, d, d1, d2, top, top1, top2
INTEGER :: i

if (shift) then

  !  Shifted Pade damping function
  !        c_m + c_(m+1) x + ... + c_{2m-4} x^{m-4} + x^(m-3)
  !  r^6 -----------------------------------------------------, x=r^2+a^2
  !      c_0 + c_1 x + c_2 x^2 + ... + c_{m-1} x^{m-1} + x^m
  x=r**2+dampf(2*m-3)**2
  d=1d0
  d1=m
  d2=m*(m-1d0)
  top=1d0
  top1=m-3
  top2=(m-3)*(m-4)
  do i=m-1,0,-1
    d=d*x+dampf(i)
    if (i .ge. 1) d1=d1*x+i*dampf(i)
    if (i .ge. 2) d2=d2*x+i*(i-1)*dampf(i)
  end do
  do i=m-4,0,-1
    top=top*x+dampf(m+i)
    if (i .ge. 1) top1=top1*x+i*dampf(m+i)
    if (i .ge. 2) top2=top2*x+i*(i-1)*dampf(m+i)
  end do
  ! These are derivatives w.r.t. x. Convert into derivatives w.r.t. r.
  d2=4d0*r**2*d2+2*d1; d1=2d0*r*d1
  top2=4d0*r**2*top2+2d0*top1; top1=2d0*r*top1

  g=top/d
  g1=(d*top1-top*d1)/d**2
  g2=(d**2*top2-2d0*(top1*d1*d-top*d1**2)-top*d2*d)/d**3

  g2=30d0*r**4*g+12d0*r**5*g1+r**6*g2
  g1=6d0*r**5*g+r**6*g1
  g=r**6*g

else

!  Pade damping function
!        c_m x^3 + ... + c_{2m-4} x^{m-1} + x^m
!  -----------------------------------------------------, x=r^2
!  c_0 + c_1 x + c_2 x^2 + ... + c_{m-1} x^{m-1} + x^m

  x=r**2
  d=1d0
  d1=m
  d2=m*(m-1d0)
  top=1d0
  top1=d1
  top2=d2
  do i=m-1,0,-1
    d=d*x+dampf(i)
    if (i .ge. 1) d1=d1*x+i*dampf(i)
    if (i .ge. 2) d2=d2*x+i*(i-1)*dampf(i)
  end do
  do i=m-1,3,-1
    top=top*x+dampf(m+i-3)
    top1=top1*x+i*dampf(m+i-3)
    top2=top2*x+i*(i-1)*dampf(m+i-3)
  end do
  top=top*x**3
  top1=top1*x**2
  top2=top2*x
  ! These are derivatives w.r.t. x. Convert into derivatives w.r.t. r.
  d2=4*x*d2+2*d1; d1=2*r*d1
  top2=4*x*top2+2*top1; top1=2*r*top1

  g=top/d
  g1=(d*top1-top*d1)/d**2
  g2=(d**2*top2-2d0*(top1*d1*d-top*d1**2)-top*d2*d)/d**3

end if

END SUBROUTINE Pade

!=======================================================================

SUBROUTINE dampp(type,dampf,n,alpha,new, r, ndp, g,g1,g2)

!  Calculate the damping function and its derivatives with respect to
!  the damping-function parameters, for use in the fit.F90 module.

USE input, ONLY : die
IMPLICIT NONE

INTEGER, INTENT(IN) :: type, n
DOUBLE PRECISION, INTENT(IN) :: dampf(0:), alpha, r
LOGICAL, INTENT(IN) :: new
INTEGER, INTENT(OUT) :: ndp
DOUBLE PRECISION, INTENT(OUT) :: g, g1(0:), g2(0:,0:)

DOUBLE PRECISION :: scale, br, br2, ebr, efbr, c, f, s, q, &
    dsdb, dsdc, dsdf, x, d, top, power(0:10), r6, z


INTEGER :: i, j, m

scale=dampf(0)
select case(type)
case(0:2,5)
  ndp=0
  call damp(type,dampf,n,alpha,new, r, g,g1(0),g2(0,0))
  g1(0)=g1(0)*r/scale
  g2(0,0)=g2(0,0)*(r/scale)**2

case(6)
  if (R < 0.1d0*scale) then
    g=0d0
    g1=0d0
    g2=0d0
  else if (R > scale) then
    g=1d0
    g1=0d0
    g2=0d0
  else
    z=scale/R
    g=exp(-(z-1)**2)
    g1=-(2d0/R)*(z-1d0)*g
    g2=(4d0*(z-1d0)**2-2d0)*g/R**2
  endif


case(7)
  !  Special damping function 7: exponential combination damping
  ndp=2
  c=dampf(1)
  f=dampf(2)
  br=scale*r
  ebr=exp(-br)
  efbr=exp(-f*br)
  s=(1d0+c*ebr-(1d0+c)*efbr)
  g=s**6
  dsdb=-r*(c*ebr-f*(1d0+c)*efbr)  ! ds/db
  dsdc=ebr-efbr
  dsdf=(1d0+c)*br*efbr
  q=6d0*s**5
  g1(0)=q*dsdb
  g2(0,0)=30d0*s**4*dsdb**2+q*(c*ebr-(1d0+c)*f*f*efbr)*r*r
  g1(1)=q*dsdc
  g1(2)=q*dsdf
  g2(0,1)=30d0*s**4*dsdb*dsdc-q*r*(ebr-f*efbr)
  g2(0,2)=30d0*s**4*dsdb*dsdf+q*(1d0+c)*(r-f*r*br)*efbr
  g2(1,1)=30d0*s**4*dsdc**2
  g2(1,2)=30d0*s**4*dsdc*dsdf+q*br*efbr
  g2(2,2)=30d0*s**4*dsdf**2-q*(1d0+c)*br**2*efbr
  g2(1,0)=g2(0,1)
  g2(2,0)=g2(0,2)
  g2(2,1)=g2(1,2)

case(17)
  !  Special damping function 17: gaussian combination damping
  ndp=2
  c=dampf(1)
  f=dampf(2)
  br2=scale*r**2
  ebr=exp(-br2)
  efbr=exp(-f*br2)
  s=(1d0+c*ebr-(1d0+c)*efbr)
  g=s**3
  dsdb=-r**2*(c*ebr-f*(1d0+c)*efbr)  ! ds/db
  dsdc=ebr-efbr
  dsdf=(1d0+c)*(br2)*efbr
  q=3d0*s**2
  g1(0)=q*dsdb
  g1(1)=q*dsdc
  g1(2)=q*dsdf
  g2(0,0)=6d0*s*dsdb**2+q*r**4*(c*ebr-(1d0+c)*f*f*efbr)
  g2(0,1)=6d0*s*dsdb*dsdc-q*r**2*(ebr-f*efbr)
  g2(0,2)=6d0*s*dsdb*dsdf+q*(1d0+c)*r**2*(1d0-f*br2)*efbr
  g2(1,1)=6d0*s*dsdc**2
  g2(1,2)=6d0*s*dsdc*dsdf+q*br*efbr
  g2(2,2)=6d0*s*dsdf**2-q*(1d0+c)*br2**2*efbr
  g2(1,0)=g2(0,1)
  g2(2,0)=g2(0,2)
  g2(2,1)=g2(1,2)

case(8,9,10,11)
  !  Pade
  !        c_m x^3 + ... + c_{2m-4} x^{m-1} + x^m
  !  -----------------------------------------------------, x=r^2
  !  c_0 + c_1 x + c_2 x^2 + ... + c_{m-1} x^{m-1} + x^m
  x=r**2
  m=type-5 ! 2m = order of Pade
  ndp=2*m-4
  power(0)=1d0
  do i=1,2*m-2
    power(i)=power(i-1)*x ! Note -- powers of r**2
  end do
  d=power(m)
  top=power(m)
  do i=m-1,0,-1
    d=d+dampf(i)*power(i)
    if (i .ge. 3) then
      top=top+dampf(m+i-3)*power(i)
    endif
  end do

  g=top/d
  do i=0,m-1
    g1(i)=-power(i)*(top/d**2)
    do j=0,i
      g2(i,j)=(2d0*top/d**3)*power(i+j)
      g2(j,i)=g2(i,j)
    end do
    do j=m,2*m-4
      g2(i,j)=-power(i+j+3-m)/d**2
      g2(j,i)=g2(i,j)
    end do
  end do
  do i=m,2*m-4
    g1(i)=power(i+3-m)/d
    do j=m,2*m-4
      g2(i,j)=0d0; g2(j,i)=0d0
    end do
  end do

case(12,13,14,15)
  !  Shifted Pade
  !        c_m + c_(m+1) x + ... + c_{2m-4} x^{m-4} + x^(m-3)
  !  r^6 -----------------------------------------------------, x=r^2+a^2
  !      c_0 + c_1 x + c_2 x^2 + ... + c_{m-1} x^{m-1} + x^m
  m=type-9 ! 2m = order of Pade
  x=r**2+dampf(2*m-3)**2
  ndp=2*m-3
  power(0)=1d0
  do i=1,2*m-2
    power(i)=power(i-1)*x ! Note -- powers of r**2+a**2
  end do
  d=power(m)
  top=power(m-3)
  do i=m-1,0,-1
    d=d+dampf(i)*power(i)
  end do
  do i=m-4,0,-1
    top=top+dampf(m+i)*power(i)
  end do
  r6=r**6
  top=top*r6

  g=top/d
  do i=0,m-1
    g1(i)=-power(i)*(top/d**2)
    do j=0,i
      g2(i,j)=(2d0*top/d**3)*power(i+j)
      g2(j,i)=g2(i,j)
    end do
    do j=m,2*m-4
      g2(i,j)=-r6*power(i+j+3-m)/d**2
      g2(j,i)=g2(i,j)
    end do
  end do
  do i=m,2*m-4
    g1(i)=r6*power(i+3-m)/d
    do j=m,2*m-4
      g2(i,j)=0d0; g2(j,i)=0d0
    end do
  end do
case default
  call die('Unrecognized damping type',.false.)
end select

END SUBROUTINE dampp

!-----------------------------------------------------------------------

SUBROUTINE check_dampp(type,dampf,n,alpha, r,ndp, g,g1,g2)

!  Check analytic damping function derivatives in g1 and g2 by numerical
!  finite difference. The dampp routine should have been called before
!  this one.

USE input, ONLY : die
IMPLICIT NONE

!  type:  Type of damping function.
!  n:     Power of 1/R to be damped.
!  alpha: Repulsion steepness parameter (for use in type 2 damping).
!  dampf: Parameters of damping function. Usually just the scale factor
!         dampf(0).
!  ndp:   Number of damping parameters used by the damping function, less 1.
!  new:   True if this calculation starts from scratch, false if a
!         previous call has evaluated intermediate quantities that can
!         be re-used.
!  R:     Value of R.
!  g:     Damped R^{-n}.
!  g1,g2: Derivatives of g w.r.t. R.
INTEGER, INTENT(IN) :: type, n
DOUBLE PRECISION, INTENT(IN) :: dampf(0:), alpha, r
INTEGER, INTENT(IN) :: ndp
DOUBLE PRECISION, INTENT(IN) :: g, g1(0:), g2(0:,0:)

DOUBLE PRECISION :: gplus, gminus, g1plus(0:ndp), g1minus(0:ndp),    &
    h2(0:ndp,0:ndp), temp(0:ubound(dampf,1))

DOUBLE PRECISION :: g1n, g2n
DOUBLE PRECISION, PARAMETER :: eps=1E-7

LOGICAL error
INTEGER i, j, ndpx

temp(:)=dampf(:)
error=.false.
do i=0,ndp
  temp(i)=dampf(i)+eps
  call dampp(type,temp,n,alpha,.false., r, ndpx, gplus, g1plus, h2)
  temp(i)=dampf(i)-eps
  call dampp(type,temp,n,alpha, .false., r, ndpx, gminus, g1minus, h2)
  temp(i)=dampf(i)
  g1n=(gplus-gminus)/(2d0*eps)
  if (abs(g1n-g1(i)) > eps) then
    print "(a,i3,a,1p,e14.5,a,e14.5)",                             &
        "Damping function derivative error.", i,                  &
        "   Numerical: ", g1n, "  Analytic: ", g1(i)
    error=.true.
  endif
end do
if (error) call die                                            &
    ("First derivative errors in damping functions",.false.)

do i=0,ndp
  temp(i)=dampf(i)+eps
  call dampp(type,temp,n,alpha, .false., r, ndpx, gplus, g1plus, h2)
  temp(i)=dampf(i)-eps
  call dampp(type,temp,n,alpha, .false., r, ndpx, gminus, g1minus, h2)
  temp(i)=dampf(i)
  do j=0,ndp
    g2n=(g1plus(j)-g1minus(j))/(2d0*eps)
    if (abs(g2n-g2(i,j)) > eps) then
      print "(a,2i3,a,1p,e14.5,a,e14.5)",                          &
          "Damping function derivative error.", i, j,             &
          "   Numerical: ", g2n, "  Analytic: ", g2(i,j)
      error=.true.
    endif
  end do
end do
if (error) call die                                            &
    ("Second derivative errors in damping functions",.false.)

END SUBROUTINE check_dampp

END MODULE damping
