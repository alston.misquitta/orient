MODULE consts

IMPLICIT NONE

INTEGER, PARAMETER :: dp=kind(1d0)

!  Conversion factors. These are the numbers by which values of energy,
!  distance and angle held internally are to be multiplied to get
!  external values.

!  EHVKJM, EHVK, EHVCMM, EHVEV, EHVMEV, BOHRVA etc. are the actual values
!  used for the conversion factors.
DOUBLE PRECISION, PARAMETER ::                                    &
    ehvkjm=2625.5d0,         & ! Hartree / kJ/mol
    ehvk=315770d0,           & ! Hartree / Kelvin
    ehvcmm=219475d0,         & ! Hartree / cm-1
    ehvev=27.211396d0,       & ! Hartree / eV
    ehvmev=27211.396d0,      & ! Hartree / meV
    ehvghz=6.579683d6,       & ! Hartree / GHz
    MItoB=60.19969151d0,     & ! see below
    freqcv=5140.40d0,        & ! see below
    auvdebye=2.5417666D0,    & ! dipole moment a.u. / debye
    time2fs=1.032749811d0,   & ! see below
    mgc=8.3145d0               ! Molar Gas Constant, J/mol/K
!  (B/cm-1) = MItoB/(I/a.m.u. bohr^2); MItoB = hbar^2/(2.100hc.m_u.a_0^2)

!  FREQCV converts angular frequency in a.u.*m_e/m_p to wavenumber (cm-1)

!  time2fs converts times in program units to femtosec. The program unit
!  is the a.u. of time, i.e. 2.41888453185826e-2 fs, multiplied by 
!  sqrt(a.m.u./m_e) because the unit of mass in a.u. is m_e but the
!  program works with masses in atomic mass units.

DOUBLE PRECISION, PARAMETER ::                                    &
    jveh=4.35975D-18,        & ! J/H  (pressure in Pascal!)
    amuvkg=1.660540d-27,     & ! amu / kg
    planck=6.626076d-34,     & ! Js
    bohrva=0.52917725d0,     & ! bohr / Angstrom
    bohrvm=0.52917725d-10      ! bohr / metre

DOUBLE PRECISION, PARAMETER :: pi=3.141592653589793238462643d0,   &
    sqrtpi=1.772453850905516027298167d0,                          &
    rthalf=0.5d0*1.4142135623730950488d0

!  Conversion factor for output of energies (default: Hartree)
DOUBLE PRECISION :: efact=1d0
CHARACTER*8 :: eunit='hartree'
CHARACTER*8 :: efmt='F12.8'
!  Conversion factor for input and output of distances (default: Bohr)
DOUBLE PRECISION :: rfact=1d0
CHARACTER*8 :: runit='bohr'
!  Conversion factor for input and output of angles (always degrees
!  externally and radians internally, though some routines allow a unit
!  to be specified).
DOUBLE PRECISION :: afact=180d0/pi
SAVE efact, rfact, afact, eunit, efmt, runit

!  ESLIMIT is the maximum power of 1/R to be kept in electrostatic
!       interactions.
INTEGER, SAVE :: eslimit=5
CHARACTER(LEN=8), DIMENSION(0:8,2), SAVE :: name
CHARACTER(LEN=6) :: which(2)=["length","energy"]
DATA name(0:8,1) /9*"BOHR"/, name(0:8,2) /9*"HARTREE"/
INTEGER, DIMENSION(2), SAVE :: ptr=0

PUBLIC
PRIVATE name

CONTAINS

SUBROUTINE unit

!  UNIT[S] POP
!  UNIT[S] name [name]
!  Read a word from the current input line. If it's a recognised unit
!  name, push the stack and set the unit conversion accordingly. If it's
!  "pop", revert to the previous units. If more than one unit is given,
!  only push the stack once. It doesn't make sense to give more than two
!  unit names (one for length, one for energy) but if names conflict, the
!  last one is used.

USE input, ONLY : reada, item, nitems, upcase
IMPLICIT NONE

CHARACTER(LEN=10) :: word
INTEGER :: k
LOGICAL :: pushed

pushed=.false.
do while (item .lt. nitems)
  call reada(word)
  select case(upcase(word))
  case ("POP")
    !  POP [POP ...]
    !  Pop the current energy and length units off the list and revert
    !  to the previous units. May be repeated if appropriate.
    call pop_unit
  case("LENGTH","ENERGY")
    !  Ignore (for compatibility with previous syntax)
  case default
    !  A unit name. If conflicting names are given, the last is used.
    !  Fudge to permit, e.g. Angstrom^2
    if (word(1:8)=="ANGSTROM") then
      ! length units
      k=1
      word=word(1:8)
    else if (word(1:4)=="BOHR") then
      k=1
      word=word(1:4)
    else
      ! energy units
      k=2
    end if
    if (.not. pushed) then
      call push_unit
      pushed=.true.
    end if
    name(0,k)=word
    call set_unit(word)
  end select
end do

END SUBROUTINE unit

SUBROUTINE set_unit(word)

USE input, ONLY : reada, upcase, die
CHARACTER(LEN=*), INTENT(IN) :: word

select case(upcase(word))

  !  Length
case('ANGSTROM','A')
  rfact=bohrva
  runit='angstrom'
case('BOHR','B')
  rfact=1d0
  runit='bohr'

  !  Energy
case('KJ/MOL')
  efact=ehvkjm
  eunit='kJ/mol'
  efmt='f12.6'
case('KELVIN','K')
  efact=ehvk
  eunit='K'
  efmt='f12.2'
case('CM-1')
  efact=ehvcmm
  eunit='cm-1'
  efmt='f12.2'
case('EV')
  efact=ehvev
  eunit='eV'
  efmt='f12.4'
case('MEV')
  efact=ehvmev
  eunit='meV'
  efmt='f12.4'
case('HARTREE')
  efact=1d0
  eunit='hartree'
  efmt='f12.8'
case('MILLIHARTREE','MH')
  efact=1d3
  eunit='mH'
  efmt='f12.6'
case("FORMAT")
  call reada(efmt)
case default
  call die ('Unit not recognized: '//word,.true.)
end select

!       if (k .eq. 1) then
!         print "(A,A)", "Length unit selected: ", runit
!       else
!         print "(A,A)", "Energy unit selected: ", eunit
!       endif

END SUBROUTINE set_unit

DOUBLE PRECISION FUNCTION unit_factor(name)

USE input, ONLY : die, upcase
!  Function value is the factor relating unit name to internal
!  units (i.e. bohr or hartree). External = factor * internal.
CHARACTER(LEN=*), INTENT(IN) :: name

select case(upcase(name))
  !  Length
case('ANGSTROM')
  unit_factor=bohrva
case('BOHR')
  unit_factor=1d0

  !  Energy
case('KJ/MOL')
  unit_factor=ehvkjm
case('KELVIN','K')
  unit_factor=ehvk
case('CM-1')
  unit_factor=ehvcmm
case('EV')
  unit_factor=ehvev
case('MEV')
  unit_factor=ehvmev
case('HARTREE')
  unit_factor=1d0
case('MILLIHARTREE','MH')
  unit_factor=1d3
case default
  call die ('Unit not recognized: '//name,.true.)
end select
END FUNCTION unit_factor

SUBROUTINE push_unit

name(1:8,1:2)=name(0:7,1:2)
name(0,1:2)=name(1,1:2)

END SUBROUTINE push_unit

SUBROUTINE pop_unit

name(0:7,1:2)=name(1:8,1:2)
name(8,1)="bohr"
name(8,2)="hartree"
call set_unit(name(0,1))
call set_unit(name(0,2))

END SUBROUTINE pop_unit

END MODULE consts

