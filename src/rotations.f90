MODULE rotations

!  quater(r,q)
!    Finds quaternion parameters q for a rotation described by an orthogonal
!    matrix r.
!  angleaxis(r,p)
!    Express a rotation matrix r in angle-axis form, as the rotation vector p.
!  aatoq(p,q)
!    Express an angle-axis vector p as quaternion q.
!  qtoaa(q,p)
!    Express a quaternion q as angle-axis vector p.
!  rotmat(psi,x,r)
!    Construct the matrix for a rotation through psi about the direction
!    defined by x, and return it in r. x need not be a unit vector.
!  eulermat(euler,r)
!    Given the euler angles euler(1:3)=[alpha,beta,gamma], return the
!    corresponding rotation matrix in r.
!  eulerquat(euler,q)
!    Given the euler angles euler(1:3)=[alpha,beta,gamma], return the
!    corresponding quaternion in q.
!  aamat(p,r)
!    Construct the matrix for a rotation described by the angle-axis
!    vector p, and return it in r.
!  quatmat(q,r)
!    Calculates the rotation matrix r corresponding to quaternion q.
!  qqmult(q1,q2,q)
!    Set quaternion q to the product of quaternions q1 and q2.
!  aamult(p1,p2,p)
!    From two angle-axis rotations p1 and p2, construct the product p=p1*p2.
!  wigner(r, d, lmax)
!    Takes a rotation matrix r for ordinary 3-d vectors (a matrix of
!    direction cosines) and returns in d the matrix required to transform
!    the set of angular momentum functions C00, C10, C11c, C11s, C20, ...,
!    CLLc, CLLs, with L=lmax.
!  readcg
!    Sets up coupling coefficients for spherical tensors of rank 1 and j2
!    expressed in real form, to give coupled tensors also in real form.
!  read_rotation(p,[r])
!    Read a rotation from the input as Euler angles alpha, beta, gamma
!    or as "BY psi ABOUT nx ny nz". Angles in degrees. The vector
!    (nx,ny,nz) need not be a unit vector; it will be normalized if not.
!    The rotation is returned as an angle-axis vector and optionally as
!    the corresponding rotation matrix.

IMPLICIT NONE

PRIVATE
PUBLIC quater, angleaxis, aamult, qtoaa, aatoq, rotmat, eulermat,       &
    eulerquat, aamat, quatmat, qqmult, read_rotation, wigner

INTEGER, PARAMETER :: dp=kind(1d0)
! DOUBLE PRECISION, PARAMETER :: pi=3.14159265358979d0
TYPE realcg
  INTEGER, POINTER, DIMENSION(:,:) :: data => null() 
  COMPLEX(KIND=dp), POINTER, DIMENSION(:,:,:) :: p=>null()
END TYPE realcg
TYPE(realcg), SAVE :: cg(4)
LOGICAL, SAVE :: cgread=.false.

CONTAINS

SUBROUTINE quater(r,a)

!  Finds quaternion parameters for a rotation described by an orthogonal
!  matrix

DOUBLE PRECISION, INTENT(IN) :: r(3,3)
DOUBLE PRECISION, INTENT(OUT) :: a(0:3)

INTEGER :: i, j, k
DOUBLE PRECISION :: s, z

!  Check that it's a valid proper rotation matrix
call check_rotation(r)

!  Find cosine of half rotation angle
z=r(1,1)+r(2,2)+r(3,3)+1
if (z .gt. 0d0) then
  a(0)=0.5*sqrt(z)
else
  a(0)=0d0
endif
if (a(0) .gt. 1d-4) then
  !  No problem
  a(1)=0.25d0*(r(3,2)-r(2,3))/a(0)
  a(2)=0.25d0*(r(1,3)-r(3,1))/a(0)
  a(3)=0.25d0*(r(2,1)-r(1,2))/a(0)
else
  !  Rotation angle close to pi
  s=1-r(1,1)-r(2,2)-r(3,3)
  do i=1,3
    z=2d0*r(i,i)+s
    if (z .gt. 0d0) then
      a(i)=0.5d0*sqrt(z)
    else
      a(i)=0d0
    endif
  end do
  !  This just gives the magnitudes of the a(i). We need to get the signs
  !  right.
  !  Find the largest a(i).
  i=1
  if (a(2)>a(i)) i=2
  if (a(3)>a(i)) i=3
  j=mod(i,3)+1
  k=mod(j,3)+1
  if (r(j,k)<r(k,j)) a(i)=-a(i)
  if ((r(i,j)+r(j,i))*a(i)<0d0) a(j)=-a(j)
  if ((r(i,k)+r(k,i))*a(i)<0d0) a(k)=-a(k)
  ! if (r(2,1) .lt. r(1,2)) a(3)=-a(3)
  ! if (r(3,2) .lt. r(2,3)) a(1)=-a(1)
  ! if (r(1,3) .lt. r(3,1)) a(2)=-a(2)
!  else
!  Rotation angle exactly pi. Choose one sign arbitrarily and others
!  relative to it.
!     if (a(1) .gt. 0.5d0) then
!       if (r(1,2)+r(2,1) .lt. 0d0) a(2)=-a(2)
!       if (r(1,3)+r(3,1) .lt. 0d0) a(3)=-a(3)
!     else if (a(2) .gt. 0.5d0) then
!       if (r(1,2)+r(2,1) .lt. 0d0) a(1)=-a(1)
!       if (r(2,3)+r(3,2) .lt. 0d0) a(3)=-a(3)
!     else if (a(3) .gt. 0.5d0) then
!       if (r(1,3)+r(3,1) .lt. 0d0) a(1)=-a(1)
!       if (r(2,3)+r(3,2) .lt. 0d0) a(2)=-a(2)
!     endif
!   endif
end if

END SUBROUTINE quater

!----------------------------------------------------------------------

SUBROUTINE angleaxis(r, p)

!  Express a rotation matrix in angle-axis form, as the rotation vector p.

DOUBLE PRECISION, INTENT(IN) :: r(3,3)
DOUBLE PRECISION, INTENT(OUT) :: p(3)

DOUBLE PRECISION :: phi, s

!  Check that it's a valid proper rotation matrix
call check_rotation(r)

phi=acos(max(-1d0,min(1d0,0.5d0*(r(1,1)+r(2,2)+r(3,3)-1d0))))
!  print "(a,f12.8)", "Angle-axis: phi = ", phi
!  0 < phi < pi
s=sin(phi)
if (s > 1d-6) then
  !  Straightforward
  p(1)=0.5d0*(r(3,2)-r(2,3))*phi/s
  p(2)=0.5d0*(r(1,3)-r(3,1))*phi/s
  p(3)=0.5d0*(r(2,1)-r(1,2))*phi/s
else
  if (phi<0.1) then
    !  phi \approx s \approx 0
    p(1)=0.5d0*(r(3,2)-r(2,3))*(1d0+phi**2/6d0)
    p(2)=0.5d0*(r(1,3)-r(3,1))*(1d0+phi**2/6d0)
    p(3)=0.5d0*(r(2,1)-r(1,2))*(1d0+phi**2/6d0)
  else
    !  phi \approx pi
    !  Unit vector component magnitudes
    !  See Altmann p. 75
    p(1)=sqrt(max(0d0,-0.5d0*(r(2,2)+r(3,3))))
    p(2)=sqrt(max(0d0,-0.5d0*(r(3,3)+r(1,1))))
    p(3)=sqrt(max(0d0,-0.5d0*(r(1,1)+r(2,2))))
    p=p/sin(0.5d0*phi)
    !  Take p(3)>0 (arbitrary)
    if (r(1,3)+r(3,1) < 0d0) p(1)=-p(1)
    if (r(2,3)+r(3,2) < 0d0) p(2)=-p(2)
    p=p*phi
  end if
end if
! print "(a,3f12.8)", "Angle-axis: p = ", p


END SUBROUTINE angleaxis

!----------------------------------------------------------------------

SUBROUTINE check_rotation(r)

!  Check that we have a true proper rotation matrix

USE input, ONLY: die

DOUBLE PRECISION, INTENT(IN) :: r(3,3)

INTEGER :: i, j, k
DOUBLE PRECISION :: s

do i=1,3
  do j=1,3
    s=0d0
    do k=1,3
      s=s+r(i,k)*r(j,k)
    end do
    if (i .eq. j) s=s-1d0
    if (abs(s) .gt. 1d-5) then
      print "(3f12.8)", r
      call die ('Matrix is not orthogonal')
    end if
    if (abs(s) .gt. 1d-10) then
      print '(/a)','WARNING: Matrix is not exactly orthogonal'
      print '(A,1P,E12.4/)','Product should be zero but is ', s
    end if
  end do
end do


s=r(1,1)*r(2,2)*r(3,3)+r(1,2)*r(2,3)*r(3,1)+r(1,3)*r(2,1)*r(3,2)-      &
    r(1,3)*r(2,2)*r(3,1)-r(1,2)*r(2,1)*r(3,3)-r(1,1)*r(2,3)*r(3,2)
if (abs(s-1d0) .gt. 1d-10) then
  print '(1x,a,1p,e12.4)', 'Checking rotation matrix. Determinant =', s
  if (abs(s-1d0) .gt. 1d-5) then
    call die('Determinant is not unity')
  end if
end if

END SUBROUTINE check_rotation

!----------------------------------------------------------------------

SUBROUTINE aamult(p1,p2,p)

!  From two angle-axis rotations p1 and p2, construct the product
!  p=p1*p2

DOUBLE PRECISION, INTENT(IN) :: p1(3), p2(3)
DOUBLE PRECISION, INTENT(OUT) :: p(3)

DOUBLE PRECISION :: q1(0:3), q2(0:3), q(0:3)

call aatoq(p1,q1)
call aatoq(p2,q2)
call qqmult(q1,q2,q)
call qtoaa(q,p)

END SUBROUTINE aamult

!----------------------------------------------------------------------

SUBROUTINE aatoq(p,q)

!  Angle-axis vector p to quaternion q

DOUBLE PRECISION, INTENT(IN) :: p(3)
DOUBLE PRECISION, INTENT(OUT) :: q(0:3)

DOUBLE PRECISION :: phi

phi=sqrt(p(1)**2+p(2)**2+p(3)**2)
if (phi<1d-6) then
  q(1:3)=p*(0.5d0+phi**2/48d0)  ! next term phi**4/3840
else
  q(1:3)=p*sin(0.5d0*phi)/phi
end if
q(0)=cos(0.5d0*phi)

END SUBROUTINE aatoq

!----------------------------------------------------------------------

SUBROUTINE qtoaa(q,p)

!  Quaternion q to angle-axis vector p

DOUBLE PRECISION, INTENT(IN) :: q(0:3)
DOUBLE PRECISION, INTENT(OUT) :: p(3)

DOUBLE PRECISION :: s, t

t=sqrt(q(0)**2+q(1)**2+q(2)**2+q(3)**2)  !  Should be 1 but might not be
s=sqrt(q(1)**2+q(2)**2+q(3)**2)/t
!  s=sin(phi/2), with phi>=0; (nx,ny,nz)=q(1:3)/s
if (s<1d-6) then
  p(1:3)=2d0*(1d0+s**2/6d0+3d0*s**4/40d0)*q(1:3)/t
else
  p(1:3)=2d0*asin(s)*q(1:3)/(s*t)
end if

END SUBROUTINE qtoaa

!----------------------------------------------------------------------

SUBROUTINE aamat(p,f)

!  Construct the matrix for a rotation described by the angle-axis
!  vector p, and return it in F.

IMPLICIT NONE

DOUBLE PRECISION, INTENT(IN) :: p(3)
DOUBLE PRECISION, INTENT(OUT) :: f(3,3)

DOUBLE PRECISION :: psi

psi=sqrt(p(1)**2+p(2)**2+p(3)**2)
call rotmat(psi, p, f)

END SUBROUTINE aamat

!----------------------------------------------------------------------

SUBROUTINE rotmat(psi, x, f)

!  Construct the matrix for a rotation through psi about the direction
!  defined by X, and return it in F.

USE input, ONLY: die
IMPLICIT NONE

DOUBLE PRECISION, INTENT(IN) :: psi, x(3)
DOUBLE PRECISION, INTENT(OUT) :: f(3,3)
DOUBLE PRECISION :: s, t, n(3), xx
INTEGER :: i

! print "(a,f10.5,a,3f10.5)", "rotmat: psi = ", psi, ", x = ", x
xx=sqrt(x(1)**2+x(2)**2+x(3)**2)
if (xx < 1d-8) then
  if (psi == 0d0) then
    f=0d0
    do i=1,3
      f(i,i)=1d0
    end do
    return
  else
    call die ("Attempt to rotate about zero vector")
  end if
end if

do i=1,3
  n(i)=x(i)/xx
end do

s=sin(psi)
t=2d0*sin(0.5*psi)**2

do i=1,3
  f(i,i)=n(i)**2*t+1d0-t
end do
f(1,2)=n(1)*n(2)*t-n(3)*s
f(2,1)=n(1)*n(2)*t+n(3)*s
f(2,3)=n(2)*n(3)*t-n(1)*s
f(3,2)=n(2)*n(3)*t+n(1)*s
f(3,1)=n(3)*n(1)*t-n(2)*s
f(1,3)=n(3)*n(1)*t+n(2)*s

END SUBROUTINE rotmat

!----------------------------------------------------------------------

SUBROUTINE eulermat(euler,f)

DOUBLE PRECISION, INTENT(IN) :: euler(3)  !  alpha, beta, gamma
DOUBLE PRECISION, INTENT(OUT) :: f(3,3)

DOUBLE PRECISION :: cosa, sina, cosb, sinb, cosc, sinc

cosa=cos(euler(1))
sina=sin(euler(1))
cosb=cos(euler(2))
sinb=sin(euler(2))
cosc=cos(euler(3))
sinc=sin(euler(3))
f(1,1)=cosa*cosb*cosc-sina*sinc
f(2,1)=sina*cosb*cosc+cosa*sinc
f(3,1)=-sinb*cosc
f(1,2)=-cosa*cosb*sinc-sina*cosc
f(2,2)=-sina*cosb*sinc+cosa*cosc
f(3,2)=sinb*sinc
f(1,3)=cosa*sinb
f(2,3)=sina*sinb
f(3,3)=cosb

END SUBROUTINE eulermat

!----------------------------------------------------------------------

SUBROUTINE eulerquat(euler,q)

DOUBLE PRECISION, INTENT(IN) :: euler(3)  !  alpha, beta, gamma
DOUBLE PRECISION, INTENT(OUT) :: q(0:3)

DOUBLE PRECISION, DIMENSION(0:3) :: qa, qb, qc, qt

qa(0)=cos(0.5d0*euler(1))
qa(1:3)=[0,0,1]*sin(0.5d0*euler(1))
qb(0)=cos(0.5d0*euler(2))
qb(1:3)=[0,1,0]*sin(0.5d0*euler(2))
qc(0)=cos(0.5d0*euler(3))
qc(1:3)=[0,0,1]*sin(0.5d0*euler(3))
call qqmult(qb,qc,qt)
call qqmult(qa,qt,q)

END SUBROUTINE eulerquat

!----------------------------------------------------------------------

SUBROUTINE quatmat(q,rotate)

!  Calculates rotation matrix from quaternions
!  The convention used here is opposite to the one used in the book by
!  M.P. Allen & D.J. Tildesley, which is inconsistent with the convention
!  used in Orient, except in the MD and MC modules.

IMPLICIT NONE

DOUBLE PRECISION, INTENT(IN) :: q(0:3)
DOUBLE PRECISION, INTENT(OUT) :: rotate(3,3)

rotate(1,1)=q(0)**2+q(1)**2-q(2)**2-q(3)**2
rotate(1,2)=2.0d0*(q(1)*q(2)-q(0)*q(3))
rotate(1,3)=2.0d0*(q(1)*q(3)+q(0)*q(2))
rotate(2,1)=2.0d0*(q(1)*q(2)+q(0)*q(3))
rotate(2,2)=q(0)**2-q(1)**2+q(2)**2-q(3)**2
rotate(2,3)=2.0d0*(q(2)*q(3)-q(0)*q(1))
rotate(3,1)=2.0d0*(q(1)*q(3)-q(0)*q(2))
rotate(3,2)=2.0d0*(q(2)*q(3)+q(0)*q(1))
rotate(3,3)=q(0)**2-q(1)**2-q(2)**2+q(3)**2

END SUBROUTINE quatmat

!----------------------------------------------------------------------

SUBROUTINE qqmult(a,b,c)

!  Set quaternion c to the product of quaternions a and b.
!  It is permissible for argument c to be the same as a or b or both.

DOUBLE PRECISION, INTENT(IN) :: a(0:3), b(0:3)
DOUBLE PRECISION, INTENT(OUT) ::  c(0:3)

DOUBLE PRECISION :: q(0:3)

q(0)=a(0)*b(0)-a(1)*b(1)-a(2)*b(2)-a(3)*b(3)
q(1:3)=a(0)*b(1:3)+b(0)*a(1:3)
q(1)=q(1)+a(2)*b(3)-a(3)*b(2)
q(2)=q(2)+a(3)*b(1)-a(1)*b(3)
q(3)=q(3)+a(1)*b(2)-a(2)*b(1)
c=q

END SUBROUTINE qqmult

!----------------------------------------------------------------------

SUBROUTINE wigner(r, d, lmax)

!  Takes a rotation matrix R for ordinary 3-d vectors (a matrix of
!  direction cosines) and returns in D the matrix required to transform
!  the set of angular momentum functions C00, C10, C11c, C11s, C20, ...,
!  CLLc, CLLs. (This is a block diagonal matrix.) Note the order of the
!  functions; in particular C10 = z, C11c = x and C11s = y, so the rows and
!  columns of the rank 1 block of D are permuted from R. Definitions of the
!  other angular momentum functions (Racah spherical harmonics) are given in
!  The Theory of Intermolecular Forces, by A. J. Stone (Oxford University Press)

!  Lmax is the maximum rank for which the rotation matrix elements are required.

!  The array D needs to have dimension at least (Lmax+1)**2 square.

! USE parameters, ONLY : sq
! USE sizes
! USE sqrts
USE input, ONLY: die
IMPLICIT NONE

DOUBLE PRECISION, PARAMETER ::                                         &
    rt2=1.4142135623730950488d0, rt3=1.7320508075688772935d0,          &
    rt5=2.2360679774997896964d0, rt7=2.6457513110645905905d0
DOUBLE PRECISION, PARAMETER :: rt6=rt2*rt3, rt10=rt2*rt5, rt14=rt2*rt7, &
    rt15=rt3*rt5, rt21=rt3*rt7, rt30=rt2*rt3*rt5, rt35=rt5*rt7,        &
    rt42=rt2*rt3*rt7, rt70=rt2*rt5*rt7, rt105=rt3*rt5*rt7

INTEGER, INTENT(IN) :: lmax
DOUBLE PRECISION, INTENT(IN) :: R(3,3)
DOUBLE PRECISION, INTENT(OUT) :: d(:,:)

DOUBLE PRECISION :: xx, xy, xz, yx, yy, yz, zx, zy, zz, srow, scol
DOUBLE PRECISION :: dd(25,25)
LOGICAL :: test=.false., ok
INTEGER :: i, j, l, k, kk, q, qq, qmax, t, tt, base, jbase

if (.not. cgread) call read_cg

xx=r(1,1)
xy=r(1,2)
xz=r(1,3)
yx=r(2,1)
yy=r(2,2)
yz=r(2,3)
zx=r(3,1)
zy=r(3,2)
zz=r(3,3)

d=0d0

d(1,1) = 1d0

d(2,2) = zz
d(2,3) = zx
d(2,4) = zy
d(3,2) = xz
d(3,3) = xx
d(3,4) = xy
d(4,2) = yz
d(4,3) = yx
d(4,4) = yy

do l=2,lmax
  !  Couple j=l-1 with 1 to get l
  !  <lk|R|lk'> = sum(tt'qq') <1jtt'|lk>* <1t|R|1q> <jt'|R|jq'> <1jqq'|lk'>
  !  cg(j)%p(t,t',base+k)=<1jtt'|lk>
  !  These coupling coefficients are for the real components j0, j1c, j1s, ...
  j=l-1
  base=l**2
  jbase=j**2
  do k=1,2*l+1
    do kk=1,2*l+1
      do t=1,3
        do tt=1,2*j+1
          do q=1,3
            do qq=1,2*j+1
              d(base+k,base+kk)=d(base+k,base+kk)+                     &
                  conjg(cg(j)%p(t,tt,base+k))*cg(j)%p(q,qq,base+kk)* &
                  d(1+t,1+q)*d(jbase+tt,jbase+qq)
            end do
          end do
        end do
      end do
    end do
  end do
end do

if (test) then
  !  Check for orthogonality
  qmax = (lmax+1)**2
  ok=.true.
  do i=1,qmax
    do j=1,qmax
      srow=0d0
      scol=0d0
      do k=1,qmax
        srow=srow+d(i,k)*d(j,k)
        scol=scol+d(k,i)*d(k,j)
      end do
      if (i .eq. j) then
        srow=srow-1d0
        scol=scol-1d0
      endif
      if (abs(srow) .gt. 1d-8) then
        print '(1x,a,i3,a,i3,a,1p,e12.4)',                             &
            '   rows', i, ' and', j, ' are not orthogonal -- error', srow
        ok=.false.
      endif
      if (abs(scol) .gt. 1d-8) then
        print '(1x,a,i3,a,i3,a,1p,e12.4)',                             &
            'columns', i, ' and', j, ' are not orthogonal -- error', scol
        ok=.false.
      endif
    end do
  end do
  if (ok) print '(1x,a)', 'd matrix is orthogonal'

  !  Rotation matrices in explicit form. No longer used, but retained to
  !  provide a check on the systematic form above.
  dd=0d0

  if (lmax .ge. 2) then
    dd(5,5) = (3d0*zz**2-1d0)/2d0
    dd(5,6) = rt3*zx*zz
    dd(5,7) = rt3*zy*zz
    dd(5,8) = (rt3*(-2d0*zy**2-zz**2+1d0))/2d0
    dd(5,9) = rt3*zx*zy
    dd(6,5) = rt3*xz*zz
    dd(6,6) = 2d0*xx*zz-yy
    dd(6,7) = yx+2d0*xy*zz
    dd(6,8) = -2d0*xy*zy-xz*zz
    dd(6,9) = xx*zy+zx*xy
    dd(7,5) = rt3*yz*zz
    dd(7,6) = 2d0*yx*zz+xy
    dd(7,7) = -xx+2d0*yy*zz
    dd(7,8) = -2d0*yy*zy-yz*zz
    dd(7,9) = yx*zy+zx*yy
    dd(8,5) = rt3*(-2d0*yz**2-zz**2+1d0)/2d0
    dd(8,6) = -2d0*yx*yz-zx*zz
    dd(8,7) = -2d0*yy*yz-zy*zz
    dd(8,8) = (4d0*yy**2+2d0*zy**2+2d0*yz**2+zz**2-3d0)/2d0
    dd(8,9) = -2d0*yx*yy-zx*zy
    dd(9,5) = rt3*xz*yz
    dd(9,6) = xx*yz+yx*xz
    dd(9,7) = xy*yz+yy*xz
    dd(9,8) = -2d0*xy*yy-xz*yz
    dd(9,9) = xx*yy+yx*xy
  endif

  if (lmax .ge. 3) then
    dd(10,10) = (-8d0*xx*yy+8d0*yx*xy+5d0*zz**3+5d0*zz)/2d0
    dd(10,11) = (rt6*zx*(5d0*zz**2-1d0))/4d0
    dd(10,12) = (rt6*zy*(5d0*zz**2-1d0))/4d0
    dd(10,13) = (rt15*zz*(-2d0*zy**2-zz**2+1d0))/2d0
    dd(10,14) = rt15*zx*zy*zz
    dd(10,15) = (rt10*zx*(-4d0*zy**2-zz**2+1d0))/4d0
    dd(10,16) = (rt10*zy*(-4d0*zy**2-3d0*zz**2+3d0))/4d0
    dd(11,10) = (rt3*xz*(5d0*zz**2-1d0))/(2d0*rt2)
    dd(11,11) = (-10d0*xx*yy**2+15d0*xx*zz**2-xx+10d0*yx*xy*yy)/4d0
    dd(11,12) = (10d0*xy*yz**2+15d0*xy*zz**2-11d0*xy-10d0*yy*xz*yz)/4d0
    dd(11,13) = (rt10*(4d0*xy*yy*yz-4d0*yy**2*xz-6d0*zy**2*               &
        xz-3d0*xz*zz**2+5d0*xz))/4d0
    dd(11,14) = rt10*(-xx*yy*yz-yx*xy*yz+2d0*yx*yy*xz+3d0*zx*zy*xz)/2d0
    dd(11,15) = (rt15*(-2d0*xx*yy**2-4d0*xx*zy**2-xx*zz**2+               &
        3d0*xx+2d0*yx*xy*yy))/4d0
    dd(11,16) = (rt15*(-4d0*xy*zy**2-2d0*xy*yz**2-3d0*xy*zz               &
        **2+3d0*xy+2d0*yy*xz*yz))/4d0
    dd(12,10) = (rt3*yz*(5d0*zz**2-1d0))/(2d0*rt2)
    dd(12,11) = (10d0*yx*zy**2+15d0*yx*zz**2-11d0*yx-10d0*zx*yy*zy)/4d0
    dd(12,12) = (5d0*yy*zz**2-yy+10d0*zy*yz*zz)/4d0
    dd(12,13) = (rt10*(-4d0*yy*zy*zz-2d0*zy**2*yz-3d0*yz*zz**2+yz))/4d0
    dd(12,14) = (rt10*(yx*zy*zz+zx*yy*zz+zx*zy*yz))/2d0
    dd(12,15) = (rt15*(-2d0*yx*zy**2-yx*zz**2+yx-2d0*zx*yy*zy))/4d0
    dd(12,16) = (rt15*(-4d0*yy*zy**2-yy*zz**2+yy-2d0*zy*yz*zz))/4d0
    dd(13,10) = rt15*zz*(-2d0*yz**2-zz**2+1d0)/2d0
    dd(13,11) = (rt10*(4d0*yx*yy*zy-4d0*zx*yy**2-6d0*zx*yz**              &
        2-3d0*zx*zz**2+5d0*zx))/4d0
    dd(13,12) = (rt10*(-4d0*yy*yz*zz-2d0*zy*yz**2-3d0*zy*zz**2+zy))/4d0
    dd(13,13) = (-4d0*xx*yy-4d0*yx*xy+12d0*yy**2*zz+6d0*zy**2*zz+         &
        6d0*yz**2*zz+3d0*zz**3-9d0*zz)/2d0
    dd(13,14) = -6d0*yx*yy*zz-3d0*zx*zy*zz-4d0*xy*yy-2d0*xz*yz
    dd(13,15) = (rt6*(4d0*yx*yy*zy+4d0*zx*yy**2+4d0*zx*zy**2              &
        +2d0*zx*yz**2+zx*zz**2-3d0*zx))/4d0
    dd(13,16) = (rt6*(8d0*yy**2*zy+4d0*yy*yz*zz+4d0*zy**3+2d0             &
        *zy*yz**2+3d0*zy*zz**2-5d0*zy))/4d0
    dd(14,10) = rt15*xz*yz*zz
    dd(14,11) = (rt10*(-xx*yy*zy-yx*xy*zy+2d0*zx*xy*yy+3d0*zx*xz*yz))/2d0
    dd(14,12) = (rt10*(xy*yz*zz+yy*xz*zz+zy*xz*yz))/2d0
    dd(14,13) = -4d0*yx*yy-2d0*zx*zy-6d0*xy*yy*zz-3d0*xz*yz*zz
    dd(14,14) = 3d0*xx*yy*zz+3d0*yx*xy*zz-4d0*yy**2-2d0*zy**2-2d0*        &
        yz**2-zz**2+3d0
    dd(14,15) = (rt6*(-xx*yy*zy-yx*xy*zy-2d0*zx*xy*yy-zx*xz*yz))/2d0
    dd(14,16) = (rt6*(-4d0*xy*yy*zy-xy*yz*zz-yy*xz*zz-zy*xz*yz))/2d0
    dd(15,10) = (rt5*xz*(-4d0*yz**2-zz**2+1d0))/(2d0*rt2)
    dd(15,11) = (rt15*(-2d0*xx*yy**2-4d0*xx*yz**2-xx*zz**2+               &
        3d0*xx+2d0*yx*xy*yy))/4d0
    dd(15,12) = (rt15*(-2d0*xy*yz**2-xy*zz**2+xy-2d0*yy*xz*yz))/4d0
    dd(15,13) = (rt6*(4d0*xy*yy*yz+4d0*yy**2*xz+2d0*zy**2*xz              &
        +4d0*xz*yz**2+xz*zz**2-3d0*xz))/4d0
    dd(15,14) = (rt6*(-xx*yy*yz-yx*xy*yz-2d0*yx*yy*xz-zx*zy*xz))/2d0
    dd(15,15) = (10d0*xx*yy**2+4d0*xx*zy**2+4d0*xx*yz**2+xx*zz**          &
        2-7d0*xx+6d0*yx*xy*yy)/4d0
    dd(15,16) = (16d0*xy*yy**2+4d0*xy*zy**2+6d0*xy*yz**2+3d0*xy*          &
        zz**2-7d0*xy+6d0*yy*xz*yz)/4d0
    dd(16,10) = (rt5*yz*(-4d0*yz**2-3d0*zz**2+3d0))/(2d0*rt2)
    dd(16,11) = (rt15*(-2d0*yx*zy**2-4d0*yx*yz**2-3d0*yx*zz               &
        **2+3d0*yx+2d0*zx*yy*zy))/4d0
    dd(16,12) = (rt15*(-4d0*yy*yz**2-yy*zz**2+yy-2d0*zy*yz*zz))/4d0
    dd(16,13) = (rt6*(8d0*yy**2*yz+4d0*yy*zy*zz+2d0*zy**2*yz              &
        +4d0*yz**3+3d0*yz*zz**2-5d0*yz))/4d0
    dd(16,14) = (rt6*(-4d0*yx*yy*yz-yx*zy*zz-zx*yy*zz-zx*zy*yz))/2d0
    dd(16,15) = (16d0*yx*yy**2+6d0*yx*zy**2+4d0*yx*yz**2+3d0*yx*          &
        zz**2-7d0*yx+6d0*zx*yy*zy)/4d0
    dd(16,16) = (16d0*yy**3+12d0*yy*zy**2+12d0*yy*yz**2+3d0*yy*zz         &
        **2-15d0*yy+6d0*zy*yz*zz)/4d0
  endif

  if (lmax .ge. 4) then
    dd(17,17) = (-68d0*yy**2*zz**2+68d0*yy**2+136d0*yy*zy*yz*zz-          &
        68d0*zy**2*yz**2+68d0*zy**2+68d0*yz**2+35d0*zz**4+38d0*zz**2-65d0)/8d0
    dd(17,18) = (rt10*(10d0*yx*yy*zy*zz-10d0*yx*zy**2*yz+ 10d0*yx*yz      &
        -10d0*zx*yy**2*zz+10d0*zx*yy*zy*yz+7d0*zx*zz**3+7d0*zx*zz))       &
        /4d0
    dd(17,19) = (rt10*zy*zz*(7d0*zz**2-3d0))/4d0
    dd(17,20) = (rt5*(-14d0*zy**2*zz**2+2d0*zy**2-7d0*zz**4+8d0*zz**2-1d0)) &
        /4d0
    dd(17,21) = (rt5*zx*zy*(7d0*zz**2-1d0))/2d0
    dd(17,22) = (rt70*(2d0*yx*yy*zy*zz-2d0*yx*zy**2*yz+2d0*               &
        yx*yz-2d0*zx*yy**2*zz+2d0*zx*yy*zy*yz-4d0*zx*zy**2*zz-zx*zz      &
        **3+3d0*zx*zz))/4d0
    dd(17,23) = (rt70*zy*zz*(-4d0*zy**2-3d0*zz**2+3d0))/4d0
    dd(17,24) = (rt35*(4d0*yy**2*zz**2-4d0*yy**2-8d0*yy*zy*               &
        yz*zz+8d0*zy**4+4d0*zy**2*yz**2+8d0*zy**2*zz**2-12d0*zy**2-4d0   &
        *yz**2+zz**4-6d0*zz**2+5d0))/8d0
    dd(17,25) = (rt35*zx*zy*(-2d0*zy**2-zz**2+1d0))/2d0
    dd(18,17) = (rt5*(10d0*xy*yy*yz*zz-10d0*xy*zy*yz**2+10d0              &
        *xy*zy-10d0*yy**2*xz*zz+10d0*yy*zy*xz*yz+7d0*xz*zz**3+7d0*xz*    &
        zz))/(2d0*rt2)
    dd(18,18) = (-24d0*xx*yy**2*zz+28d0*xx*zz**3+28d0*xx*zz+24d0*         &
        yx*xy*yy*zz-31d0*yy*zz**2+3d0*yy+34d0*zy*yz*zz)/4d0
    dd(18,19) = (-34d0*yx*zy**2-3d0*yx*zz**2+31d0*yx+34d0*zx*yy*          &
        zy+24d0*xy*yz**2*zz+28d0*xy*zz**3+4d0*xy*zz-24d0*yy*xz*yz*zz)/4d0
    dd(18,20) = (rt2*(4d0*xy*yy*yz*zz+4d0*xy*zy*yz**2+xy*zy               &
        *zz**2-3d0*xy*zy-4d0*yy**2*xz*zz-4d0*yy*zy*xz*yz-15d0*zy**2*     &
        xz*zz-7d0*xz*zz**3+8d0*xz*zz))/2d0
    dd(18,21) = (rt2*(-22d0*xx*yy**2*zy-30d0*xx*zy**3-xx*zy               &
        *zz**2+29d0*xx*zy+22d0*yx*xy*yy*zy+30d0*zx*xy*zy**2+22d0*zx*     &
        xy*yz**2+29d0*zx*xy*zz**2-23d0*zx*xy-22d0*zx*yy*xz*yz))/4d0
    dd(18,22) = (rt7*(-8d0*xx*yy**2*zz-16d0*xx*zy**2*zz-4d0*              &
        xx*zz**3+12d0*xx*zz+8d0*yx*xy*yy*zz+4d0*yy*zy**2+yy*zz**2-yy     &
        +2d0*zy*yz*zz))/4d0
    dd(18,23) = (rt7*(-2d0*yx*zy**2-yx*zz**2+yx-2d0*zx*yy*                &
        zy-16d0*xy*zy**2*zz-8d0*xy*yz**2*zz-12d0*xy*zz**3+12d0*xy*zz+    &
        8d0*yy*xz*yz*zz))/4d0
    dd(18,24) = (rt14*(-2d0*xy*yy*yz*zz+8d0*xy*zy**3+2d0*xy*              &
        zy*yz**2+4d0*xy*zy*zz**2-6d0*xy*zy+2d0*yy**2*xz*zz-2d0*yy*zy*    &
        xz*yz+4d0*zy**2*xz*zz+xz*zz**3-3d0*xz*zz))/4d0
    dd(18,25) = (rt14*(-2d0*xx*yy**2*zy-4d0*xx*zy**3-xx*zy*               &
        zz**2+3d0*xx*zy+2d0*yx*xy*yy*zy-4d0*zx*xy*zy**2-2d0*zx*xy*yz     &
        **2-3d0*zx*xy*zz**2+3d0*zx*xy+2d0*zx*yy*xz*yz))/4d0
    dd(19,17) = (rt5*yz*zz*(7d0*zz**2-3d0))/(2d0*rt2)
    dd(19,18) = (24d0*yx*zy**2*zz+28d0*yx*zz**3+4d0*yx*zz-24d0*zx         &
        *yy*zy*zz-34d0*xy*yz**2-3d0*xy*zz**2+31d0*xy+34d0*yy*xz*yz)/4d0
    dd(19,19) = (-34d0*xx*yy**2+3d0*xx*zz**2+3d0*xx+34d0*yx*xy*yy         &
        +4d0*yy*zz**3+28d0*yy*zz+24d0*zy*yz*zz**2)/4d0
    dd(19,20) = (rt2*(-7d0*yy*zy*zz**2+yy*zy-7d0*zy**2*yz*                &
        zz-7d0*yz*zz**3+4d0*yz*zz))/2d0
    dd(19,21) = (rt2*(-8d0*yx*zy**3-yx*zy*zz**2+7d0*yx*zy+                &
        8d0*zx*yy*zy**2+7d0*zx*yy*zz**2-zx*yy+22d0*zx*zy*yz*zz))/4d0
    dd(19,22) = (rt7*(-8d0*yx*zy**2*zz-4d0*yx*zz**3+4d0*yx*               &
        zz-8d0*zx*yy*zy*zz-4d0*xy*zy**2-2d0*xy*yz**2-3d0*xy*zz**2+3d0*   &
        xy+2d0*yy*xz*yz))/4d0
    dd(19,23) = (rt7*(2d0*xx*yy**2+4d0*xx*zy**2+xx*zz**2-3d0              &
        *xx-2d0*yx*xy*yy-16d0*yy*zy**2*zz-4d0*yy*zz**3+4d0*yy*zz-8d0*    &
        zy*yz*zz**2))/4d0
    dd(19,24) = (rt14*(8d0*yy*zy**3+4d0*yy*zy*zz**2-4d0*yy*               &
        zy+4d0*zy**2*yz*zz+yz*zz**3-yz*zz))/4d0
    dd(19,25) = (rt14*(-2d0*yx*zy**3-yx*zy*zz**2+yx*zy-6d0*               &
        zx*yy*zy**2-zx*yy*zz**2+zx*yy-2d0*zx*zy*yz*zz))/4d0
    dd(20,17) = rt5*(-14d0*yz**2*zz**2+2d0*yz**2-7d0*zz**4                &
        +8d0*zz**2-1d0)/4d0
    dd(20,18) = (rt2*(4d0*yx*yy*zy*zz+4d0*yx*zy**2*yz+yx*yz               &
        *zz**2-3d0*yx*yz-4d0*zx*yy**2*zz-4d0*zx*yy*zy*yz-15d0*zx*yz**    &
        2*zz-7d0*zx*zz**3+8d0*zx*zz))/2d0
    dd(20,19) = (rt2*(-7d0*yy*yz*zz**2+yy*yz-7d0*zy*yz**2*                &
        zz-7d0*zy*zz**3+4d0*zy*zz))/2d0
    dd(20,20) = (14d0*yy**2*zz**2-10d0*yy**2+14d0*zy**2*yz**2+            &
        14d0*zy**2*zz**2-12d0*zy**2+14d0*yz**2*zz**2-12d0*yz**2+7d0*zz   &
        **4-20d0*zz**2+11d0)/2d0
    dd(20,21) = -7d0*yx*yy*zz**2+5d0*yx*yy-7d0*zx*zy*yz**2-7d0*zx         &
        *zy*zz**2+6d0*zx*zy
    dd(20,22) = (rt14*(4d0*yx*zy**2*yz+yx*yz*zz**2-3d0*yx*yz+             &
        4d0*zx*yy**2*zz+4d0*zx*zy**2*zz+zx*yz**2*zz+zx*zz**3-4d0*zx*zz))/2d0
    dd(20,23) = (rt14*(4d0*yy**2*zy*zz+4d0*yy*zy**2*yz+3d0*               &
        yy*yz*zz**2-yy*yz+4d0*zy**3*zz+3d0*zy*yz**2*zz+3d0*zy*zz**3-     &
        4d0*zy*zz))/2d0
    dd(20,24) = (rt7*(-16d0*yy**2*zy**2-8d0*yy**2*zz**2+8d0*              &
        yy**2-8d0*zy**4-8d0*zy**2*yz**2-8d0*zy**2*zz**2+16d0*zy**2-2d0   &
        *yz**2*zz**2+6d0*yz**2-zz**4+8d0*zz**2-7d0))/4d0
    dd(20,25) = rt7*(2d0*yx*yy*zy**2+yx*yy*zz**2-yx*yy+2d0*               &
        zx*yy**2*zy+2d0*zx*zy**3+zx*zy*yz**2+zx*zy*zz**2-2d0*zx*zy)
    dd(21,17) = rt5*xz*yz*(7d0*zz**2-1d0)/2d0
    dd(21,18) = (rt2*(-22d0*xx*yy**2*yz-30d0*xx*yz**3-xx*yz               &
        *zz**2+29d0*xx*yz+22d0*yx*xy*yy*yz+22d0*yx*zy**2*xz+30d0*yx*     &
        xz*yz**2+29d0*yx*xz*zz**2-23d0*yx*xz-22d0*zx*yy*zy*xz))/4d0
    dd(21,19) = (rt2*(14d0*xy*yz**3+21d0*xy*yz*zz**2-15d0*xy              &
        *yz-14d0*yy*xz*yz**2+7d0*yy*xz*zz**2-yy*xz))/4d0
    dd(21,20) = -7d0*xy*yy*zz**2+5d0*xy*yy-7d0*zy**2*xz*yz-7d0*xz         &
        *yz*zz**2+6d0*xz*yz
    dd(21,21) = (-28d0*xx*yy**3-14d0*xx*yy*zy**2-14d0*xx*yy*yz**          &
        2+7d0*xx*yy*zz**2+23d0*xx*yy+28d0*yx*xy*yy**2+14d0*yx*xy*zy**    &
        2+14d0*yx*xy*yz**2+21d0*yx*xy*zz**2-19d0*yx*xy)/2d0
    dd(21,22) = (rt14*(2d0*xx*yy**2*yz-4d0*xx*zy**2*yz+2d0*               &
        xx*yz**3-xx*yz*zz**2+xx*yz+6d0*yx*xy*yy*yz-8d0*yx*yy**2*xz-      &
        6d0*yx*zy**2*xz-2d0*yx*xz*yz**2-3d0*yx*xz*zz**2+5d0*yx*xz-6d0*   &
        zx*yy*zy*xz))/4d0
    dd(21,23) = (rt14*(8d0*xy*yy**2*yz-4d0*xy*zy**2*yz-6d0*               &
        xy*yz**3-9d0*xy*yz*zz**2+7d0*xy*yz-8d0*yy**3*xz-12d0*yy*zy**2    &
        *xz+6d0*yy*xz*yz**2-3d0*yy*xz*zz**2+9d0*yy*xz))/4d0
    dd(21,24) = (rt7*(8d0*xy*yy*zy**2+4d0*xy*yy*zz**2-4d0*xy              &
        *yy+4d0*zy**2*xz*yz+xz*yz*zz**2-3d0*xz*yz))/2d0
    dd(21,25) = (rt7*(-4d0*xx*yy*zy**2+2d0*xx*yy*yz**2-xx*                &
        yy*zz**2+xx*yy-4d0*yx*xy*zy**2-2d0*yx*xy*yz**2-3d0*yx*xy*zz      &
        **2+3d0*yx*xy))/2d0
    dd(22,17) = (rt35*(2d0*xy*yy*yz*zz-2d0*xy*zy*yz**2+2d0*               &
        xy*zy-2d0*yy**2*xz*zz+2d0*yy*zy*xz*yz-4d0*xz*yz**2*zz-xz*zz      &
        **3+3d0*xz*zz))/(2d0*rt2)
    dd(22,18) = (rt7*(-8d0*xx*yy**2*zz-16d0*xx*yz**2*zz-4d0*              &
        xx*zz**3+12d0*xx*zz+8d0*yx*xy*yy*zz+4d0*yy*yz**2+yy*zz**2-yy     &
        +2d0*zy*yz*zz))/4d0
    dd(22,19) = (rt7*(-2d0*yx*zy**2-4d0*yx*yz**2-3d0*yx*zz**              &
        2+3d0*yx+2d0*zx*yy*zy-8d0*xy*yz**2*zz-4d0*xy*zz**3+4d0*xy*zz-    &
        8d0*yy*xz*yz*zz))/4d0
    dd(22,20) = (rt14*(4d0*xy*zy*yz**2+xy*zy*zz**2-3d0*xy*                &
        zy+4d0*yy**2*xz*zz+zy**2*xz*zz+4d0*xz*yz**2*zz+xz*zz**3-4d0*     &
        xz*zz))/2d0
    dd(22,21) = (rt14*(2d0*xx*yy**2*zy+2d0*xx*zy**3-4d0*xx*               &
        zy*yz**2-xx*zy*zz**2+xx*zy+6d0*yx*xy*yy*zy-8d0*zx*xy*yy**2-      &
        2d0*zx*xy*zy**2-6d0*zx*xy*yz**2-3d0*zx*xy*zz**2+5d0*zx*xy-6d0*   &
        zx*yy*xz*yz))/4d0
    dd(22,22) = (40d0*xx*yy**2*zz+16d0*xx*zy**2*zz+16d0*xx*yz**2          &
        *zz+4d0*xx*zz**3-28d0*xx*zz+24d0*yx*xy*yy*zz-48d0*yy**3-36d0*    &
        yy*zy**2-36d0*yy*yz**2-9d0*yy*zz**2+45d0*yy-18d0*zy*yz*zz)/4d0
    dd(22,23) = (48d0*yx*yy**2+18d0*yx*zy**2+12d0*yx*yz**2+9d0*yx         &
        *zz**2-21d0*yx+18d0*zx*yy*zy+64d0*xy*yy**2*zz+16d0*xy*zy**2*     &
        zz+24d0*xy*yz**2*zz+12d0*xy*zz**3-28d0*xy*zz+24d0*yy*xz*yz*zz)/4d0
    dd(22,24) = (rt2*(-32d0*xy*yy**2*zy-6d0*xy*yy*yz*zz-8d0*              &
        xy*zy**3-10d0*xy*zy*yz**2-4d0*xy*zy*zz**2+14d0*xy*zy-10d0*yy     &
        **2*xz*zz-6d0*yy*zy*xz*yz-4d0*zy**2*xz*zz-4d0*xz*yz**2*zz-xz     &
        *zz**3+7d0*xz*zz))/4d0
    dd(22,25) = (rt2*(10d0*xx*yy**2*zy+4d0*xx*zy**3+4d0*xx*               &
        zy*yz**2+xx*zy*zz**2-7d0*xx*zy+6d0*yx*xy*yy*zy+16d0*zx*xy*yy     &
        **2+4d0*zx*xy*zy**2+6d0*zx*xy*yz**2+3d0*zx*xy*zz**2-7d0*zx*xy    &
        +6d0*zx*yy*xz*yz))/4d0
    dd(23,17) = (rt35*yz*zz*(-4d0*yz**2-3d0*zz**2+3d0))/(2d0*rt2)
    dd(23,18) = (rt7*(-8d0*yx*zy**2*zz-16d0*yx*yz**2*zz-12d0              &
        *yx*zz**3+12d0*yx*zz+8d0*zx*yy*zy*zz-2d0*xy*yz**2-xy*zz**2+      &
        xy-2d0*yy*xz*yz))/4d0
    dd(23,19) = (rt7*(2d0*xx*yy**2+4d0*xx*yz**2+xx*zz**2-3d0              &
        *xx-2d0*yx*xy*yy-16d0*yy*yz**2*zz-4d0*yy*zz**3+4d0*yy*zz-8d0*    &
        zy*yz*zz**2))/4d0
    dd(23,20) = (rt14*(4d0*yy**2*yz*zz+4d0*yy*zy*yz**2+3d0*               &
        yy*zy*zz**2-yy*zy+3d0*zy**2*yz*zz+4d0*yz**3*zz+3d0*yz*zz**3-     &
        4d0*yz*zz))/2d0
    dd(23,21) = (rt14*(8d0*yx*yy**2*zy-4d0*yx*zy*yz**2-3d0*               &
        yx*zy*zz**2+yx*zy-8d0*zx*yy**3-12d0*zx*yy*yz**2-3d0*zx*yy*zz     &
        **2+9d0*zx*yy-6d0*zx*zy*yz*zz))/4d0
    dd(23,22) = (64d0*yx*yy**2*zz+24d0*yx*zy**2*zz+16d0*yx*yz**2          &
        *zz+12d0*yx*zz**3-28d0*yx*zz+24d0*zx*yy*zy*zz+48d0*xy*yy**2+     &
        12d0*xy*zy**2+18d0*xy*yz**2+9d0*xy*zz**2-21d0*xy+18d0*yy*xz*yz)/4d0
    dd(23,23) = (-30d0*xx*yy**2-12d0*xx*zy**2-12d0*xx*yz**2-3d0*          &
        xx*zz**2+21d0*xx-18d0*yx*xy*yy+64d0*yy**3*zz+48d0*yy*zy**2*zz    &
        +48d0*yy*yz**2*zz+12d0*yy*zz**3-60d0*yy*zz+24d0*zy*yz*zz**2)/4d0
    dd(23,24) = (rt2*(-32d0*yy**3*zy-16d0*yy**2*yz*zz-24d0*               &
        yy*zy**3-16d0*yy*zy*yz**2-12d0*yy*zy*zz**2+28d0*yy*zy-12d0*zy    &
        **2*yz*zz-4d0*yz**3*zz-3d0*yz*zz**3+7d0*yz*zz))/4d0
    dd(23,25) = (rt2*(16d0*yx*yy**2*zy+6d0*yx*zy**3+4d0*yx*               &
        zy*yz**2+3d0*yx*zy*zz**2-7d0*yx*zy+16d0*zx*yy**3+18d0*zx*yy*     &
        zy**2+12d0*zx*yy*yz**2+3d0*zx*yy*zz**2-15d0*zx*yy+6d0*zx*zy*     &
        yz*zz))/4d0
    dd(24,17) = rt35*(4d0*yy**2*zz**2-4d0*yy**2-8d0*yy*zy*                &
        yz*zz+4d0*zy**2*yz**2-4d0*zy**2+8d0*yz**4+8d0*yz**2*zz**2-12d0   &
        *yz**2+zz**4-6d0*zz**2+5d0)/8d0
    dd(24,18) = (rt14*(-2d0*yx*yy*zy*zz+2d0*yx*zy**2*yz+8d0*              &
        yx*yz**3+4d0*yx*yz*zz**2-6d0*yx*yz+2d0*zx*yy**2*zz-2d0*zx*yy*    &
        zy*yz+4d0*zx*yz**2*zz+zx*zz**3-3d0*zx*zz))/4d0
    dd(24,19) = (rt14*(8d0*yy*yz**3+4d0*yy*yz*zz**2-4d0*yy*               &
        yz+4d0*zy*yz**2*zz+zy*zz**3-zy*zz))/4d0
    dd(24,20) = (rt7*(-16d0*yy**2*yz**2-8d0*yy**2*zz**2+8d0*              &
        yy**2-8d0*zy**2*yz**2-2d0*zy**2*zz**2+6d0*zy**2-8d0*yz**4-8d0*   &
        yz**2*zz**2+16d0*yz**2-zz**4+8d0*zz**2-7d0))/4d0
    dd(24,21) = (rt7*(8d0*yx*yy*yz**2+4d0*yx*yy*zz**2-4d0*yx              &
        *yy+4d0*zx*zy*yz**2+zx*zy*zz**2-3d0*zx*zy))/2d0
    dd(24,22) = (rt2*(-32d0*yx*yy**2*yz-6d0*yx*yy*zy*zz-10d0              &
        *yx*zy**2*yz-8d0*yx*yz**3-4d0*yx*yz*zz**2+14d0*yx*yz-10d0*zx*    &
        yy**2*zz-6d0*zx*yy*zy*yz-4d0*zx*zy**2*zz-4d0*zx*yz**2*zz-zx*     &
        zz**3+7d0*zx*zz))/4d0
    dd(24,23) = (rt2*(-32d0*yy**3*yz-16d0*yy**2*zy*zz-16d0*               &
        yy*zy**2*yz-24d0*yy*yz**3-12d0*yy*yz*zz**2+28d0*yy*yz-4d0*zy     &
        **3*zz-12d0*zy*yz**2*zz-3d0*zy*zz**3+7d0*zy*zz))/4d0
    dd(24,24) = (64d0*yy**4+64d0*yy**2*zy**2+64d0*yy**2*yz**2+            &
        20d0*yy**2*zz**2-84d0*yy**2+24d0*yy*zy*yz*zz+8d0*zy**4+20d0*zy   &
        **2*yz**2+8d0*zy**2*zz**2-28d0*zy**2+8d0*yz**4+8d0*yz**2*zz**    &
        2-28d0*yz**2+zz**4-14d0*zz**2+21d0)/8d0
    dd(24,25) = (-16d0*yx*yy**3-8d0*yx*yy*zy**2-8d0*yx*yy*yz**2-          &
        4d0*yx*yy*zz**2+12d0*yx*yy-8d0*zx*yy**2*zy-2d0*zx*zy**3-4d0*zx*  &
        zy*yz**2-zx*zy*zz**2+5d0*zx*zy)/2d0
    dd(25,17) = rt35*xz*yz*(-2d0*yz**2-zz**2+1d0)/2d0
    dd(25,18) = (rt14*(-2d0*xx*yy**2*yz-4d0*xx*yz**3-xx*yz*               &
        zz**2+3d0*xx*yz+2d0*yx*xy*yy*yz-2d0*yx*zy**2*xz-4d0*yx*xz*yz**2- &
        3d0*yx*xz*zz**2+3d0*yx*xz+2d0*zx*yy*zy*xz))/4d0
    dd(25,19) = (rt14*(-4d0*xy*yz**3-3d0*xy*yz*zz**2+3d0*xy*yz-           &
        4d0*yy*xz*yz**2-yy*xz*zz**2+yy*xz))/4d0
    dd(25,20) = rt7*(2d0*xy*yy*yz**2+xy*yy*zz**2-xy*yy+2d0*               &
        yy**2*xz*yz+zy**2*xz*yz+2d0*xz*yz**3+xz*yz*zz**2-2d0*xz*yz)
    dd(25,21) = (rt7*(2d0*xx*yy*zy**2-4d0*xx*yy*yz**2-xx*yy               &
        *zz**2+xx*yy-2d0*yx*xy*zy**2-4d0*yx*xy*yz**2-3d0*yx*xy*zz**2     &
        +3d0*yx*xy))/2d0
    dd(25,22) = (rt2*(10d0*xx*yy**2*yz+4d0*xx*zy**2*yz+4d0*               &
        xx*yz**3+xx*yz*zz**2-7d0*xx*yz+6d0*yx*xy*yy*yz+16d0*yx*yy**2     &
        *xz+6d0*yx*zy**2*xz+4d0*yx*xz*yz**2+3d0*yx*xz*zz**2-7d0*yx*xz    &
        +6d0*zx*yy*zy*xz))/4d0
    dd(25,23) = (rt2*(16d0*xy*yy**2*yz+4d0*xy*zy**2*yz+12d0*              &
        xy*yz**3+9d0*xy*yz*zz**2-13d0*xy*yz+16d0*yy**3*xz+12d0*yy*zy**2*xz+ &
        12d0*yy*xz*yz**2+3d0*yy*xz*zz**2-15d0*yy*xz))/4d0
    dd(25,24) = (-16d0*xy*yy**3-8d0*xy*yy*zy**2-8d0*xy*yy*yz**2-          &
        4d0*xy*yy*zz**2+12d0*xy*yy-8d0*yy**2*xz*yz-4d0*zy**2*xz*yz-2d0*  &
        xz*yz**3-xz*yz*zz**2+5d0*xz*yz)/2d0
    dd(25,25) = (8d0*xx*yy**3+4d0*xx*yy*zy**2+4d0*xx*yy*yz**2+xx          &
        *yy*zz**2-7d0*xx*yy+8d0*yx*xy*yy**2+4d0*yx*xy*zy**2+4d0*yx*xy    &
        *yz**2+3d0*yx*xy*zz**2-5d0*yx*xy)/2d0
  endif

  !  Check values from coupling formula against explicit formulae
  !  obtained previously
  do l=2,4
    do i=l**2+1,(l+1)**2
      do j=l**2+1,(l+1)**2
        if (abs(d(i,j)-dd(i,j))>1d-10) then
          print "(a,i0,a,i0,2f12.8)", "Error at ", i, ",", j, d(i,j), dd(i,j)
          ok=.false.
        end if
      end do
    end do
  end do

  if (.not. ok) call die("Wigner: Errors in rotation matrix")
endif

END SUBROUTINE wigner

!----------------------------------------------------------------------

SUBROUTINE read_cg

USE input, ONLY: die
INTEGER :: i, j, j2, k, ok, n
DOUBLE PRECISION :: i1, i2, i3, i4

if (cgread) return

!  Data for coupling coefficients
allocate(cg(1)%data(7,20))
cg(1)%data = reshape(                     &
    [0, 0, 0,-1, 1, 1, 3,   1, 1, 0,-1, 1, 1, 3,   2, 2, 0,-1, 1, 1, 3,   2, 1, 1,-1, 1,-1, 2,   1, 2, 1, 1, 1,-1, 2, &
     2, 0, 2, 1, 1,-1, 2,   0, 2, 2,-1, 1,-1, 2,   1, 0, 3,-1, 1,-1, 2,   0, 1, 3, 1, 1,-1, 2,   0, 0, 4, 1, 1, 2, 3, &
     1, 1, 4,-1, 1, 1, 6,   2, 2, 4,-1, 1, 1, 6,   1, 0, 5, 1, 1, 1, 2,   0, 1, 5, 1, 1, 1, 2,   2, 0, 6, 1, 1, 1, 2, &
     0, 2, 6, 1, 1, 1, 2,   1, 1, 7, 1, 1, 1, 2,   2, 2, 7,-1, 1, 1, 2,   2, 1, 8, 1, 1, 1, 2,   1, 2, 8, 1, 1, 1, 2],[7,20])
allocate(cg(2)%data(7,48))
cg(2)%data = reshape(                     &
    [0, 0, 1,-1, 1, 2, 5,   1, 1, 1,-1, 1, 3,10,   2, 2, 1,-1, 1, 3,10,   1, 0, 2, 1, 1, 1,10,   0, 1, 2,-1, 1, 3,10, &
     1, 3, 2,-1, 1, 3,10,   2, 4, 2,-1, 1, 3,10,   2, 0, 3, 1, 1, 1,10,   0, 2, 3,-1, 1, 3,10,   2, 3, 3, 1, 1, 3,10, &
     1, 4, 3,-1, 1, 3,10,   2, 1, 4,-1, 1,-1, 2,   1, 2, 4, 1, 1,-1, 2,   2, 0, 5, 1, 1,-1, 2,   0, 2, 5,-1, 1,-1, 6, &
     2, 3, 5,-1, 1,-1, 6,   1, 4, 5, 1, 1,-1, 6,   1, 0, 6,-1, 1,-1, 2,   0, 1, 6, 1, 1,-1, 6,   1, 3, 6,-1, 1,-1, 6, &
     2, 4, 6,-1, 1,-1, 6,   2, 1, 7, 1, 1,-1, 6,   1, 2, 7, 1, 1,-1, 6,   0, 4, 7,-1, 1,-2, 3,   1, 1, 8,-1, 1,-1, 6, &
     2, 2, 8, 1, 1,-1, 6,   0, 3, 8, 1, 1,-2, 3,   0, 0, 9, 1, 1, 3, 5,   1, 1, 9,-1, 1, 1, 5,   2, 2, 9,-1, 1, 1, 5, &
     1, 0,10, 1, 1, 2, 5,   0, 1,10, 2, 1, 2,15,   1, 3,10,-1, 1, 1,30,   2, 4,10,-1, 1, 1,30,   2, 0,11, 1, 1, 2, 5, &
     0, 2,11, 2, 1, 2,15,   2, 3,11, 1, 1, 1,30,   1, 4,11,-1, 1, 1,30,   1, 1,12, 1, 1, 1, 3,   2, 2,12,-1, 1, 1, 3, &
     0, 3,12, 1, 1, 1, 3,   2, 1,13, 1, 1, 1, 3,   1, 2,13, 1, 1, 1, 3,   0, 4,13, 1, 1, 1, 3,   1, 3,14, 1, 1, 1, 2, &
     2, 4,14,-1, 1, 1, 2,   2, 3,15, 1, 1, 1, 2,   1, 4,15, 1, 1, 1, 2],[7,48])
allocate(cg(3)%data(7,78))
cg(3)%data = reshape(                     &
    [0, 0, 4,-1, 1, 3, 7,   1, 1, 4,-1, 1, 2, 7,   2, 2, 4,-1, 1, 2, 7,   1, 0, 5, 1, 1, 1, 7,   0, 1, 5,-2, 1, 2,21, &
     1, 3, 5,-1, 1, 5,21,   2, 4, 5,-1, 1, 5,21,   2, 0, 6, 1, 1, 1, 7,   0, 2, 6,-2, 1, 2,21,   2, 3, 6, 1, 1, 5,21, &
     1, 4, 6,-1, 1, 5,21,   1, 1, 7, 1, 1, 1,42,   2, 2, 7,-1, 1, 1,42,   0, 3, 7,-1, 1, 5,21,   1, 5, 7,-1, 1, 5,14, &
     2, 6, 7,-1, 1, 5,14,   2, 1, 8, 1, 1, 1,42,   1, 2, 8, 1, 1, 1,42,   0, 4, 8,-1, 1, 5,21,   2, 5, 8, 1, 1, 5,14, &
     1, 6, 8,-1, 1, 5,14,   2, 1, 9,-1, 1,-1, 2,   1, 2, 9, 1, 1,-1, 2,   2, 0,10, 1, 1,-1, 2,   0, 2,10,-1, 2,-1, 3, &
     2, 3,10,-1, 2,-5, 6,   1, 4,10, 1, 2,-5, 6,   1, 0,11,-1, 1,-1, 2,   0, 1,11, 1, 2,-1, 3,   1, 3,11,-1, 2,-5, 6, &
     2, 4,11,-1, 2,-5, 6,   2, 1,12, 1, 2,-5, 6,   1, 2,12, 1, 2,-5, 6,   0, 4,12,-1, 1,-1, 3,   2, 5,12,-1, 2,-1, 2, &
     1, 6,12, 1, 2,-1, 2,   1, 1,13,-1, 2,-5, 6,   2, 2,13, 1, 2,-5, 6,   0, 3,13, 1, 1,-1, 3,   1, 5,13,-1, 2,-1, 2, &
     2, 6,13,-1, 2,-1, 2,   2, 3,14, 1, 2,-1, 2,   1, 4,14, 1, 2,-1, 2,   0, 6,14,-1, 2,-3, 1,   1, 3,15,-1, 2,-1, 2, &
     2, 4,15, 1, 2,-1, 2,   0, 5,15, 1, 2,-3, 1,   0, 0,16, 2, 1, 1, 7,   1, 1,16,-1, 1, 3,14,   2, 2,16,-1, 1, 3,14, &
     1, 0,17, 1, 1, 5,14,   0, 1,17, 1, 2,15, 7,   1, 3,17,-1, 2, 3,14,   2, 4,17,-1, 2, 3,14,   2, 0,18, 1, 1, 5,14, &
     0, 2,18, 1, 2,15, 7,   2, 3,18, 1, 2, 3,14,   1, 4,18,-1, 2, 3,14,   1, 1,19, 1, 2,15,14,   2, 2,19,-1, 2,15,14, &
     0, 3,19, 1, 1, 3, 7,   1, 5,19,-1, 2, 1,14,   2, 6,19,-1, 2, 1,14,   2, 1,20, 1, 2,15,14,   1, 2,20, 1, 2,15,14, &
     0, 4,20, 1, 1, 3, 7,   2, 5,20, 1, 2, 1,14,   1, 6,20,-1, 2, 1,14,   1, 3,21, 1, 2, 3, 2,   2, 4,21,-1, 2, 3, 2, &
     0, 5,21, 1, 2, 1, 1,   2, 3,22, 1, 2, 3, 2,   1, 4,22, 1, 2, 3, 2,   0, 6,22, 1, 2, 1, 1,   1, 5,23, 1, 1, 1, 2, &
     2, 6,23,-1, 1, 1, 2,   2, 5,24, 1, 1, 1, 2,   1, 6,24, 1, 1, 1, 2],[7,78])
allocate(cg(4)%data(7,108))
cg(4)%data = reshape(                     &
    [0, 0, 9,-2, 3, 1, 1,   1, 1, 9,-1, 3, 5, 2,   2, 2, 9,-1, 3, 5, 2,   1, 0,10, 1, 1, 1, 6,   0, 1,10,-1, 2, 5, 3, &
     1, 3,10,-1, 2, 5, 6,   2, 4,10,-1, 2, 5, 6,   2, 0,11, 1, 1, 1, 6,   0, 2,11,-1, 2, 5, 3,   2, 3,11, 1, 2, 5, 6, &
     1, 4,11,-1, 2, 5, 6,   1, 1,12, 1, 2, 1, 6,   2, 2,12,-1, 2, 1, 6,   0, 3,12,-1, 1, 1, 3,   1, 5,12,-1, 2, 7, 6, &
     2, 6,12,-1, 2, 7, 6,   2, 1,13, 1, 2, 1, 6,   1, 2,13, 1, 2, 1, 6,   0, 4,13,-1, 1, 1, 3,   2, 5,13, 1, 2, 7, 6, &
     1, 6,13,-1, 2, 7, 6,   1, 3,14, 1, 6, 1, 2,   2, 4,14,-1, 6, 1, 2,   0, 5,14,-1, 6, 7, 1,   1, 7,14,-1, 3, 7, 2, &
     2, 8,14,-1, 3, 7, 2,   2, 3,15, 1, 6, 1, 2,   1, 4,15, 1, 6, 1, 2,   0, 6,15,-1, 6, 7, 1,   2, 7,15, 1, 3, 7, 2, &
     1, 8,15,-1, 3, 7, 2,   2, 1,16,-1, 1,-1, 2,   1, 2,16, 1, 1,-1, 2,   2, 0,17, 1, 1,-1, 2,   0, 2,17,-1, 2,-1, 5, &
     2, 3,17,-3, 2,-1,10,   1, 4,17, 3, 2,-1,10,   1, 0,18,-1, 1,-1, 2,   0, 1,18, 1, 2,-1, 5,   1, 3,18,-3, 2,-1,10, &
     2, 4,18,-3, 2,-1,10,   2, 1,19, 3, 2,-1,10,   1, 2,19, 3, 2,-1,10,   0, 4,19,-1, 1,-1, 5,   2, 5,19,-1, 2,-7,10, &
     1, 6,19, 1, 2,-7,10,   1, 1,20,-3, 2,-1,10,   2, 2,20, 3, 2,-1,10,   0, 3,20, 1, 1,-1, 5,   1, 5,20,-1, 2,-7,10, &
     2, 6,20,-1, 2,-7,10,   2, 3,21, 1, 2,-7,10,   1, 4,21, 1, 2,-7,10,   0, 6,21,-3, 2,-1, 5,   2, 7,21,-1, 1,-1,10, &
     1, 8,21, 1, 1,-1,10,   1, 3,22,-1, 2,-7,10,   2, 4,22, 1, 2,-7,10,   0, 5,22, 3, 2,-1, 5,   1, 7,22,-1, 1,-1,10, &
     2, 8,22,-1, 1,-1,10,   2, 5,23, 1, 1,-1,10,   1, 6,23, 1, 1,-1,10,   0, 8,23,-2, 1,-1, 5,   1, 5,24,-1, 1,-1,10, &
     2, 6,24, 1, 1,-1,10,   0, 7,24, 2, 1,-1, 5,   0, 0,25, 1, 3, 5, 1,   1, 1,25,-1, 3, 2, 1,   2, 2,25,-1, 3, 2, 1, &
     1, 0,26, 1, 1, 1, 3,   0, 1,26, 2, 1, 2,15,   1, 3,26,-1, 1, 1,15,   2, 4,26,-1, 1, 1,15,   2, 0,27, 1, 1, 1, 3, &
     0, 2,27, 2, 1, 2,15,   2, 3,27, 1, 1, 1,15,   1, 4,27,-1, 1, 1,15,   1, 1,28, 1, 1, 7,30,   2, 2,28,-1, 1, 7,30, &
     0, 3,28, 1, 1, 7,15,   1, 5,28,-1, 1, 1,30,   2, 6,28,-1, 1, 1,30,   2, 1,29, 1, 1, 7,30,   1, 2,29, 1, 1, 7,30, &
     0, 4,29, 1, 1, 7,15,   2, 5,29, 1, 1, 1,30,   1, 6,29,-1, 1, 1,30,   1, 3,30, 1, 3,14, 5,   2, 4,30,-1, 3,14, 5, &
     0, 5,30, 4, 3, 1, 5,   1, 7,30,-1, 3, 1,10,   2, 8,30,-1, 3, 1,10,   2, 3,31, 1, 3,14, 5,   1, 4,31, 1, 3,14, 5, &
     0, 6,31, 4, 3, 1, 5,   2, 7,31, 1, 3, 1,10,   1, 8,31,-1, 3, 1,10,   1, 5,32, 1, 1, 2, 5,   2, 6,32,-1, 1, 2, 5, &
     0, 7,32, 1, 1, 1, 5,   2, 5,33, 1, 1, 2, 5,   1, 6,33, 1, 1, 2, 5,   0, 8,33, 1, 1, 1, 5,   1, 7,34, 1, 1, 1, 2, &
     2, 8,34,-1, 1, 1, 2,   2, 7,35, 1, 1, 1, 2,   1, 8,35, 1, 1, 1, 2],[7,108])


!  Coupling coefficients for spherical tensors of rank 1 and j2 expressed
!  in real form, to give coupled tensors also in real form.

!  Array size for cg(p,q) is p(2p+1,2q+1,(p+q+1)^2)
allocate(cg(1)%p(3,3,9), cg(2)%p(3,5,16), cg(3)%p(3,7,25),       &
    cg(4)%p(3,9,36), stat=ok)
if (ok>0) call die("Can't allocate CG coefficient matrices")

do j2=1,4
  cg(j2)%p=cmplx(0d0,0d0,dp)
  do n=1,size(cg(j2)%data,2)
    k=cg(j2)%data(1,n)
    j=cg(j2)%data(2,n)
    i=cg(j2)%data(3,n)
    i1=cg(j2)%data(4,n)
    i2=cg(j2)%data(5,n)
    i3=cg(j2)%data(6,n)
    i4=cg(j2)%data(7,n)
    !  print "(2i2, i4,2x,7i3)", j1, j2, n, cg(j2)%data(:,n)
    !  Note: index values in the CG data files start at zero
    if (i3>0) then
      cg(j2)%p(k+1,j+1,i+1)=cmplx((i1/i2)*sqrt(i3/i4),0d0,dp)
    else
      i3=-i3
      cg(j2)%p(k+1,j+1,i+1)=cmplx(0d0,(i1/i2)*sqrt(i3/i4),dp)
    end if
  end do
end do
cgread=.true.

END SUBROUTINE read_cg

!----------------------------------------------------------------------

SUBROUTINE read_rotation(p,r)

USE input, ONLY: item, nitems, reada, readf, reread, upcase, die
USE consts, ONLY: afact

!  Read a rotation from the input and return it in angle-axis form p(3)
!  and optionally in matrix r(3,3).

DOUBLE PRECISION, INTENT(OUT) :: p(3)
DOUBLE PRECISION, INTENT(OUT), OPTIONAL :: r(3,3)

!  It's assumed that a ROTATED or equivalent keyword has been read, so
!  we expect a set of Euler angles or
!  BY psi ABOUT nx, ny, nz

DOUBLE PRECISION :: psi, euler(3), q(0:3)
CHARACTER(LEN=8) :: ww
INTEGER :: flag

flag=0
do while (item<nitems)
  call reada(ww)
  select case(upcase(ww))
  case("BY")
    flag=flag+1
    !  Read rotation angle in degrees and convert to radians
    call readf(psi,afact)
  case("ABOUT")
    flag=flag+2
    call readf(p)
  case default
    flag=flag+4
    !  Read Euler angles alpha, beta and gamma and convert to radians
    call reread(-1)
    call readf(euler,afact)
  end select
  if (flag>2) exit
end do
select case(flag)
case(0)
  call die("Rotation data missing",.true.)
case(1)
  call die("Rotation axis missing",.true.)
case(2)
  call die("Rotation angle missing",.true.)
case(3)
  !  Angle-axis provided
  p=p*psi/sqrt(p(1)**2+p(2)**2+p(3)**2)
  if (present(r)) call aamat(p,r)
  !  Not implemented, as we don't need Euler angles in the program.
  !  call aaeuler(p,euler)
case(4)
  !  Euler angles provided
  call eulerquat(euler,q)
  call qtoaa(q,p)
  if (present(r)) call eulermat(euler,r)
end select

END SUBROUTINE read_rotation

END MODULE rotations
