      SUBROUTINE mlinfo(m1,k1,m2,k2,order,ka,kb,adin2)

      USE global
      USE interact
      USE molecules
      USE monolayer
      USE sites
      IMPLICIT NONE

!  Calculate the scalar products of the local axis vectors and the
!  inter-site vector (magnitude R and unit vector ER) for a pair of
!  sites in a periodic layer.
!  If order>0, the vector products are calculated for use in the
!  derivative formulae.

!  Input:
!  m1, m2: The two molecules being considered. (They may be different
!  periodic images of the same molecule.)
!  k1, k2: Site pointers.
!  ka, kb: The index of the surface unit cell in which site k2
!  is to be found. Site k1 is always in the reference cell (0,0).
!  Irrelevant for a simulation, where we take the nearest periodic
!  image, provided that it isn't outside the cutoff range.

!  order:  Order of derivatives needed.
      INTEGER, INTENT(IN) :: k1, k2, m1, m2, order, ka, kb, adin2

!  Output:
!  The following arrays are stored in the common block "interact".
!  E1R(3)    E1R(i) is the scalar product of the unit vector e_i
!  for site k1 with the unit inter-site vector ER
!  (components of ER in the axis system of site k1)
!  E2R(3)    Scalar product of the unit vector e_i for site k2
!  with the unit inter-site vector -ER (components of -ER in
!  the axis system of site k2). NOTE SIGN!
!  xx(3,3)   Scalar products of unit vectors for site k1 and site k2
!  R         distance between sites
!  The above comprise the 16 intermediate variables q(i). Note that they
!  involve the unit intermolecular vector in this implementation.
!  D1(16,12) First derivatives of the 16 intermediate variables w.r.t. the
!  molecular position and torque variables
!  D2(16,12,12) Second derivatives.


      DOUBLE PRECISION                                                   &
          rab(3),            & ! site-site vector R
          er(3),             & ! normalised site-site vector
          a(3), b(3),        & ! site vectors relative to molecular origin (c. of m.)
          ra(3), rb(3),      & ! R+a and R-b respectively
          rxa(3), rxb(3),    & ! R x a and R x b
          raxw1(3,3),        & ! (R+a) x w1     Note: w1(i,j) = se(i,j,p1)
          rbxw2(3,3),        & ! (R-b) x w2           w2(i,j) = se(i,j,p2)
          w2xa(3,3),         & ! w2 x a
          w1xb(3,3),         & ! w1 x b
          w1xw2(3,3,3),      & ! w1 x w2
          ab, raa, rbb,      & ! a . b, (R+a) . a, (R-b) . b
          aw1(3), bw1(3),    & ! a . w1, b . w1
          aw2(3), bw2(3),    & ! a . w2, b . w2
          w1ra(3), w2rb(3)   ! w1 . (R+a), w1 . (R-b)
!     DOUBLE PRECISION :: d11j,d12j,d13j,d14j,d15j,d16j,d116j,d116i,d216ij
!  In the vector products, the first index labels the vector component.
!  w1xw2(i,j1,j2) is the cross product of local unit vector w1(j1)
!  for site k1 with local unit vector w2(j2) for site k2.

      DOUBLE PRECISION :: xx(3,3)
      INTEGER :: i, j, j1, j2, k, m, p1, p2,                            &
          i1(3)=(/2,3,1/), i2(3)=(/3,1,2/)

!     print "(2I3,3x,2i3,3x,2i3)", m1, k1, m2, k2, ka, kb
!     call flush(6)

!  Pointers to orientation matrices
      p1=sm(k1)
      p2=sm(k2)

!  Scalar products of local axis vectors. xx(i,j) = w1(i) . w2(j)
      do j=1,3
        do i=1,3
            xx(i,j)=se(1,i,p1)*se(1,j,p2)                                &
                   +se(2,i,p1)*se(2,j,p2)                                &
                   +se(3,i,p1)*se(3,j,p2)
        end do
      end do
      cosine(7:15)=reshape(xx,(/9/))
!     cosine(7:15)=reshape(matmul(transpose(se(:,:,p1)),se(:,:,p2)),(/9/))

!  Offset the position of molecule 2 according to the cell
!  being considered
      rab(:)=sx(:,k2)-sx(:,k1)
      rab(1)=rab(1)+avecml(adin2,1)*ka+bvecml(adin2,1)*kb
      rab(2)=rab(2)+avecml(adin2,2)*ka+bvecml(adin2,2)*kb
      r=sqrt(rab(1)**2+rab(2)**2+rab(3)**2)

      er(:)=rab(:)/r   ! normalized intersite vector

      e1r(:)=matmul(er(:),se(:,:,p1))
      e2r(:)=-matmul(er(:),se(:,:,p2))  ! Note sign

!  If order=0 this is all we need
      if (order .lt. 1) return

      do i=1,3
        a(i)=sx(i,k1)-sx(i,cm(m1))     !  site vectors wrt GLOBAL AXES
        b(i)=sx(i,k2)-sx(i,cm(m2))     !  relative to molecular origin
        ra(i) = rab(i) + a(i)
        rb(i) = rab(i) - b(i)
      end do

!  Calculate cross products needed in first derivatives
      rxa(1) = rab(2)*a(3) - rab(3)*a(2)
      rxa(2) = rab(3)*a(1) - rab(1)*a(3)
      rxa(3) = rab(1)*a(2) - rab(2)*a(1)

      rxb(1) = rab(2)*b(3) - rab(3)*b(2)
      rxb(2) = rab(3)*b(1) - rab(1)*b(3)
      rxb(3) = rab(1)*b(2) - rab(2)*b(1)

      do j1=1,3
        raxw1(1,j1) = ra(2)*se(3,j1,p1) - ra(3)*se(2,j1,p1)
        raxw1(2,j1) = ra(3)*se(1,j1,p1) - ra(1)*se(3,j1,p1)
        raxw1(3,j1) = ra(1)*se(2,j1,p1) - ra(2)*se(1,j1,p1)

        rbxw2(1,j1) = rb(2)*se(3,j1,p2) - rb(3)*se(2,j1,p2)
        rbxw2(2,j1) = rb(3)*se(1,j1,p2) - rb(1)*se(3,j1,p2)
        rbxw2(3,j1) = rb(1)*se(2,j1,p2) - rb(2)*se(1,j1,p2)

        w2xa(1,j1)  = se(2,j1,p2)*a(3) - se(3,j1,p2)*a(2)
        w2xa(2,j1)  = se(3,j1,p2)*a(1) - se(1,j1,p2)*a(3)
        w2xa(3,j1)  = se(1,j1,p2)*a(2) - se(2,j1,p2)*a(1)

        w1xb(1,j1)  = se(2,j1,p1)*b(3) - se(3,j1,p1)*b(2)
        w1xb(2,j1)  = se(3,j1,p1)*b(1) - se(1,j1,p1)*b(3)
        w1xb(3,j1)  = se(1,j1,p1)*b(2) - se(2,j1,p1)*b(1)

        do j2=1,3   !  nine cross products for w1xw2
          w1xw2(1,j1,j2)=se(2,j1,p1)*se(3,j2,p2)-se(3,j1,p1)*se(2,j2,p2)
          w1xw2(2,j1,j2)=se(3,j1,p1)*se(1,j2,p2)-se(1,j1,p1)*se(3,j2,p2)
          w1xw2(3,j1,j2)=se(1,j1,p1)*se(2,j2,p2)-se(2,j1,p1)*se(1,j2,p2)
        end do
      end do

!  Now compute first derivatives of |R|, w1.R, w2.R and w1.w2. The scalar
!  products involve the vector R, not the unit vector er.
      do i=1,3
!  R
        D1(16,i)=-rab(i)/R
        D1(16,3+i)=rxa(i)/R
        D1(16,6+i)=rab(i)/R
        D1(16,9+i)=-rxb(i)/R
        do j=1,3
!  w1.R
          D1(j,i)=-se(i,j,p1)
          D1(j,3+i)=-raxw1(i,j)
          D1(j,6+i)=se(i,j,p1)
          D1(j,9+i)=-w1xb(i,j)
!  w2.R
          D1(3+j,i)=se(i,j,p2)
          D1(3+j,3+i)=-w2xa(i,j)
          D1(3+j,6+i)=-se(i,j,p2)
          D1(3+j,9+i)=rbxw2(i,j)
          do j1=1,3
!  w1.w2
            D1(3+j1+3*j,i)=0d0
            D1(3+j1+3*j,3+i)=w1xw2(i,j1,j)
            D1(3+j1+3*j,6+i)=0d0
            D1(3+j1+3*j,9+i)=-w1xw2(i,j1,j)
          end do
        end do
      end do

!  Now we have to manipulate these to get derivatives of the unit-vector
!  scalar products.
!  Not optimised
!     do j=1,3
!       do i=1,12
!         D1(j,i)=(D1(j,i)-e1r(j)*D1(16,i))/R
!         D1(3+j,i)=(D1(3+j,i)-e2r(j)*D1(16,i))/R
!       end do
!     end do
!  Optimised
      do i=1,12
        D1(1,i)=(D1(1,i)-e1r(1)*D1(16,i))/R
        D1(2,i)=(D1(2,i)-e1r(2)*D1(16,i))/R
        D1(3,i)=(D1(3,i)-e1r(3)*D1(16,i))/R
        D1(4,i)=(D1(4,i)-e2r(1)*D1(16,i))/R
        D1(5,i)=(D1(5,i)-e2r(2)*D1(16,i))/R
        D1(6,i)=(D1(6,i)-e2r(3)*D1(16,i))/R
      end do
!  End optimised

      if (order .lt. 2) return

!  Scalar products occurring in second derivatives
      do i=1,3
        aw1(i)=0.d0
        aw2(i)=0.d0
        bw1(i)=0.d0
        bw2(i)=0.d0
        w1ra(i)=0.d0
        w2rb(i)=0.d0
      end do

      ab=a(1)*b(1)+a(2)*b(2)+a(3)*b(3)
      raa=ra(1)*a(1)+ra(2)*a(2)+ra(3)*a(3)
      rbb=rb(1)*b(1)+rb(2)*b(2)+rb(3)*b(3)
      do i=1,3
        do j=1,3
          aw1(i)=aw1(i)+se(j,i,p1)*a(j)
          aw2(i)=aw2(i)+se(j,i,p2)*a(j)
          bw1(i)=bw1(i)+se(j,i,p1)*b(j)
          bw2(i)=bw2(i)+se(j,i,p2)*b(j)
          w1ra(i)=w1ra(i)+se(j,i,p1)*ra(j)
          w2rb(i)=w2rb(i)+se(j,i,p2)*rb(j)
        end do
      end do

!  Second derivatives. Clear matrix.
!  Not optimised
!     do i=1,12
!       do j=1,12
!         do iq=1,16
!           d2(iq,i,j)=0d0
!         end do
!       end do
!     end do
!  Optimised This routine needs 8% CPU Time!
      do j=1,12
        do i=1,12
            d2(1,i,j)=0d0
            d2(2,i,j)=0d0
            d2(3,i,j)=0d0
            d2(4,i,j)=0d0
            d2(5,i,j)=0d0
            d2(6,i,j)=0d0
            d2(7,i,j)=0d0
            d2(8,i,j)=0d0
            d2(9,i,j)=0d0
            d2(10,i,j)=0d0
            d2(11,i,j)=0d0
            d2(12,i,j)=0d0
            d2(13,i,j)=0d0
            d2(14,i,j)=0d0
            d2(15,i,j)=0d0
            d2(16,i,j)=0d0
        end do
      end do
!  End optimised

!  Second derivatives of R.R (factor of 2 omitted from all of these)
      do i=1,3
        D2(16,i,i)=1d0
        D2(16,6+i,i)=-1d0
        D2(16,i,6+i)=-1d0
        D2(16,6+i,6+i)=1d0
        do j=1,3
          D2(16,3+i,3+j)=-ra(i)*a(j)
          D2(16,3+i,9+j)=b(i)*a(j)
          D2(16,9+i,3+j)=a(i)*b(j)
          D2(16,9+i,9+j)=rb(i)*b(j)
        end do
        D2(16,3+i,3+i)=D2(16,3+i,3+i)+raa
        D2(16,3+i,9+i)=D2(16,3+i,9+i)-ab
        D2(16,9+i,3+i)=D2(16,9+i,3+i)-ab
        D2(16,9+i,9+i)=D2(16,9+i,9+i)-rbb
        j=i1(i)
        k=i2(i)
        D2(16,j,3+k)=a(i)
        D2(16,k,3+j)=-a(i)
        D2(16,j,9+k)=-b(i)
        D2(16,k,9+j)=b(i)
        D2(16,3+j,k)=-a(i)
        D2(16,3+k,j)=a(i)
        D2(16,3+j,6+k)=a(i)
        D2(16,3+k,6+j)=-a(i)
        D2(16,6+j,3+k)=-a(i)
        D2(16,6+k,3+j)=a(i)
        D2(16,6+j,9+k)=b(i)
        D2(16,6+k,9+j)=-b(i)
        D2(16,9+j,k)=b(i)
        D2(16,9+k,j)=-b(i)
        D2(16,9+j,6+k)=-b(i)
        D2(16,9+k,6+j)=b(i)
      end do
!  Construct second derivatives of |R| from these
      do i=1,12
        do j=1,12
          D2(16,i,j)=(D2(16,i,j)-D1(16,i)*D1(16,j))/R
        end do
      end do

!  Second derivatives of w1.R and w2.R. Again, the scalar
!  products initially involve the vector R, not the unit vector er.
      do k=1,3
        do i=1,3
          j1=i1(i)
          j2=i2(i)
!  w1(k).R
          do j=1,3
            D2(k,3+i,3+j)=ra(i)*se(j,k,p1)
            D2(k,3+i,9+j)=-b(i)*se(j,k,p1)
            D2(k,9+i,3+j)=-se(i,k,p1)*b(j)
            D2(k,9+i,9+j)=se(i,k,p1)*b(j)
          end do
          D2(k,3+i,3+i)=D2(k,3+i,3+i)-w1ra(k)
          D2(k,3+i,9+i)=D2(k,3+i,9+i)+bw1(k)
          D2(k,9+i,3+i)=D2(k,9+i,3+i)+bw1(k)
          D2(k,9+i,9+i)=D2(k,9+i,9+i)-bw1(k)
          D2(k,j1,3+j2)=-se(i,k,p1)
          D2(k,j2,3+j1)=se(i,k,p1)
          D2(k,3+j1,j2)=se(i,k,p1)
          D2(k,3+j2,j1)=-se(i,k,p1)
          D2(k,3+j1,6+j2)=-se(i,k,p1)
          D2(k,3+j2,6+j1)=se(i,k,p1)
          D2(k,6+j1,3+j2)=se(i,k,p1)
          D2(k,6+j2,3+j1)=-se(i,k,p1)
!  w2(k).R
          do j=1,3
            D2(3+k,3+i,3+j)=se(i,k,p2)*a(j)
            D2(3+k,3+i,9+j)=-se(i,k,p2)*a(j)
            D2(3+k,9+i,3+j)=-a(i)*se(j,k,p2)
            D2(3+k,9+i,9+j)=-rb(i)*se(j,k,p2)
          end do
          D2(3+k,3+i,3+i)=D2(3+k,3+i,3+i)-aw2(k)
          D2(3+k,3+i,9+i)=D2(3+k,3+i,9+i)+aw2(k)
          D2(3+k,9+i,3+i)=D2(3+k,9+i,3+i)+aw2(k)
          D2(3+k,9+i,9+i)=D2(3+k,9+i,9+i)+w2rb(k)
          D2(3+k,j1,9+j2)=se(i,k,p2)
          D2(3+k,j2,9+j1)=-se(i,k,p2)
          D2(3+k,6+j1,9+j2)=-se(i,k,p2)
          D2(3+k,6+j2,9+j1)=se(i,k,p2)
          D2(3+k,9+j1,j2)=-se(i,k,p2)
          D2(3+k,9+j2,j1)=se(i,k,p2)
          D2(3+k,9+j1,6+j2)=se(i,k,p2)
          D2(3+k,9+j2,6+j1)=-se(i,k,p2)
!  w1(k).w2(m)
          do m=1,3
            do j=1,3
              D2(3+k+3*m,3+i,3+j)=se(j,k,p1)*se(i,m,p2)
              D2(3+k+3*m,3+i,9+j)=-se(j,k,p1)*se(i,m,p2)
              D2(3+k+3*m,9+i,3+j)=-se(i,k,p1)*se(j,m,p2)
              D2(3+k+3*m,9+i,9+j)=se(i,k,p1)*se(j,m,p2)
            end do
            D2(3+k+3*m,3+i,3+i)=D2(3+k+3*m,3+i,3+i)-xx(k,m)
            D2(3+k+3*m,3+i,9+i)=D2(3+k+3*m,3+i,9+i)+xx(k,m)
            D2(3+k+3*m,9+i,3+i)=D2(3+k+3*m,9+i,3+i)+xx(k,m)
            D2(3+k+3*m,9+i,9+i)=D2(3+k+3*m,9+i,9+i)-xx(k,m)
          end do ! m
        end do ! i
      end do ! k

!  Convert derivatives of w1.R and w2.R into derivatives of w1.R/R and w2.R/R
!  Not optimised
!     do k=1,3
!       do i=1,12
!         do j=1,12
!           D2(k,i,j)=(D2(k,i,j)-e1r(k)*D2(16,i,j)
!    &                  -D1(16,i)*D1(k,j)-D1(16,j)*D1(k,i))/R
!           D2(3+k,i,j)=(D2(3+k,i,j)-e2r(k)*D2(16,i,j)
!    &                  -D1(16,i)*D1(3+k,j)-D1(16,j)*D1(3+k,i))/R
!         end do ! j
!       end do ! i
!     end do ! k
!  Optimised
      do j=1,12
            D2(1,1,j)=(D2(1,1,j)-e1r(1)*D2(16,1,j)                       &
                        -D1(16,1)*D1(1,j)-D1(16,j)*D1(1,1))/R
            D2(2,1,j)=(D2(2,1,j)-e1r(2)*D2(16,1,j)                       &
                        -D1(16,1)*D1(2,j)-D1(16,j)*D1(2,1))/R
            D2(3,1,j)=(D2(3,1,j)-e1r(3)*D2(16,1,j)                       &
                        -D1(16,1)*D1(3,j)-D1(16,j)*D1(3,1))/R
            D2(4,1,j)=(D2(4,1,j)-e2r(1)*D2(16,1,j)                       &
                        -D1(16,1)*D1(4,j)-D1(16,j)*D1(4,1))/R
            D2(5,1,j)=(D2(5,1,j)-e2r(2)*D2(16,1,j)                       &
                        -D1(16,1)*D1(5,j)-D1(16,j)*D1(5,1))/R
            D2(6,1,j)=(D2(6,1,j)-e2r(3)*D2(16,1,j)                       &
                        -D1(16,1)*D1(6,j)-D1(16,j)*D1(6,1))/R

            D2(1,2,j)=(D2(1,2,j)-e1r(1)*D2(16,2,j)                       &
                        -D1(16,2)*D1(1,j)-D1(16,j)*D1(1,2))/R
            D2(2,2,j)=(D2(2,2,j)-e1r(2)*D2(16,2,j)                       &
                        -D1(16,2)*D1(2,j)-D1(16,j)*D1(2,2))/R
            D2(3,2,j)=(D2(3,2,j)-e1r(3)*D2(16,2,j)                       &
                        -D1(16,2)*D1(3,j)-D1(16,j)*D1(3,2))/R
            D2(4,2,j)=(D2(4,2,j)-e2r(1)*D2(16,2,j)                       &
                        -D1(16,2)*D1(4,j)-D1(16,j)*D1(4,2))/R
            D2(5,2,j)=(D2(5,2,j)-e2r(2)*D2(16,2,j)                       &
                        -D1(16,2)*D1(5,j)-D1(16,j)*D1(5,2))/R
            D2(6,2,j)=(D2(6,2,j)-e2r(3)*D2(16,2,j)                       &
                        -D1(16,2)*D1(6,j)-D1(16,j)*D1(6,2))/R

            D2(1,3,j)=(D2(1,3,j)-e1r(1)*D2(16,3,j)                       &
                        -D1(16,3)*D1(1,j)-D1(16,j)*D1(1,3))/R
            D2(2,3,j)=(D2(2,3,j)-e1r(2)*D2(16,3,j)                       &
                        -D1(16,3)*D1(2,j)-D1(16,j)*D1(2,3))/R
            D2(3,3,j)=(D2(3,3,j)-e1r(3)*D2(16,3,j)                       &
                        -D1(16,3)*D1(3,j)-D1(16,j)*D1(3,3))/R
            D2(4,3,j)=(D2(4,3,j)-e2r(1)*D2(16,3,j)                       &
                        -D1(16,3)*D1(4,j)-D1(16,j)*D1(4,3))/R
            D2(5,3,j)=(D2(5,3,j)-e2r(2)*D2(16,3,j)                       &
                        -D1(16,3)*D1(5,j)-D1(16,j)*D1(5,3))/R
            D2(6,3,j)=(D2(6,3,j)-e2r(3)*D2(16,3,j)                       &
                        -D1(16,3)*D1(6,j)-D1(16,j)*D1(6,3))/R

            D2(1,4,j)=(D2(1,4,j)-e1r(1)*D2(16,4,j)                       &
                        -D1(16,4)*D1(1,j)-D1(16,j)*D1(1,4))/R
            D2(2,4,j)=(D2(2,4,j)-e1r(2)*D2(16,4,j)                       &
                        -D1(16,4)*D1(2,j)-D1(16,j)*D1(2,4))/R
            D2(3,4,j)=(D2(3,4,j)-e1r(3)*D2(16,4,j)                       &
                        -D1(16,4)*D1(3,j)-D1(16,j)*D1(3,4))/R
            D2(4,4,j)=(D2(4,4,j)-e2r(1)*D2(16,4,j)                       &
                        -D1(16,4)*D1(4,j)-D1(16,j)*D1(4,4))/R
            D2(5,4,j)=(D2(5,4,j)-e2r(2)*D2(16,4,j)                       &
                        -D1(16,4)*D1(5,j)-D1(16,j)*D1(5,4))/R
            D2(6,4,j)=(D2(6,4,j)-e2r(3)*D2(16,4,j)                       &
                        -D1(16,4)*D1(6,j)-D1(16,j)*D1(6,4))/R

            D2(1,5,j)=(D2(1,5,j)-e1r(1)*D2(16,5,j)                       &
                        -D1(16,5)*D1(1,j)-D1(16,j)*D1(1,5))/R
            D2(2,5,j)=(D2(2,5,j)-e1r(2)*D2(16,5,j)                       &
                        -D1(16,5)*D1(2,j)-D1(16,j)*D1(2,5))/R
            D2(3,5,j)=(D2(3,5,j)-e1r(3)*D2(16,5,j)                       &
                        -D1(16,5)*D1(3,j)-D1(16,j)*D1(3,5))/R
            D2(4,5,j)=(D2(4,5,j)-e2r(1)*D2(16,5,j)                       &
                        -D1(16,5)*D1(4,j)-D1(16,j)*D1(4,5))/R
            D2(5,5,j)=(D2(5,5,j)-e2r(2)*D2(16,5,j)                       &
                        -D1(16,5)*D1(5,j)-D1(16,j)*D1(5,5))/R
            D2(6,5,j)=(D2(6,5,j)-e2r(3)*D2(16,5,j)                       &
                        -D1(16,5)*D1(6,j)-D1(16,j)*D1(6,5))/R

            D2(1,6,j)=(D2(1,6,j)-e1r(1)*D2(16,6,j)                       &
                        -D1(16,6)*D1(1,j)-D1(16,j)*D1(1,6))/R
            D2(2,6,j)=(D2(2,6,j)-e1r(2)*D2(16,6,j)                       &
                        -D1(16,6)*D1(2,j)-D1(16,j)*D1(2,6))/R
            D2(3,6,j)=(D2(3,6,j)-e1r(3)*D2(16,6,j)                       &
                        -D1(16,6)*D1(3,j)-D1(16,j)*D1(3,6))/R
            D2(4,6,j)=(D2(4,6,j)-e2r(1)*D2(16,6,j)                       &
                        -D1(16,6)*D1(4,j)-D1(16,j)*D1(4,6))/R
            D2(5,6,j)=(D2(5,6,j)-e2r(2)*D2(16,6,j)                       &
                        -D1(16,6)*D1(5,j)-D1(16,j)*D1(5,6))/R
            D2(6,6,j)=(D2(6,6,j)-e2r(3)*D2(16,6,j)                       &
                        -D1(16,6)*D1(6,j)-D1(16,j)*D1(6,6))/R

            D2(1,7,j)=(D2(1,7,j)-e1r(1)*D2(16,7,j)                       &
                        -D1(16,7)*D1(1,j)-D1(16,j)*D1(1,7))/R
            D2(2,7,j)=(D2(2,7,j)-e1r(2)*D2(16,7,j)                       &
                        -D1(16,7)*D1(2,j)-D1(16,j)*D1(2,7))/R
            D2(3,7,j)=(D2(3,7,j)-e1r(3)*D2(16,7,j)                       &
                        -D1(16,7)*D1(3,j)-D1(16,j)*D1(3,7))/R
            D2(4,7,j)=(D2(4,7,j)-e2r(1)*D2(16,7,j)                       &
                        -D1(16,7)*D1(4,j)-D1(16,j)*D1(4,7))/R
            D2(5,7,j)=(D2(5,7,j)-e2r(2)*D2(16,7,j)                       &
                        -D1(16,7)*D1(5,j)-D1(16,j)*D1(5,7))/R
            D2(6,7,j)=(D2(6,7,j)-e2r(3)*D2(16,7,j)                       &
                        -D1(16,7)*D1(6,j)-D1(16,j)*D1(6,7))/R

            D2(1,8,j)=(D2(1,8,j)-e1r(1)*D2(16,8,j)                       &
                        -D1(16,8)*D1(1,j)-D1(16,j)*D1(1,8))/R
            D2(2,8,j)=(D2(2,8,j)-e1r(2)*D2(16,8,j)                       &
                        -D1(16,8)*D1(2,j)-D1(16,j)*D1(2,8))/R
            D2(3,8,j)=(D2(3,8,j)-e1r(3)*D2(16,8,j)                       &
                        -D1(16,8)*D1(3,j)-D1(16,j)*D1(3,8))/R
            D2(4,8,j)=(D2(4,8,j)-e2r(1)*D2(16,8,j)                       &
                        -D1(16,8)*D1(4,j)-D1(16,j)*D1(4,8))/R
            D2(5,8,j)=(D2(5,8,j)-e2r(2)*D2(16,8,j)                       &
                        -D1(16,8)*D1(5,j)-D1(16,j)*D1(5,8))/R
            D2(6,8,j)=(D2(6,8,j)-e2r(3)*D2(16,8,j)                       &
                        -D1(16,8)*D1(6,j)-D1(16,j)*D1(6,8))/R

            D2(1,9,j)=(D2(1,9,j)-e1r(1)*D2(16,9,j)                       &
                        -D1(16,9)*D1(1,j)-D1(16,j)*D1(1,9))/R
            D2(2,9,j)=(D2(2,9,j)-e1r(2)*D2(16,9,j)                       &
                        -D1(16,9)*D1(2,j)-D1(16,j)*D1(2,9))/R
            D2(3,9,j)=(D2(3,9,j)-e1r(3)*D2(16,9,j)                       &
                        -D1(16,9)*D1(3,j)-D1(16,j)*D1(3,9))/R
            D2(4,9,j)=(D2(4,9,j)-e2r(1)*D2(16,9,j)                       &
                        -D1(16,9)*D1(4,j)-D1(16,j)*D1(4,9))/R
            D2(5,9,j)=(D2(5,9,j)-e2r(2)*D2(16,9,j)                       &
                        -D1(16,9)*D1(5,j)-D1(16,j)*D1(5,9))/R
            D2(6,9,j)=(D2(6,9,j)-e2r(3)*D2(16,9,j)                       &
                        -D1(16,9)*D1(6,j)-D1(16,j)*D1(6,9))/R

            D2(1,10,j)=(D2(1,10,j)-e1r(1)*D2(16,10,j)                    &
                        -D1(16,10)*D1(1,j)-D1(16,j)*D1(1,10))/R
            D2(2,10,j)=(D2(2,10,j)-e1r(2)*D2(16,10,j)                    &
                        -D1(16,10)*D1(2,j)-D1(16,j)*D1(2,10))/R
            D2(3,10,j)=(D2(3,10,j)-e1r(3)*D2(16,10,j)                    &
                        -D1(16,10)*D1(3,j)-D1(16,j)*D1(3,10))/R
            D2(4,10,j)=(D2(4,10,j)-e2r(1)*D2(16,10,j)                    &
                        -D1(16,10)*D1(4,j)-D1(16,j)*D1(4,10))/R
            D2(5,10,j)=(D2(5,10,j)-e2r(2)*D2(16,10,j)                    &
                        -D1(16,10)*D1(5,j)-D1(16,j)*D1(5,10))/R
            D2(6,10,j)=(D2(6,10,j)-e2r(3)*D2(16,10,j)                    &
                        -D1(16,10)*D1(6,j)-D1(16,j)*D1(6,10))/R

            D2(1,11,j)=(D2(1,11,j)-e1r(1)*D2(16,11,j)                    &
                        -D1(16,11)*D1(1,j)-D1(16,j)*D1(1,11))/R
            D2(2,11,j)=(D2(2,11,j)-e1r(2)*D2(16,11,j)                    &
                        -D1(16,11)*D1(2,j)-D1(16,j)*D1(2,11))/R
            D2(3,11,j)=(D2(3,11,j)-e1r(3)*D2(16,11,j)                    &
                        -D1(16,11)*D1(3,j)-D1(16,j)*D1(3,11))/R
            D2(4,11,j)=(D2(4,11,j)-e2r(1)*D2(16,11,j)                    &
                        -D1(16,11)*D1(4,j)-D1(16,j)*D1(4,11))/R
            D2(5,11,j)=(D2(5,11,j)-e2r(2)*D2(16,11,j)                    &
                        -D1(16,11)*D1(5,j)-D1(16,j)*D1(5,11))/R
            D2(6,11,j)=(D2(6,11,j)-e2r(3)*D2(16,11,j)                    &
                        -D1(16,11)*D1(6,j)-D1(16,j)*D1(6,11))/R

            D2(1,12,j)=(D2(1,12,j)-e1r(1)*D2(16,12,j)                    &
                        -D1(16,12)*D1(1,j)-D1(16,j)*D1(1,12))/R
            D2(2,12,j)=(D2(2,12,j)-e1r(2)*D2(16,12,j)                    &
                        -D1(16,12)*D1(2,j)-D1(16,j)*D1(2,12))/R
            D2(3,12,j)=(D2(3,12,j)-e1r(3)*D2(16,12,j)                    &
                        -D1(16,12)*D1(3,j)-D1(16,j)*D1(3,12))/R
            D2(4,12,j)=(D2(4,12,j)-e2r(1)*D2(16,12,j)                    &
                        -D1(16,12)*D1(4,j)-D1(16,j)*D1(4,12))/R
            D2(5,12,j)=(D2(5,12,j)-e2r(2)*D2(16,12,j)                    &
                        -D1(16,12)*D1(5,j)-D1(16,j)*D1(5,12))/R
            D2(6,12,j)=(D2(6,12,j)-e2r(3)*D2(16,12,j)                    &
                        -D1(16,12)*D1(6,j)-D1(16,j)*D1(6,12))/R
      end do ! j

!      do j=1,12
!        d11j=D1(1,j)
!        d12j=D1(2,j)
!        d13j=D1(3,j)
!        d14j=D1(4,j)
!        d15j=D1(5,j)
!        d16j=D1(6,j)
!        d116j=D1(16,j)
!        do i=1,12
!            d116i=D1(16,i)
!            d216ij=D2(16,i,j)
!            D2(1,i,j)=(D2(1,i,j)-e1r(1)*d216ij
!     &                  -d116i*d11j-d116j*D1(1,i))/R
!            D2(2,i,j)=(D2(2,i,j)-e1r(2)*d216ij
!     &                  -d116i*d12j-d116j*D1(2,i))/R
!            D2(3,i,j)=(D2(3,i,j)-e1r(3)*d216ij
!     &                  -d116i*d13j-d116j*D1(3,i))/R
!            D2(4,i,j)=(D2(4,i,j)-e2r(1)*d216ij
!     &                  -d116i*d14j-d116j*D1(4,i))/R
!            D2(5,i,j)=(D2(5,i,j)-e2r(2)*d216ij
!     &                  -d116i*d15j-d116j*D1(5,i))/R
!            D2(6,i,j)=(D2(6,i,j)-e2r(3)*d216ij
!     &                  -d116i*d16j-d116j*D1(6,i))/R
!        end do ! i
!      end do ! j
!  End optimised

      END SUBROUTINE mlinfo

!-----------------------------------------------------------------------

      SUBROUTINE nearest_image(m1,m2,ka,kb,cutoff,adin2)

      USE global
      USE interact
      USE molecules
      USE monolayer
      USE sites
      IMPLICIT NONE

!  m1, m2: The two molecules being considered.
      INTEGER, INTENT(IN) :: m1, m2, adin2
      INTEGER, INTENT(OUT) :: ka, kb
      LOGICAL, INTENT(OUT) :: cutoff

!  Output:
!  ka, kb: The index of the surface unit cell in which the
!  nearest image of molecule m2 is to be found.

!  cutoff:  will be set true by this routine if the molecules are
!  too far apart.

      DOUBLE PRECISION :: rab(3), rsq

!  Find the periodic image of molecule 2 nearest to molecule 1
      rab(:)=sx(:,cm(m2))-sx(:,cm(m1))
      ka=-nint((rab(2)*bvecml(adin2,1)-rab(1)*bvecml(adin2,2))                  &
          /(avecml(adin2,2)*bvecml(adin2,1)-avecml(adin2,1)*bvecml(adin2,2)))
      kb=-nint((rab(2)*avecml(adin2,1)-rab(1)*avecml(adin2,2))                  &
          /(bvecml(adin2,2)*avecml(adin2,1)-bvecml(adin2,1)*avecml(adin2,2)))
      rab(1)=rab(1)+avecml(adin2,1)*ka+bvecml(adin2,1)*kb
      rab(2)=rab(2)+avecml(adin2,2)*ka+bvecml(adin2,2)*kb
      rsq=rab(1)*rab(1)+rab(2)*rab(2)+rab(3)*rab(3)
      cutoff=(rsq>mc_cutoff*mc_cutoff)

      END SUBROUTINE nearest_image
