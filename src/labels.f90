MODULE labels

INTEGER, PARAMETER :: nlabels=81
!  LABEL is the array of angular momentum indices: 00, 10, 11c, 11s, etc.
CHARACTER(LEN=4) :: label(nlabels) = (/                                &
    '00 ', '10 ', '11c', '11s',                                        &
    '20 ', '21c', '21s', '22c', '22s',                                 &
    '30 ', '31c', '31s', '32c', '32s', '33c', '33s',                   &
    '40 ', '41c', '41s', '42c', '42s', '43c', '43s', '44c', '44s',     &
    '50 ', '51c', '51s', '52c', '52s', '53c', '53s', '54c', '54s',     &
           '55c', '55s',                                               &
    '60 ', '61c', '61s', '62c', '62s', '63c', '63s', '64c', '64s',     &
           '65c', '65s', '66c', '66s',                                 &
    '70 ', '71c', '71s', '72c', '72s', '73c', '73s', '74c', '74s',     &
           '75c', '75s', '76c', '76s', '77c', '77s',                   &
    '80 ', '81c', '81s', '82c', '82s', '83c', '83s', '84c', '84s',     &
           '85c', '85s', '86c', '86s', '87c', '87s', '88c', '88s'      &
    /)

CHARACTER(LEN=80) :: title(2) = (/ " ", " " /)

END MODULE labels
