MODULE molecules

USE input, ONLY : die
USE binomials, ONLY : tablesize
USE consts, ONLY : dp

!  NMOLS is the maximum number of molecules that can be defined.
!  NHESS=6*NMOLS is the dimension for arrays involving displacement
!    and rotation coordinates.
!  MOLS is the total number of molecules actually defined.
!  HEAD(M) points to the first site of molecule M.
!  LINEAR is true for linear molecules
!  FIXED is true for molecules that have no variable coordinates.
!  CALC is true if the site positions and orientations have been
!    calculated (AXES routine).
!  MDIM is no longer used -- now in TYPE(molecule)

!  CM(M) points to the special centre-of-mass site for molecule M.
!  This site is used in optimizations and is at the centre of mass.
!  It precedes the first site head(M) in the site chain. Its orientation
!  gives the principal axes of the inertia tensor.
!  For an atom, however, it is the same as head(M).

!  MMASS(M) is the mass of molecule M in a.m.u.

!  MI(k,M) is the kth principal moment of inertia for molecule M in
!  descending order, in a.m.u. bohr^2.

!  MP(:,M) is the set of 3 angle-axis variables used in some cases to
!  describe the orientation of molecule M.

!  QM(M) points to a site used for the total molecular moments, referred
!  to the centre of mass and to global axes.

!  When M=0, CM(M), MMASS(M) and MI(k,M) refer to the entire system.

!  NATOMS is the number of molecules that are actually atoms.
!  NLIN is the number of linear molecules.
!  These values are set in routine sortmolecules.

!  FIRSTP(M) is the first polarizable site (in linked-list order) for
!  molecule M. It is zero if M has no polarizable sites.

!  TS(M) holds the value of HEAD(M) for a suppressed molecule. When a
!  molecule is suppressed, its HEAD(M) is set to zero.

!  Molecule 0 is shorthand in some places for the complete set of
!  molecules. 
!  Molecule -1 is used when a whole molecule, or the complete
!  collection of molecules, is condensed to a single site. The site is
!  site -1, and it is the sole site of molecule -1.
!  Molecule -2 (and site -2) are used in a similar way for the lattice
!  cell.

LOGICAL, ALLOCATABLE, SAVE :: linear(:), fixed(:), calc(:)
INTEGER, SAVE :: nmols, mols=0, natoms=0, nlin=0, nhess
INTEGER, ALLOCATABLE, SAVE :: head(:), cm(:), qm(:), mp(:,:), &
    firstp(:), ts(:)

DOUBLE PRECISION, ALLOCATABLE, SAVE :: mmass(:), mi(:,:)

!  move(i,m) specifies whether coordinate i of molecule m is free to move
LOGICAL, ALLOCATABLE, SAVE :: move(:,:)

TYPE molecule
  !  This should contain all of the above information.
  !  At the moment it only contains angle-axis information
  INTEGER :: head            !  Index of head site
  CHARACTER(LEN=20) :: name  !  Name of molecule
  INTEGER :: mdim=0          !  0=atom, 1=linear, 3 otherwise.
                             !  (No need to distinguish planar molecules
                             !  from non-planar.)
  DOUBLE PRECISION :: cm(3)  !  Centre-of-mass position
  DOUBLE PRECISION :: p(3)   !  Angle-axis coordinates
  DOUBLE PRECISION :: N(3,3) !  Skew-symmetric matrix derived from
                             !  the unit vector n parallel to p
  DOUBLE PRECISION :: M(3,3) !  Molecular rotation matrix. (Rotation
                             !  to take inertial axes from coincidence
                             !  with global axes into actual orientation.)
  DOUBLE PRECISION :: M1(3,3,3) ! M1(:,:,k) is the derivative of M w.r.t
                             !  angle-axis coordinate p(k).
  LOGICAL :: aaderivs_done=.false.  !  Flag showing whether the
                             !  derivatives have been calculated for this
                             !  molecule.
  LOGICAL :: suppressed = .false.
                             !  Suppressed molecules don't take part in energy
                             !  calculations.
END TYPE molecule

TYPE(molecule), ALLOCATABLE :: mol(:)

!  The following definitions are to do with the grouping of molecules into
!  layers -- see comments in mdata.f90 for more information

!  All molecules are read into layer(0), a linked list of molecules.
!  If a real layer is later defined, molecules are transferred from
!  layer(0) to layer(layerindex). Nextm() and prevm() are the next
!  and previous molecules in the list respectively. The number of molecules
!  in each layer is contained in nmolls(layerindex).
!  Following a definition of a layer, any molecules remaining in layer(0)
!  are treated as an isolated cluster on the surface.

DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE, SAVE :: a0ml, b0ml,       &
    areamml !, mc_cutoff, mlrange
DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE, SAVE :: avecml,         &
    bvecml, astarml, bstarml
      
INTEGER, DIMENSION(:), ALLOCATABLE, SAVE :: layer, prevm, nextm,       &
    moll, nmolls
INTEGER, SAVE :: layerindex=0, nlayers, lastmoll=0     
INTEGER, DIMENSION(:), ALLOCATABLE, SAVE :: mlcells_tmp !, ewaldml_limit
!      INTEGER, DIMENSION(:,:,:), ALLOCATABLE, SAVE :: mlindex
!      INTEGER, PARAMETER :: MAXMLCELLS=5000

COMPLEX(kind=DP), DIMENSION(:,:), ALLOCATABLE, SAVE :: c_senet, thetac

CONTAINS

SUBROUTINE alloc_mols
IMPLICIT NONE
INTEGER :: ok

allocate(linear(nmols), fixed(nmols), calc(nmols), head(-2:nmols),     &
    firstp(nmols), cm(-2:nmols), mol(nmols),                           &
    mmass(-2:nmols), mi(3,0:nmols), qm(0:nmols), move(6,nmols),        &
    layer(0:nlayers), prevm(0:nmols), nextm(0:nmols), ts(-2:nmols),    &
    moll(1:nmols), nmolls(0:nlayers),                                  &
    a0ml(1:nlayers), b0ml(1:nlayers), areamml(1:nlayers),              &
    avecml(0:nlayers,2), bvecml(0:nlayers,2),                          & 
    astarml(0:nlayers,2), bstarml(0:nlayers,2),                        &
    !   ewaldml_limit(1:nlayers), mlrange(1:nlayers), mc_cutoff(1:nlayers), 
    mlcells_tmp(1:nlayers),                                            &
    !   mlindex(1:nlayers,2,0:MAXMLCELLS),                             &
    c_senet(1:nlayers,49),thetac(1:nlayers,((tablesize/2)-1)**2),      &
    stat=ok)
if (ok .gt. 0) call die                                                &
    ("Couldn't allocate arrays in MOLECULES module", .true.)

layer(:)=0
prevm(:)=0
nextm(:)=0
moll(:)=0
nmolls(:)=0

!  ewaldml_limit(:)=3
!  mlrange(:)=40d0
!  mc_cutoff(:)=0d0

nhess=6*nmols

head(:)=0
firstp(:)=0
! mdim(:)=0
cm(:)=0
mmass(:)=0d0
mi(:,:)=0d0
qm(:)=0
ts(:)=0

END SUBROUTINE alloc_mols

SUBROUTINE suppress(m)
!  Suppress molecule m so that it doesn't tke part in energy
!  calculations
INTEGER, INTENT(IN) :: m
mol(m)%suppressed = .true.
firstp(m) = -abs(firstp(m))
ts(m) = head(m)
head(m) = 0
END SUBROUTINE suppress

SUBROUTINE activate(m)
!  Re-activate molecules that have been previously suppressed.
INTEGER, INTENT(IN) :: m
if (mol(m)%suppressed) then
  head(m) = ts(m)
  firstp(m) = abs(firstp(m))
end if
mol(m)%suppressed = .false.
END SUBROUTINE activate

END MODULE molecules
