MODULE sqrts

DOUBLE PRECISION, PARAMETER ::                                         &
    rt2=1.4142135623730950488d0, rt3=1.7320508075688772935d0,          &
    rt5=2.2360679774997896964d0, rt7=2.6457513110645905905d0
DOUBLE PRECISION, PARAMETER :: rt6=rt2*rt3, rt10=rt2*rt5, rt14=rt2*rt7, &
    rt15=rt3*rt5, rt21=rt3*rt7, rt30=rt2*rt3*rt5, rt35=rt5*rt7,        &
    rt42=rt2*rt3*rt7, rt70=rt2*rt5*rt7, rt105=rt3*rt5*rt7

END MODULE sqrts
