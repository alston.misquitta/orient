SUBROUTINE axis(m,k0,r,e,verbose)

!  Find matrix of direction cosines for local axes of site k0 in molecule
!  m, given a data line of the form
!     (site0) GLOBAL
!  or
!     (site0) PRINCIPAL
!  or
!     (site0) x [FROM site1] TO site2 y [FROM site3] TO site4 [TWIST angle]
!  (but see below).
!  It is assumed that site0 has already been read by the calling routine
!  and is passed as the input argument k0.

!  E(k,i) is the direction cosine between global axis k and local axis i.
!  On entry, matrix E is the existing orientation matrix for the site. It is
!  replaced by the orientation matrix defining the new axes.
!  R is replaced by the transformation matrix between old and new local axes.
!  R(i,j) is the direction cosine between old axis i and new axis j.

!  If GLOBAL is specified, the new axes are the global axes, E is the unit
!  matrix, and R is the transpose of the old E.

!  If PRINCIPAL is specified, the new axes are the principal axes of the
!  dipole-dipole polarizability for site K0.

!  Otherwise any two of x, y and z may be specified, in any order.
!  The first axis specified is taken in the direction given; the second
!  is initially specified similarly but is then orthogonalised to the
!  first; and the third forms a right-handed set.

!  The FROM clause may be omitted if this axis runs from the site K0 for
!  which the axes are being defined. However, K0 may be zero, in which
!  case an explicit FROM clause is required.

!  Either or both of the axes may alternatively be defined by the syntax
!     {x|y|z} [FROM site1] BETWEEN site2 AND site3
!  In this case the axis is defined to bisect the angle between the
!  site1-site2 direction and the site1-site3 direction.

!  Also either or both of the axes may be defined by the syntax
!     {x|y|z} GLOBAL [ { g | -g } ]
!  In this case the particular axis specified is in the same direction
!  as, or the opposite direction to, global axis g. If the "g" or "-g"
!  is absent, the axis specified is in the same direction as the
!  corresponding global axis. g is specified as x, y or z.

!  It is possible to specify
!     TWIST angle
!  meaning that the existing geometry includes a twist through the
!  specified angle, which may be a variable or an explicit constant.

!  It is assumed that the AXES routine has been called before entry,
!  so that the current site positions are in the array SX.

USE alphas
USE input
USE indexes
USE indparams
USE sites
USE labels
USE molecules
USE variables, ONLY : readv
IMPLICIT NONE
DOUBLE PRECISION :: r(3,3), e(3,3), a(3), b(3), c(3,3), d
INTEGER, INTENT(IN) :: k0, m
LOGICAL, INTENT(IN), OPTIONAL :: verbose
INTEGER :: i,j,k, i1, i2, i3, j1, ka, nx

CHARACTER(LEN=12) :: ww
LOGICAL :: variable, unquiet

INTERFACE
  SUBROUTINE hdiag (iegen,order,a,eivr,n)
  LOGICAL, INTENT(IN) :: order, iegen
  DOUBLE PRECISION :: a(:,:)
  DOUBLE PRECISION, INTENT(OUT) :: eivr(:,:)
  INTEGER, INTENT(IN) :: n
  END SUBROUTINE hdiag
END INTERFACE

if (present(verbose)) then
  unquiet=verbose
else
  unquiet=.false.
end if
nx=0
do while (item .lt. nitems)
  call readu(ww)
  select case(ww)
  case("DEFINE","ROTATE")
    !  AXIS has been called from MDATA and has done what it has to do.
    call reread(-1)
    exit
  case("GLOBAL")
    !  Set the local axes to coincide with the global axes
    do i=1,3
      do j=1,3
        r(i,j)=e(j,i)
        e(j,i)=0d0
      end do
      e(i,i)=1d0
    end do
    return

  case("PRINCIPAL")
    !  Get the dipole polarizability matrix for site K0
    ka=lookup(indexa,k0,k0)
    if (ka .eq. 0) call die                                       &
        ('No polarizability for site '//name(k0),.true.)
    !  Polarizabilities are in the order z, x, y
    do i=1,3
      i1=mod(i,3)+2
      do j=1,3
        j1=mod(j,3)+2
        c(i,j)=alpha(i1,j1,ka)
      end do
    end do
    !  Diagonalise it
    call hdiag (.true.,.false.,c,r,3)
    !  Now the rotation matrix is in R -- but make sure it's a proper rotation
    d=r(1,1)*(r(2,2)*r(3,3)-r(2,3)*r(3,2))                           &
        +r(1,2)*(r(2,3)*r(3,1)-r(2,1)*r(3,3))                           &
        +r(1,3)*(r(2,1)*r(3,2)-r(2,2)*r(3,1))
    if (d .lt. 0d0) then
      r(3,1)=-r(3,1)
      r(3,2)=-r(3,2)
      r(3,3)=-r(3,3)
    endif
    !  r(i,j) is the direction cosine between principal axis j and the original
    !  local axis i.
    print '(1x,a/(1x,a,3f10.5))',                                  &
        '    Principal axes:   x         y         z',               &
        'Local axes:     X', (R(1,J), J=1,3),                        &
        '                Y', (R(2,J), J=1,3),                        &
        '                Z', (R(3,J), J=1,3)
    !  e(k,i) as given to the routine is the direction cosine between global
    !  axis k and local axis i.
    do i=1,3
      do j=1,3
        c(i,j)=e(i,j)
        e(i,j)=0d0
      end do
    end do
    do k=1,3
      do j=1,3
        do i=1,3
          e(k,j)=e(k,j)+c(k,i)*r(i,j)
        end do
      end do
    end do
    print '(1x,a/(1x,a,3f10.5))',                                  &
        '    Principal axes:   x         y         z',               &
        'Global axes:    X', (E(1,J), J=1,3),                        &
        '                Y', (E(2,J), J=1,3),                        &
        '                Z', (E(3,J), J=1,3)

  case("X","1")
    call find_axis(1)
  case("Y","2")
    call find_axis(2)
  case("Z","3")
    call find_axis(3)
  case("TWIST")
    if (k0 .eq. 0) call die                                       &
        ('Twist specification inappropriate',.true.)
    call readv(st(k0),2,variable)
  case default
    call die ('Syntax error in axis specification',.true.)
  end select
end do

if (nx < 2 .and. .not. linear(m))                                      &
    call die ("Axis specification incomplete",.true.)

CONTAINS

SUBROUTINE find_axis(i0)

!  Find local i0 axis: 1=x, 2=y, 3=z.
!  For linear molecules (i.e. molecules declared as linear in the
!  molecule definition), only the z axis may be specified unless global
!  axes are used.

IMPLICIT NONE
INTEGER, INTENT(IN) :: i0
INTEGER :: i, j, k, k1, k2, k3, ig
LOGICAL :: bisect, global
DOUBLE PRECISION :: aa, bb, s, u
CHARACTER(LEN=1) :: waxis
CHARACTER(LEN=12) :: ww

nx=nx+1
bisect=.false.
global=.false.
k1=k0
do
  call readu(ww)
  select case(ww)
  case("FROM")
    call read_site(k1,m)
  case("TO")
    call read_site(k2,m)
    exit
  case("BETWEEN")
    bisect=.true.
    call read_site(k2,m)
  case("AND")
    if (.not. bisect)                                                &
        call die ('Syntax error in axis specification',.true.)
    call read_site(k3,m)
    exit
  case("GLOBAL")
    global=.true.
    ig=i0
    s=1d0
    if (item < nitems) then
      call readu(ww)
      select case(ww)
      case("X","1")
        ig=1
      case("Y","2")
        ig=2
      case("Z","3")
        ig=3
      case("-X")
        ig=1
        s=-1d0
      case("-Y")
        ig=2
        s=-1d0
      case("-Z")
        ig=3
        s=-1d0
      case("+","-")
        if (ww .eq. "-") s=-1d0
        call readu(waxis)
        select case(waxis)
        case("X","1")
          ig=1
        case("Y","2")
          ig=2
        case("Z","3")
          ig=3
        end select
      case default
        call reread(-1)
      end select
    end if
    exit
  case default
    call die ('Syntax error in axis specification',.true.)
  end select
end do
if (k1 .eq. 0) call die ('FROM clause required',.true.)

!  Find vector in specified direction (not normalised)
if (global) then
  a(:)=0d0
  a(ig)=s
else
  if (linear(m) .and. i0 .ne. 3)                                       &
    call die("Only the z axis may be redefined for linear molecules",.true.)
  a(:)=sx(:,k2)-sx(:,k1)
  if (bisect) then
    aa=0d0
    bb=0d0
    do j=1,3
      b(j)=sx(j,k3)-sx(j,k1)
      aa=aa+a(j)**2
      bb=bb+b(j)**2
    end do
    if (aa .lt. 1d-6 .or. bb .lt. 1d-6) call die                 &
        ("Sites coincide: vector undefined",.true.)
    a(:)=(a(:)/sqrt(aa))+(b(:)/sqrt(bb))
  end if
end if

if (nx .eq. 1) then
  !  First axis
  i1=i0
  !  if (unquiet) print "(/a,i1,a)", "Axis ",i1, ":"
  !  print "(a,3f10.5)", "     1: ", a
  !  Normalise
  aa=0d0
  do j=1,3
    aa=aa+a(j)**2
  end do
  c(:,i1)=a(:)/sqrt(aa)
  if (unquiet) print "(a,i0,a,3f10.5)", "Axis ",i1, ":", c(:,i1)
  if (.not. linear(m)) return
end if

!  Second axis
if (linear(m)) then
  !  Axes perpendicular to the molecular axis (the z axis) are arbitrary.
  !  Choose the x axis parallel to the global X axis, unless that is close to
  !  the local z axis, in which case take it parallel to the global Y axis.
  i2=1
  a=[1d0,0d0,0d0]
  if (c(3,i1)**2+c(2,i1)**2 < 0.1d0) a=[0d0,1d0,0d0]
else
  i2=i0
end if

!  print "(/a,i1,a)", "Axis ",i2, ":"
!  print "(a,3f10.5)", "     1: ", a
!  Orthogonalise, then normalise
u=0d0
aa=0d0
do j=1,3
  u=u+c(j,i1)*a(j)
end do
do j=1,3
  a(j)=a(j)-u*c(j,i1)
  aa=aa+a(j)**2
end do
if (aa < 1d-6) call die("Specified axes are not independent",.true.)
!  print "(a,3f10.5)", "     2: ", a
c(:,i2)=a(:)/sqrt(aa)
if (unquiet) print "(a,i0,a,3f10.5)", "Axis ",i2, ":", c(:,i2)

!  Now we have c(:,i1) and c(:,i2). Third axis requires vector product.
i3=6-i1-i2
if (mod(i2,3) .eq. mod(i1+1,3)) then
  s=1d0
else
  s=-1d0
endif
c(1,i3)=s*(c(2,i1)*c(3,i2)-c(3,i1)*c(2,i2))
c(2,i3)=s*(c(3,i1)*c(1,i2)-c(1,i1)*c(3,i2))
c(3,i3)=s*(c(1,i1)*c(2,i2)-c(2,i1)*c(1,i2))
  
!  Now C is the new orientation matrix. Construct the rotation matrix R
!  from old to new. multipoles in rotated axes are related to multipoles
!  in global axes by
!     (mu_X mu_Y mu_Z) = (mu_x mu_y mu_z) E^T
!                      = (mu'_x mu'_y mu'_z) C^T,
!  because we are describing the _same_ multipoles in global axes in
!  alternative local axes, so we need to have
!     mu' = mu E^T C
!  and 
!     R = E^T C

!  (Remember that columns of E (rows of E^T) refer to local axes, and
!  rows (columns of E^T) to global axes.)

do i=1,3
  do j=1,3
    r(i,j)=0d0
    do k=1,3
      r(i,j)=r(i,j)+e(k,i)*c(k,j)
    end do
  end do
end do
e(:,:)=c(:,:)

END SUBROUTINE find_axis

END SUBROUTINE axis
