#!/usr/bin/env python3
#  -*-  coding:  iso-8859-1  -*-

"""Convert Mathematica S-function output for further processing.
"""

import os
import re
import argparse

parser = argparse.ArgumentParser(formatter_class = argparse.RawDescriptionHelpFormatter,
    description="""Convert Mathematica S-function output for further processing.
""", epilog="""
The output should have been saved from Mathematica as a text file called
Sll'j.txt, where l, l' and j are the actual numerical arguments. 
""")
parser.add_argument("file", help="Mathematica output file to process") 
args = parser.parse_args()

#  Extract the angular moment labels l1, l2 and j from the file name.
m = re.search(r'S(\d)(\d)(\d)', args.file)
l1 = int(m.group(1))
l2 = int(m.group(2))
j = int(m.group(3))


# os.rename(args.file, args.file + ".bak")
with open(args.file + ".new","w") as OUT:
    OUT.write(f"\\!\\(Array[S{l1:0d}{l2:0d}{j:0d},{{{2*l1+1:0d},{2*l2+1:0d}}},{{0,0}}]\)\n")
    with open(args.file) as IN:
        for line in IN:
            line = re.sub(r'^During.*:= *', '', line)
            line = re.sub(r'\[x,y,u,v\]', '', line)
            line = re.sub(r'^', r'\!\(', line)
            line = re.sub('\n', '\)\n', line)
            OUT.write(line)
        OUT.write("\)\n")

