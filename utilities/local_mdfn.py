#!/usr/bin/python
#  -*-  coding  iso-8859-1  -*-

"""Rewrite a molecule definition from global to local axes.
"""

import argparse
import re
import os
# import string
import subprocess

parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Rewrite a molecule definition from global to local axes.
""",epilog="""
E.g.
local_mdfn.py diglycine --mdfn <file> [<file> ...] --axes digly.axes

Apply a local-axis definition to each specified molecule definition file,
assumed to be given in global axes, and output a new molecule definition
file with the geometry and multipoles redefined for the local axes.
It is assumed that each molecule definition file name has the suffix .mdfn,
and the new file has the suffix _local.mdfn.
""")

parser.add_argument("name", help="Molecule name")
parser.add_argument("--mdfn", help="Molecule definition file", nargs="+")
parser.add_argument("--axes", help="File containing local axis definitions",
                    required=True)
# parser.add_argument("--to", help="File for molecule definition in local axes")

args = parser.parse_args()
if not os.path.exists(args.axes):
  print "Can't find axis definition file {}".format(args.axes)
  exit(1)

for file in args.mdfn:
  print file
  outfile = re.sub(r'\.mdfn', '_local.mdfn', file)
  if os.path.exists(outfile):
    print "File {} exists".format(outfile)
    q = raw_input("Overwrite? [Yn] ")
    if q in ["","Y","y"]:
      os.remove(outfile)
    else:
      continue
  q = raw_input("Proceed? [Yn] ")
  if q not in ["","Y","y"]:
    continue
  datafile = "orient.data"
  with open(datafile,"w") as DATA:
    DATA.write("""!  Generic Orient data file for potential maps

Allocate
  Molecules 20
End

Types
  H  Z  1
  C  Z  6
  N  Z  7
  O  Z  8
  F  Z  9
  S  Z 16
  b  Z  0
  Q  Z  0
End

""")
    with open(file) as MDF:
      DATA.write(MDF.read())

    DATA.write("""
Edit {mol}
  Bonds auto
  verbose
  #include {axes}
End

show {mol} write {out}

Finish
""".format(mol=args.name,axes=args.axes,out=outfile))

  with open(datafile) as IN, open("orient.out","w") as OUT:
    subprocess.call(["/home/ajs1/orient/trunk/bin/orient"],
                    stdin=IN, stdout=OUT)
  os.remove("orient.data")
  os.remove("orient.out")
